package lezione_06.esercizio04.zeno;
import java.util.Scanner;
/**
Creare una funzione chiamata “calcolo” che per ogni operazione aritmetica
che abbiamo visto (+, -, *, /, %), ritorni il risultato dell’operazione tra
altri due parametri numerici passati [Una sola per tutte le operazioni]
Consiglio: usare un parametro aggiuntivo per capire quale operazione svolgere,
 ad esempio un parametro di tipo stringa

Creare una funzione chiamata divisibile che prenda due parametri e ritorni True
se il primo è divisibile per il secondo, altrimenti ritorna False

Utilizzare queste funzioni in un programma chiamato “calcolatrice.java”, il programma 
dovrà leggere una stringa dall’utente, se la stringa è uguale ad una delle operazioni
 (+,-, *, /, %) il programma deve chiedere all’utente due input numerici da tastiera, eseguire l’operazione corrispondente, stampare il risultato ottenuto e aspettare che
l’utente inserisca un’altra operazione


seconda parametri

Se l’utente inserisce la stringa “div” il programma deve chiedere due input da tastiera all’utente e stampare se il primo è divisibile per il secondo ed aspettare che l’utente inserisca un’altra operazione (eg. dati 4 e 2 stampa “4 è divisibile per 2”, dati 6 e 5 stampa “6 non è divisibile per 5”)

Se l’utente inserisce la stringa “quit” il programma deve terminare salutando l’utente con la stringa: “arrivederci”. Nota= Fintantoché l’operazione inserita dall’utente non è quit, il programma continua a chiedere operazioni e ad eseguirle

Se l’utente inserisce una stringa diversa da quelle precedenti il programma deve stampare: “Scelta non valida”

(Facoltativo) Se l’utente inserisce la stringa “res” il programma deve stampare il risultato dell’ultima operazione e poi aspettare che l’utente inserisca un’altra operazione.

(Facoltativo) Modificate le funzioni in modo che il cast numerico sull’input sia fatto
solo nelle funzioni
 */

public class es1 {
    public static void main(String[] args) {
        calcolatrice();
    }
        public static double calcolo(char operatore, int a, int b){
            switch (operatore){
                case '+':
                    return a + b;
                case '-':
                    return a - b;

                case '/':
                    return a / b;

                    case '*':
                        return a * b;
                case '%':
                    return a % b;
                default:
                    return 0;

            }
        }
        public static boolean divisibile(int a, int b){
            return(calcolo('%', a, b) == 0);     
            
        } 
        public static void es1() {
            System.out.println(calcolo('+', 2, 3));
            System.out.println(calcolo('*', 2, 3));
            System.out.println(calcolo('/', 2, 3));
            System.out.println(calcolo('%', 2, 3));
            System.out.println(calcolo('-', 2, 3));
            System.out.println(divisibile(2, 3));


        }
        public static void calcolatrice(){
            Scanner input = new Scanner(System.in);
            while(true){
                System.out.println("inserisci l'operatore: ");
                char operazione = input.nextLine().charAt(0);
                System.out.println("inserisci il primo numero: ");
                int a = input.nextInt();
                System.out.println("inserisci il secondo numero: ");
                int b = input.nextInt();
                System.out.println(calcolo(operazione, a, b));
            }

        }
}
