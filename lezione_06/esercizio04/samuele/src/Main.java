import java.util.Scanner;

public class Main {

    public static boolean divisible(int a, int b){
        return a % b == 0;
    }
    public static int calc(int a, int b, String op){
        int result = 0;
        switch (op) {
            case "+" -> result = a + b;
            case "-" -> result = a - b;
            case "*" -> result = a * b;
            case "/" -> {
                if (divisible(a, b)) {
                    result = a / b;
                } else {
                    System.out.println(a + " non è interamente  divisibile per " + b);
                }
            }
            default -> System.out.println("Nessuna operazione valida inserita...");
        }

        return result;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Inserisci tipo di operazione (+, -, *, /): ");
        String op = input.nextLine();
        System.out.print("Inserisci primo numero: ");
        int x = input.nextInt();
        System.out.print("Inserisci secondo numero: ");
        int y = input.nextInt();
        System.out.println(calc(x, y, op));

    }
}