import java.util.Scanner;

public class fattoriale{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci un numero");
        int x = sc.nextInt();
        int res = fattoriale(x);
        System.out.println("Il fattoriale di " + x + " è " + res);
        sc.close();
    }

    public static int fattoriale (int x){
        if(x==1){
            return 1;
        }
        else{
            return x * fattoriale(x - 1);
        }

    }
}