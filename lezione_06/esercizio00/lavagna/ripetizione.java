package lezione_06.esercizio00;
public class ripetizione {

    static int fattoriale(int num){
        if(num ==1){
            return 1;
        }
        else {
            return fattoriale(num-1)*num;
        }
    }

    public static void main(String[] args){

        int x = 5;
        int fatt = fattoriale(x);
        System.out.println("Fattoriale di "+x+" è "+fatt);
    }
}
