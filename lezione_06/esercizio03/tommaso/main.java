package lezione_06.esercizio03.tommaso;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Cosa vuoi fare?");
        String op = in.nextLine();
        Integer num1 = 0;
        Integer num2 = 0;

        while(true) {
            if(op.equals("div")) {
                op = "";
                System.out.println("Inserisci un numero: ");
                num1 = Integer.parseInt(in.nextLine());
                System.out.println("Inserisci un altro numero: ");
                num2 = Integer.parseInt(in.nextLine());
                System.out.println(divisibile(num1, num2));
            } else if( op.equals("quit")) {
                op = "";
                return;
            } else if(op.equals("+") || op.equals("-") || op.equals("*") || op.equals("/")) {
                op = "";
                System.out.println("Inserisci un numero: ");
                num1 = Integer.parseInt(in.nextLine());
                System.out.println("Inserisci un altro numero: ");
                num2 = Integer.parseInt(in.nextLine());
                System.out.println(calcolo("-", num1,num2));
            } else{
                op = "";
                System.out.println("operazione non valida");
            }
            if(op == "") {
                System.out.println("Cosa vuoi fare?");
                op = in.nextLine();
            }
            
        }
    }

    public static int calcolo(String op, int num1, int num2 ) {
        if(op == "-") {
            return num1 - num2;
        } else if(op == "+") {
            return num1 + num2;
        } else if(op == "*") {
            return num1 * num2;
        } else if(op == "/") {
            return num1 / num2;
        } else if(op == "%") {
            return num1 % num2;
        } else {
            return -1;
        }
    }

    public static boolean divisibile(int num1, int num2) {
        return num1 % num2 == 0;
    }
}
