import java.util.Scanner;

public class Contazampe{
    public static void main(String[] args) {
        System.out.println("inserisci il numero di animali (galline, mucche, maiali, ragni)");
        Scanner s = new Scanner(System.in);
        int ga = s.nextInt();
        int mu = s.nextInt();
        int ma = s.nextInt();
        int ra = s.nextInt();

        Animali a = new Animali(ga, mu, ma, ra);
        System.out.println("Zampe totali: " + a.contazampe());
    }
}