import java.util.Scanner;

public class Main {
    public static final int BIPEDAL = 2;
    public static final int QUADRUPED = 4;
    public static final int ARACHNID = 8;
    public static int farm(int ck, int cw, int pg, int sp){
        int totCk = ck * BIPEDAL, totCw = cw * QUADRUPED, totPg = pg * QUADRUPED, totSp = sp * ARACHNID;
        return totCk + totCw + totPg + totSp;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Inserisci numero di galline: ");
        int nChickens = input.nextInt();
        System.out.print("Inserisci numero di mucche: ");
        int nCows = input.nextInt();
        System.out.print("Inserisci numero di maiali: ");
        int nPigs = input.nextInt();
        System.out.print("Inserisci numero di ragni: ");
        int nSpiders = input.nextInt();

        System.out.println("Nella fattoria ci sono " + farm(nChickens, nCows, nPigs, nSpiders) + " zampe");
    }
}