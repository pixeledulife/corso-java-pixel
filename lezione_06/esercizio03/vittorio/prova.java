package lezione_06.esercizio03.vittorio;

import java.util.Scanner;

/*

Il problema della fattoria
Un fattore ci chiede di indicargli quante zampe sono presenti fra i suoi animali.
Il fattore possiede quattro specie:
Gallina = 2 zampe
Mucca = 4 zampe
Maiale = 4 zampe
Ragno = 8 zampe (ricco di proteine e che fa bene all’ambiente)
Il contadino ha contato i suoi animali e ci fornisce il numero di ciascuna specie.
Implementare la funzione che ritorna il numero totale di zampe di tutti gli animali.
Esempi
animali(2, 3, 5,1) ➞ 44

animali(1, 2, 3,0) ➞ 22

animali(5, 2, 8,0) ➞ 50


*/

public class prova {

    static int zampe(int galline, int mucche, int maiali, int ragni){
        return (galline * 2 + mucche * 4 + maiali * 4 + ragni * 8);
    }

    public static void main(String[] args) {
        System.out.println(zampe(2,3,5,1));
    }
}
