ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
tra due numeri dati in input, utilizzarla più volte in un programma

ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input

ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
 creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
 di questi dica se è pari o dispari, ad esempio usando un while ed un if.
