package lezione_06.esercizio02.vittorio;

import java.util.Scanner;

/*

ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
tra due numeri dati in input, utilizzarla più volte in un programma

ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input

ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
di questi dica se è pari o dispari, ad esempio usando un while ed un if.

*/

public class prova {

    static Double sottrazione(double a, Double b) {
        return (a - b);
    }

    static void stamparStinga(String a, int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(a);
        }
        return;
    }

    static boolean isPari(int a) {
        return (a % 2 == 0);
    }

    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username");



        String stringa = myObj.nextLine();
        int n = Integer.parseInt(myObj.nextLine());
        stamparStinga(stringa, n);

        for(int i = 0; i < 10; i++){
            int a = Integer.parseInt(myObj.nextLine());

            System.out.println(isPari(a));
        }

        myObj.close();
    }
}
