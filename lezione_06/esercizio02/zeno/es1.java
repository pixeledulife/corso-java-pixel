package lezione_06.esercizio02.zeno;
import java.util.Scanner;

/*
 * ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
tra due numeri dati in input, utilizzarla più volte in un programma

ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input

ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
 creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
 di questi dica se è pari o dispari, ad esempio usando un while ed un if.
 */
public class es1 {
    public static void main(String[] args) {
        ES_03();
    }
    
        public static int sottrazione (int a, int b){
            return a - b; 
        }

        public static void stampaStringa (String par1, int par2){
            for(int i = 0; i < par2; i++){
                System.out.println(par1);
            }  
        }
        public static boolean pariDispari(int numero){
           
            return (numero % 2 == 0);
        }
        public static void ES_01() {
            
            System.out.println(sottrazione(5, 4));
        }
        public static void ES_02() {
            stampaStringa("yeyeyeye", 4);
        }
        public static void ES_03() {
            Scanner input = new Scanner(System.in);
            int i = 0;
            while(i < 10){
                System.out.println("Inserisci un numero");
                int n = input.nextInt();
                System.out.println(pariDispari(n));
                i++;
            }
            input.close();
        }
}
