import java.util.Scanner;

// ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
// è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
//  creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
//  di questi dica se è pari o dispari, ad esempio usando un while ed un if.


public class esercizio3 {
    static boolean isEven(int x){
        return x % 2 == 0;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        for(int i = 0; i < 10; i++){
            System.out.println("Inserisci un numero: ");
            int n = s.nextInt();
            if (isEven(n)){
                System.out.println(n + " è pari");
            }
            else{
                System.out.println(n + " è dispari");
            }
        }
        s.close();
    }
}
