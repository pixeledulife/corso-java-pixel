import java.util.Scanner;

// ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
// tra due numeri dati in input, utilizzarla più volte in un programma

public class esercizio1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Inserisci a e b: ");
        int a = s.nextInt();
        int b = s.nextInt();
        System.out.println(a + " - " + b + " = " + sottrazione(a, b));

        System.out.println("Inserisci c e d: ");
        int c = s.nextInt();
        int d = s.nextInt();
        System.out.println(c + " - " + d + " = " + sottrazione(c, d));
        s.close();
    }

    static int sottrazione(int a, int b){
        return a - b;
    }
}
