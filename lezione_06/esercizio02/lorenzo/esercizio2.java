import java.util.Scanner;

// ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
// Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input
public class esercizio2 {

    static String stringhe(String a, int n){
        String strf = "";
        for(int i = 0; i < n; i++){
            strf += a;
        }
        return strf;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Inserisci una stringa e il numero di volte che deve essere ripetuta: ");
        String str = s.nextLine();
        int n = s.nextInt();
        System.out.println(stringhe(str, n));
        s.close();
    }
}
