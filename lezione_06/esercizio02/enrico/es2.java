package lezione_06.esercizio02.enrico;

import java.util.Random;

public class es2 {
    /*ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione tra due numeri dati in input, 
    utilizzarla più volte in un programma*/
    public static int esUno(int x, int y) {
        int sottrazione = x - y;
        return sottrazione;
    }

    /*ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
    Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input */
    public static void esDue(String parola, int volte) {
        int i=0;
        while(i<volte){
            System.out.println(parola);
            volte--;
        }
    }

    /*ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
    è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
    creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
    di questi dica se è pari o dispari, ad esempio usando un while ed un if. */
    public static boolean esTre(double n) {
        boolean pari;
            if(n%2==0){
                pari = true;
            }else{
                pari = false;
            }
        return pari;
    }


    public static void main(String[] args) {
        double[] numeri = new double[10];

        for(int i=0; i<10; i++){
            numeri[i] = Math.random()*10;
        }

        for(int x=0; x<10; x++){
            System.out.println(numeri[x]);
        }

        for(int j=0; j<10; j++){
            if(esTre(numeri[j])){
                System.out.println("Pari\n");
            }
            else
                System.out.println("Dispari\n");
        }
    }
}
