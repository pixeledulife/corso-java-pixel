import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Main {

    public static int sottrazione(int a, int b){
        return a - b;
    }

    public static void spam(String s, int n){
        for (int i = 0; i < n; i++) {
            System.out.println(s);
        }
    }

    public static boolean pari(int x){
        if(x % 2 == 0){
            return true;
        }else{
            return false;
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //Ex1
        System.out.print("Inserisci primo numero: ");
        int a = input.nextInt();
        System.out.print("Inserisci secondo numero: ");
        int b = input.nextInt();
        System.out.println(a + " - " + b + " = " + sottrazione(a, b));

        //Ex2
        System.out.print("Inserisci una parola: ");
        String s = input.nextLine();
        System.out.print("Inserisci un numero");
        int n = input.nextInt();
        spam(s, n);

        //Ex3
        for (int i = 0; i < 10; i++) {
            System.out.print("Inserisci un numero: ");
            int x = input.nextInt();
            if(pari(x)){
                System.out.println(x + " è pari");
            }else{
                System.out.println(x + " è dispari");
            }
        }


    }
}