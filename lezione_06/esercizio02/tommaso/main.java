package lezione_06.esercizio02.tommaso;

/**
 * main
 */
public class main {
    public static void main(String[] args) {
        sottrazione(5, 4);
        sottrazione(3, 2);
    }

    /**
     * int sottrazione
     */
    public static int sottrazione(int num1, int num2) {
        return num1 - num2;
    }
}