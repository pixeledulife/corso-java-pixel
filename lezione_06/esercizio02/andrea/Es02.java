import java.util.Scanner;

import javax.sound.sampled.SourceDataLine;

/*ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
tra due numeri dati in input, utilizzarla più volte in un programma

ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input

ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
 creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
 di questi dica se è pari o dispari, ad esempio usando un while ed un if.


 */

 public class Es02{
    public static void main(String[] args) {
       /*  boolean prova = pariDispari(3);
        System.out.println(prova);*/

        /* 
        printString("Ciao", 3);
        */
        System.out.println("Inserisci 10 numeri da tastiera");

        Scanner sc = new Scanner (System.in);
        int [] values = new int [10];
        for(int i = 0; i < values.length; i++){
            values[i] = sc.nextInt();

        }
        System.out.println("***");
        for (int i = 0; i < values.length; i++){
            boolean isEven = pariDispari(values[i]);
            if(isEven){
                System.out.println(values[i] + " è pari");
            }
            else{
                System.out.println(values[i] + " è dispari");
            }
        }
        sc.close();
    }





    public static int sottrazione(int a, int b){
        int result = a - b;
        return result;
    }
    public static void printString(String a, int b){
        int i = 0;
        while(i < b){
            System.out.println(a);
            i++;
        }
    }
    public static boolean pariDispari(int n){
        if(n%2 == 0){
            return true;
        }
        else{
            return false;
        }
    }

 }
