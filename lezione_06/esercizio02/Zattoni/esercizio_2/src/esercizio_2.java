
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 */

/*
*   ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
*   tra due numeri dati in input, utilizzarla più volte in un programma
*
*   ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
*   Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input
*
*   ES 03) [MEDIO] Creare una funzione che dica se un numero, dato in input, 
*   è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
*   creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
*    di questi dica se è pari o dispari, ad esempio usando un while ed un if.
*/

public class esercizio_2 {
    
    public static int sottrazione(int num_1, int num_2){
        /*
        *   ES 01) Creare una funzione “sottrazione” che stampi il risultato della sottrazione
        *   tra due numeri dati in input, utilizzarla più volte in un programma
        */
        int result;
        result = num_1 - num_2;
        return result;
    }
    
    public static void ripeti(String valore, int num){
        /*
        *   ES 02) Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
        *   Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input
        */
        for(int i=0; i<num;i++){
            System.out.print(valore+" ");
        }
    }
    
    public static boolean pari(int numero){
        return(numero%2==0);
    }
    
    public static void main(String[]args){
        //PARTE_1
        
        int num_1 = 20;
        int num_2 = 10;
        Scanner input = new Scanner(System.in);
        try{
            System.out.print("Inserisci il numero_1: ");
            num_1 = input.nextInt();
            System.out.print("Inserisci il numero_2: ");
            num_2 = input.nextInt();
        }catch(Exception e){
            System.out.println("Errore: "+e);
        }
        System.out.println("La sottrazione tra "+num_1+" e "+num_2+" e' "+sottrazione(num_1,num_2));
        
        //PARTE_2
        
        ripeti("ciao", 5);
        System.out.print("\n");
        
        //PARTE_3
        try{
            String result ="";
            for(int i = 0; i<10;i++){
                System.out.print("Il numero: ");
                result = (pari(input.nextInt()))? "e' pari": "e' dispari";
                System.out.print(result+"\n");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        input.close();
    }
    
}
