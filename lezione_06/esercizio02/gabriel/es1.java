import java.util.*;


// Creare una funzione “sottrazione” che stampi il risultato della sottrazione
// tra due numeri dati in input, utilizzarla più volte in un programma

public class es1{

    static int sottrazione(int a, int b){
        return a - b;
    }

public static void main(String[] args) {
    Scanner s = new Scanner(System.in);

    System.out.println("inserisci due numeri");
    int a = s.nextInt();
    int b = s.nextInt();
    int sott = sottrazione(a, b);

    System.out.println(sott);

    s.close();
}
}
