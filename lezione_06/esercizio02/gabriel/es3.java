import java.util.Scanner;

public class es3 {
    
    //Creare una funzione che dica se un numero, dato in input, 
    //è pari o dispari, in caso sia pari ritorna True, altrimenti ritorna False;
    //creare quindi un programma che legga da tastiera 10 numeri e per ognuno 
    //di questi dica se è pari o dispari, ad esempio usando un while ed un if.

    static boolean pari_dispari(int n1){
        if(n1 % 2 == 0){
            return true;
        }
        else{
            return false;
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        for(int i = 0; i < 10; i++){
            System.out.println("numero?");
            int n1 = s.nextInt();
            System.out.println(pari_dispari(n1));
        }
        
        s.close();
    }


}
