import java.util.Scanner;

public class es2 {
    
//Crea una funzione che stampa una stringa (parametro 1) n volte (parametro 2). 
//Creare sotto il codice per testare la funzione con una stringa, e un numero, dati in input

    static String  ripeti (String s1, int n2){
        String sf = "";
        while(n2 > 0){
            sf += s1;
            n2 -= 1;
        }
        return sf;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("parola e numero di volte da ripetere");
        String s1 = s.nextLine();
        int n2 = s.nextInt();
        System.out.println(ripeti(s1, n2));
        s.close();
    }


}
