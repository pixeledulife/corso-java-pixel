import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Quanti anni hai: ");
        int anni = scanner.nextInt();
        System.out.println("Hai " + anni + " anni");
        scanner.close();
    }
}
