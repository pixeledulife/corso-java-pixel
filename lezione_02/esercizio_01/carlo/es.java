package lezione_02.esercizio_01.carlo;

import java.util.Scanner;

class Main {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username");
        //Read User input
        int anni = myObj.nextInt();
        System.out.println("Hai "+ anni+" anni");
        myObj.close();
    }
}