import java.util.Scanner;

class Main 
{
	public static void main(String[] args)
	{
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci il nome");
        String nome = sc.nextLine();
        System.out.println("Inserisci il cognome");
        String cognome = sc.nextLine();

        System.out.println("Buonasera " + nome + " " + cognome + ", cosa posso fare per te?");

        sc.close();
	}

}