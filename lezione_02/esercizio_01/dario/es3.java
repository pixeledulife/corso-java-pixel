import java.util.Scanner;

class Main 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);

        int primo = sc.nextInt();
        int secondo = sc.nextInt();

        if(primo % 2 == 0 && secondo % 2 == 0 )
        {
            System.out.println("Il numero " + primo + " e il numero " + secondo + " sono pari");
        }
        else if(primo % 2 == 0 && secondo % 2 != 0)
        {
            System.out.println("Il numero " + primo + " è pari, invece il numero " + secondo + " è dispari");
        }
        else if(primo % 2 != 0 && secondo % 2 == 0)
        {
            System.out.println("Il numero " + primo + " è dispari, invece il numero " + secondo + " è pari");
        }
        else
        {
            System.out.println("Il numero " + primo + " e il numero " + secondo + "sono entrambi dispari");
        }
        sc.close();
	}
}