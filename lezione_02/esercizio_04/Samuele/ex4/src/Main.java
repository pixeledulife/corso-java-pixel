import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //ex1
        /*
        System.out.println("Inserisci un numero: ");
        int a = sc.nextInt();

        if (a % 2 == 0){
            System.out.println("Il numero è PARI");
        }else{
            System.out.println("Inserisci un altro  numero: ");
            a = sc.nextInt();

            if (a % 2 == 0){
                System.out.println("Primo è DISPARI, il secondo PARI");
            }else{
                System.out.println("Entrambi in numeri sono DISPARI");
            }
        }
         */

        //ex2
        System.out.println("Inserisci 3 numeri: ");

        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int max;
        int min;

        //1 modo
        /*
        if(a > b){
            max = a;
            min = b;

        }else{
            max = b;
            min = a;

        }

        if(c > max){
            max = c;

        }else{
            if(c < min){
                min = c;

            }
        }
         */

        if(a > b && a > c){
            max = a;

            if(b > c){
                min = c;
            }else{
                min = b;
            }

        }else if(b > a && b > c){
            max = b;

            if(a > c){
                min = c;
            }else{
                min = a;
            }

        }else{
            max = c;

            if(a > b){
                min = b;

            }else{
                min = a;
            }

        }

        System.out.println("N max: " + max);
        System.out.println("N min: " + min);

    }
}