package lezione_02.esercizio_04.lavagna;

import java.util.Scanner;

/*

-Leggere un numero da terminale e controllare se è pari;
 se lo è stampate “pari” a terminale, altrimenti leggete un secondo numero.
  Se il secondo numero è dispari stampate “entrambi dispari”, 
  altrimenti stampate “primo dispari, secondo pari”
-Dati tre numeri identificare il minimo e il massimo
-Si realizzi un programma che acquisisca da tastiera un numero
 e stampi il valore assoluto di tale numero.
-Scrivere un programma che legge un numero compreso tra 1 e 5 e controlla se è primo.

 */
public class esercizio {
    public static void main(String[] args) {
        esercizioB();
    }

    public static void esercizioA() {
        Scanner input = new Scanner(System.in);
        System.out.println("Esercizio 03");
        System.out.println("Numero?");

        int n = input.nextInt();

        if (n % 2 == 0) {
            System.out.println("Pari");
        } else {
            System.out.println("Secondo numero?");
            n = input.nextInt();
            if (n % 2 == 0) {
                System.out.println("primo dispari, secondo pari");
            } else {
                System.out.println("entrambi dispari");
            }
        }
        input.close();

    }

    /* Dati tre numeri identificare il minimo e il massimo */
    public static void esercizioB() {
        int a = 3;
        int b = 4;
        int c = 7;

        if (a >= b && a >= c) {
            System.out.println("a è il massimo.");
        } else if (b >= a && b >= c) {
            System.out.println("b è il massimo.");
        } else {
            System.out.println("c è il massimo.");
        }
        if (a <= b && a <= c) {
            System.out.println("a è il minimo.");
        } else if (b <= a && b <= c) {
            System.out.println("b è il minimo.");
        } else {
            System.out.println("c è il minimo.");
        }
    }

    /*
     * Si realizzi un programma che acquisisca da tastiera un numero
     * e stampi il valore assoluto di tale numero.
     */
    public static void esercizioC() {

    }

    /*
     * Scrivere un programma che legge un numero compreso tra 1 e 5 e controlla se è
     * primo.
     */
    public static void esercizioD() {

    }

}