import java.util.*;

public class es6 {

    public static void main(String[] args) {  

        Scanner sc = new Scanner(System.in);
        System.out.println("scivi tre numeri");
        
        int n1 = sc.nextInt();
        int n2 = sc.nextInt();
        int n3 = sc.nextInt();

        Integer[] array = {n1, n2, n3};

        List<Integer> ints = Arrays.asList(array);
        System.out.println(Collections.max(ints));
        System.out.println(Collections.min(ints));
        
        sc.close();


    }
}


