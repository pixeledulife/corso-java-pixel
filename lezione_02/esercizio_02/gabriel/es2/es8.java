import java.util.*;

public class es8 {
    public static void main(String[] args) {  

        Scanner sc = new Scanner(System.in);
        System.out.println("scivi un numero tra uno e 5");
        
        int n = sc.nextInt();
        
        if(n > 0){
            System.out.println("valore assoluto " + n);
        }
        else{
            n = n * -1;
            System.out.println("valore assoluto " + n);
        }
        
        sc.close();


    }
}
