package esercizio_03;
import java.util.Scanner;
import java.util.function.Function;

class Main {

    public static void nome(Scanner s){
        System.out.println("Inserisci nome: ");
        String nome = s.nextLine();
        System.out.println("Inserisci Cognome: ");
        String cognome = s.nextLine();

        System.out.println("Buonasera "+nome+" "+cognome+", cosa posso fare per te?");
    }

    public static void ultimaCifra(Scanner s){
        System.out.println("Inserire numero: ");
        int num = s.nextInt();
        int ultimo = num % 10;

        System.out.println("L'ultima cifra è: "+ultimo);
    }

    public static void confronto(Scanner s){
        System.out.println("Inserisci prima stringa: ");
        String s1 = s.nextLine();

        System.out.println("Inserisci seconda stringa: ");
        String s2 = s.nextLine();

        if(s1.equals(s2)){
            System.out.println("Sono uguali!");
        }
        else{
            System.out.println("Sono diverse!");
        }
    }

    public static void pari(Scanner s){
        System.out.println("Inserisci numero: ");
        int numero = s.nextInt();

        if(numero % 2 == 0){
            System.out.println("Pari");
        }
        else{
            System.out.println("Dispari");
        }
    }

    public static void min_max(Scanner s){
        System.out.println("Numero 1: ");
        int n1 = s.nextInt();
        System.out.println("Numero 2: ");
        int n2 = s.nextInt();
        System.out.println("Numero 3: ");
        int n3 = s.nextInt();
        int max = 0;
        int min = 0;
        if(n1 >= n2 && n1 >= n3){
            max = n1;
        }
        else{
            if(n2 >= n1 && n2 >= n3){
                max = n2;
            }
            else{
                if(n3 >= n1 && n3 >= n2){
                    max = n3;
                }
            }    
        }

        
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
    }
}
