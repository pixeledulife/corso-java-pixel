import java.util.Scanner;

class Main{
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);

        System.out.println("Inserisci anni: ");
        int anni = s.nextInt();

        System.out.println("Hai "+anni+" anni!");

        s.close();
    }
}
