import java.util.Scanner;

class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Inserisci password: ");

        String tentativo = sc.nextLine(); // Read user input

        if (tentativo.equals("ciao123")) {
            System.out.print("Password corretta");
        } else {
            System.out.println("Password non corretta");
        }

        sc.close();

    }
}