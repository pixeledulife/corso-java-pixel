import java.util.Scanner;

public class Es6{
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Inserisci un numero:");
        int n1 = s.nextInt();

        if(n1%2 == 0){
            System.out.println("il numero è pari");
        }
        else {
            System.out.println("Il numero è dispari, inserisci un numero:");
            int n2 = s.nextInt();
            if(n2%2 != 0){
                System.out.println("entrambi dispari");
            }
            else{
                System.out.println("il numero è pari");

            }
        }

        s.close();
    }
    
}