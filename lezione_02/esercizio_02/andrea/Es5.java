import java.util.Scanner;

public class Es5{
    public static void main (String[] args){
        Scanner s = new Scanner(System.in);
        System.out.println("Inserisci la prima stringa:");
        String s1 = s.nextLine();
        System.out.println("Inserisci la seconda stringa:");
        String s2 = s.nextLine();
        System.out.println("Inserisci la terza stringa:");
        String s3 = s.nextLine();
        if((s1.equals(s2) && s2.equals(s3))){
            System.out.println("Le stringhe sono uguali");
        }
        else{
            System.out.println("Le stringhe sono diverse");

        }

        s.close();
    }
}