/*

 -Leggere un numero da terminale e controllare se è pari;
 se lo è stampate “pari” a terminale, altrimenti leggete un secondo numero.
  Se il secondo numero è dispari stampate “entrambi dispari”, 
  altrimenti stampate “primo dispari, secondo pari”
-Dati tre numeri identificare il minimo e il massimo
-Si realizzi un programma che acquisisca da tastiera un numero
 e stampi il valore assoluto di tale numero.
-Scrivere un programma che legge un numero compreso tra 1 e 5 e controlla se è primo.

 */
import java.util.Scanner;

public class es4 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Numero: ");
        int n = input.nextInt();

        if(n%2 == 0){
            System.out.println("Pari");
        }else{
            System.out.println("Dammi un secondo numero: ");
            n = input.nextInt();
            if(n % 2 == 0){
                System.out.println("Primo dispari e il secondo e' pari");
            }else{
                System.out.println("Entrambi dispari!!!");
            }          
        }
        input.close();
    }
}
