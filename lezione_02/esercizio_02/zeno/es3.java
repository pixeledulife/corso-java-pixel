/*

1- Scrivere un programma che legge il vostro nome e stampa
   “Buonasera [vostro nome e cognome], cosa posso fare per te?”
2- Leggere un numero in input e stamparne l’ultima cifra.
3- Leggere due stringhe da terminale e controllare se sono uguali

 */
import java.util.Scanner;

class es3{


    public static void esA(String[] args) {
        Scanner sc = new Scanner(System.in);
        //esercizio nome
        System.out.println("Scrivi il tuo nome: ");
        String nome = sc.nextLine(); // read user input
        System.out.println("Buonasera "+nome+", cosa possso fare per te?");

        //esercizio numero
        int numero = sc.nextInt();
        System.out.println("inserisci un numero");
        numero = numero % 10;
        System.out.println("L'ultima cifra del numero e': "+numero);
        

        //esercizio che controlla se due stringhe sono uguali
        System.out.println("Scrivi la prima stringa:  ");
        String first = sc.nextLine();
        System.out.println("Scrivi la seconda stringa: ");
        String second = sc.nextLine();

        if(first.equals(second)){
            System.out.println("Le stringhe inserite sono UGUALI");
        }else{
            System.out.println("Le stringhe inserite sono DIFFERENTI");

        }
    sc.close();
    }
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        String str1 = input.nextLine();
        System.out.println("Inserisci la stringa 1: ");
        String str2 = input.nextLine();
        System.out.println("Inserisci la stringa 2: ");
        String str3 = input.nextLine();
        System.out.println("Inserisci la stringa 3: ");

        if(str1.equals(str2) && str2.equals(str3)){
            System.out.println("tutte le stringhe sono uguali");
        }else{
            System.out.println("Sono diverse!!");
        }
        input.close();
    }

}
