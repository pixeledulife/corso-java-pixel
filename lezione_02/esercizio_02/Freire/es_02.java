import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisce password...");
        String tentativo = sc.nextLine(); // Read user input

        if (tentativo.equals("ciao123")) {
            System.out.println("Password corretta!");
        } 
        else {
            System.out.println("Password errata!");
        }

        sc.close();
    }
}