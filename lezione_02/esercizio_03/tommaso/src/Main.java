import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        es3();

    }

    public static void es1() {
        Scanner input = new Scanner(System.in);

        System.out.println("Nome?");
        String nome = input.nextLine();

        System.out.println("Cognome?");
        String cognome = input.nextLine();

        System.out.println("Buonasera " + nome + " " + cognome + ", come posso aiutarti?");
    }

    public static void es2() {
        Scanner input = new Scanner(System.in);

        System.out.println("Numero?");
        int num = Integer.parseInt(input.nextLine());

        System.out.println(num % 10);
    }

    public static void es3() {
        Scanner input = new Scanner(System.in);

        System.out.println("Stringa?");
        String s1 = input.nextLine();
        System.out.println("Stringa?");
        String s2 = input.nextLine();


        System.out.println(s2.equals(s1) ? "Sono uguali" : "Non sono uguali");
    }
}