package lezione_02.esercizio_03.lavagna;
import java.util.Scanner;
/*
 -Scrivere un programma che legge il vostro nome e stampa
    “Buonasera [vostro nome e cognome], cosa posso fare per te?”
 -Leggere un numero in input e stamparne l’ultima cifra.
 -Leggere tre stringhe da terminale e controllare se sono uguali
 */
public class esercizio{
    public static void main(String[] args){
    
    }

    public static void esercizioC(){
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci stringa 1");
        String uno = input.nextLine();
        System.out.println("Inserisci stringa 2");
        String due = input.nextLine();
        System.out.println("Inserisci stringa 3");
        String tre = input.nextLine();

        if (uno.equals(due) && due.equals(tre)){
            System.out.println("Le tre stringhe sono uguali");
        }

        else{
            System.out.println("Le tre stringhe non sono uguali");
        }
        input.close();
    }
    public static void esercizioA(){
        Scanner input = new Scanner(System.in);
        System.out.println("Esercizio 03");
        System.out.println("Nome?");

        String nome = input.nextLine();

        System.out.println("Cognome?");

        
        String cognome = input.nextLine();

        System.out.println("Buonasera " + nome + " " + cognome + ", cosa posso fare per te?");
        input.close();

    }
    public static void esercizioB(){

        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        System.out.println(number%10);
        input.close();

    }
}