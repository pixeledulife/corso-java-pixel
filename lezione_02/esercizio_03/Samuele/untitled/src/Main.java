import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //ex 1
        /*
        System.out.println("Inserisci nome e cognome: ");
        String name = sc.nextLine();
        String surname = sc.nextLine();
        System.out.println("Buona sera " + name + " " + surname + " cosa posso fare per te?");
         */

        //ex2
        /*
        System.out.println("Inserisci un numero: ");
        int n = sc.nextInt() % 10;
        System.out.println("Ultima cifra: " + n);
         */

        //ex3
        System.out.println("Inserisci tre stringhe: ");
        String a = sc.nextLine();
        String b = sc.nextLine();
        String c = sc.nextLine();

        if (a.equals(b) && a.equals(c)){
            System.out.println("Le stringhe sono uguali!");
        }else{
            System.out.println("Le stringhe NON sono uguali");
        }
    }
}