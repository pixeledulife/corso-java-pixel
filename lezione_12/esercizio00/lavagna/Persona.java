public abstract class Persona {
    String nome;
    String cognome;

    public Persona(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    public abstract String getNome();
    public abstract void setNome(String nome);
    public abstract String getCognome();
    public abstract void setCognome(String cognome);


    @Override
    public String toString(){

        return this.getNome()+", "+this.getCognome();
    }
}
