import java.util.ArrayList;

public class Studente extends Persona {
    private String classe;
    private ArrayList<Materia> materie;

    public Studente(String nome, String cognome, ArrayList<Materia> materie, String classe) {
        super(nome, cognome);
        this.materie = materie;
        this.classe = classe;
    }

    @Override
    public String getCognome() {
        return this.nome;
    }

    @Override
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    @Override
    public String getNome() {
        return this.cognome;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return super.toString()+this.materie.toString();
    }
}
