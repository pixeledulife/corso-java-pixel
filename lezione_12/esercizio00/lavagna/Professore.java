import java.util.ArrayList;

public class Professore extends Persona{
    ArrayList<String> materie = new ArrayList<String>();
    ArrayList<String> classi = new ArrayList<String>();

    public Professore(String nome, String cognome, ArrayList<String> materie, ArrayList<String> classi) {
        super(nome, cognome);
        this.materie = materie;
        this.classi = classi;
    }

    @Override
    public String getCognome() {
        return this.nome;
    }

    @Override
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    @Override
    public String getNome() {
        return this.cognome;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString(){
        return super.toString()+this.materie.toString()+this.classi.toString();
    }
}
