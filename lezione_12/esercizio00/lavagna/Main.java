/*
    Creare un programma che chieda ad uno studente di inserire una materia.
    Chiedere di inserire la media che ha negli scritti,
    la media che ha negli orali ed infine un voto con il quale ha provato a recuperare tale materia.
    Si hanno quindi 3 voti.
    Il programma usa una funzione per calcolare la media scritto + orale + recupero.
    Infine usa una per stampare una stringa contenente il voto
    ottenuto dalla media (scritto + orale + recupero) e la materia.
    Risolvere il problema utilizzando classi e ArrayList
    EG.
    utente digita: Inglese
    utente digita (scritto): 3
    utente digita (orale): 3
    utente digita (recupero): 6
    Programma stampa: "4 Inglese”

    Si può estendere il programma precedente permettendo allo studente
    di fare il calcolo per più materie
    Si può estendere il programma creando la classe “Persona” superclasse
    sia di Studente che di Professore,
    il quale non ha voti ma un arraylist di Stringhe contenenti le materie insegnate.
    Lo studente ha più materie, delle quali si può sapere la media invocando un metodo.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        /*
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserire il nome di una materia");
        String materia = sc.nextLine();
        System.out.println("Inserire voto parte scritta");
        double scritti = Double.parseDouble(sc.nextLine());
        System.out.println("Inserire voto parte orale");
        double orale = Double.parseDouble(sc.nextLine());
        System.out.println("Inserire voto parte recupero");
        double recupero = Double.parseDouble(sc.nextLine());
        Materia mat = new Materia(materia, scritti, orale, recupero);
        System.out.println("La media di "+mat.getNome()+" è "+mat.getMedia());
        */

        String nome = "Harry";
        String cognome = "Potter";
        ArrayList<Materia> rendimento = new ArrayList<>();
        rendimento.add(new Materia("Pozioni", 4,5,3));
        rendimento.add(new Materia("Difesa contro le arti oscure", 9,9,10));
        String classe = "Grifondoro";
        Studente stud = new Studente(nome, cognome, rendimento, classe);

        String nome2 = "Severus";
        String cognome2 = "Piton";
        ArrayList<String> materie_insegnate=new ArrayList<>();
        materie_insegnate.add("Pozioni");
        materie_insegnate.add("Difesa contro le arti oscure");
        ArrayList<String> classi=new ArrayList<>();
        classi.add("Tassorosso");
        Professore prof = new Professore(nome2, cognome2, materie_insegnate, classi);
        System.out.println(stud);
        System.out.println(prof);
        return;
    }
}