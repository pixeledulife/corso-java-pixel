public class Materia {
    private final String nome;
    private final double scritti, orale, recupero;
    private double media;

    public Materia(String nome, double scritti, double orale, double recupero) {
        this.nome = nome;
        this.scritti = scritti;
        this.orale = orale;
        this.recupero = recupero;
        this.calcolaMedia();
    }
    private void calcolaMedia(){
        media = (this.scritti+this.orale+this.recupero)/3;
        media = Math.round(media*100.0)/100.0;
    }

    public String getNome() {
        return this.nome;
    }

    public double getMedia() {
        return this.media;
    }
    @Override
    public String toString(){
        return this.nome+" "+ this.getMedia();
    }
}
