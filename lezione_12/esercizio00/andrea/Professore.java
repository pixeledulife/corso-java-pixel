import java.util.ArrayList;

public class Studente extends Persona{
    ArrayList<String> materie;
    ArrayList<String> classi;


    public Studente(String nome, String cognome, ArrayList<String> materie,ArrayList<String> classi ){

        super(nome,cognome);
        this.materie = materie;
        this.classi = classi;
        

    }
    public String getNome(){
        return this.nome;
    }
    public String getCognome(){
        return this.cognome;
    }
    public void setNome(String nome){
        this.nome = nome;
  
    }
    public void setCognome(String cognome){
        this.cognome = cognome;
  
    }


}
