import java.util.ArrayList;

public class Studente extends Persona{
    private ArrayList<Materia> materie;



    public Studente(String nome, String cognome){
        super(nome,cognome);

    }
    public String getNome(){
        return this.nome;
    }
    public String getCognome(){
        return this.cognome;
    }
    public void setNome(String nome){
        this.nome = nome;
  
    }
    public void setCognome(String cognome){
        this.cognome = cognome;
  
    }


}
