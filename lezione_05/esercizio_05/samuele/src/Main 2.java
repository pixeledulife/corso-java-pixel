import java.util.Scanner;

public class Main {

    public static void es1(){
        for (int i = 0; i < 100; i++) {
            System.out.println("Topo Gigio");
        }
    }

    public static  void es2(){
        for (int i = 1; i <= 20; i++) {
            System.out.println(i);
        }
    }

    public static void es3(){
        System.out.println("Numeri pari: ");
        for (int i = 1; i <= 20; i++) {
            if((i % 2) == 0) System.out.print(i);
        }

        System.out.println(" \n");
        System.out.println("Numeri dispari: ");
        for (int i = 1; i <= 20; i++) {
            if((i % 2) != 0) System.out.print(i);
        }
    }

    public static void es4(){
        Scanner input = new Scanner(System.in);

        System.out.print("Inserisci numero: ");
        int n = input.nextInt();

        if((n % 3) == 0){
            for (int i = 0; i < 10; i++) {
                System.out.println("Osteria numero " + i);
            }
        }else{
            for (int i = 0; i < 10; i++) {
                System.out.println("Il numero non è divisibile per 3!");
            }
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String pw = " ";
        String answ = " ";

        do{
            System.out.println("Parola d'ordine: ");
            answ = input.nextLine();
        }while(!answ.equals(pw));
        System.out.println("Password corretta");
    }
}