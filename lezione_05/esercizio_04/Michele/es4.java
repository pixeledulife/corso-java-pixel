import java.util.Scanner;

/**
 * es4
 */
public class es4 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Inserisci la lunghezza: ");
        int n = s.nextInt();

        for (int i = 1; i <= n; i++) {
            // Ciclo interno per i caratteri
            for (int k = 1; k <= 2*i-1; k++) {
                System.out.print("*");
            }
            System.out.println();

    }
}
}