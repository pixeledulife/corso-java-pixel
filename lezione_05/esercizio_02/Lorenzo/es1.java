import java.util.*;
public class es1 {

//  Leggere un array di interi di 10 posizioni
//  e stampare il numero che compare più volte 
//  all’interno dell’array, qualora ci siano 
//  più numeri che compaiano lo stesso numero 
//  di volte stampare quello che compare per primo.
public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    int[]num = new int [10];
    for (int i = 0; i < 10; i++){
        System.out.print(i + ": ");
        num[i] = s.nextInt();

            }
    s.close();

    int[]contatori = new int [10];

    for (int i = 0; i < 10; i++){        
        for (int j = 0; j < 10; j++){
            if(num[i] == num[j]){
                contatori[i]++;
            }
        }
    }
    int max = num[0];
    int index = 0;
    for (int i = 0; i < num.length; i++){
        if (num[i] >= max){
            max = num[i];
            index = i;
        }
    }
    System.out.println("il numero che è ripetuto più volte è il " + num[index]);
}
}