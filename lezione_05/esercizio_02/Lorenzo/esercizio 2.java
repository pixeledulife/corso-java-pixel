public class esercizio {
    public static void main(String[] args) {
        int n = 1234;
        String reverse = "";
        while (n != 0){
            reverse += n % 10;
            n = n / 10;
        }
        System.out.println(reverse);
    }
}
