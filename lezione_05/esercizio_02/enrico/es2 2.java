package lezione_05.esercizio_02.enrico;
import java.util.*;

public class es2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int numero;

        System.out.println("Inserisci un numero: ");
        numero = s.nextInt();

        while(numero != 0){
            int n = numero % 10;
            numero = numero/10;
            System.out.print(n);
        }
    }
}
