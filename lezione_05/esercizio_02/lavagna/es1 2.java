package lezione_05.esercizio_02.lavagna;

import java.util.Scanner;

/*
 -Leggere un array di interi di 10 posizioni
 e stampare il numero che compare più volte 
 all’interno dell’array, qualora ci siano 
 più numeri che compaiano lo stesso numero 
 di volte stampare quello che compare per primo.
 */
public class es1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int array[] = new int[10];

        for(int i=0; i<10; i++){
            array[i] = sc.nextInt();
        }
        sc.close();

        int contatore[] = new int[10];

        for(int j=0; j<10; j++){
            contatore[j]=0;
        }

        for(int j=0; j<10; j++){

            for(int i=0; i<10; i++){
                if(array[j] == array[i]){
                    contatore[j]++;
                }
            }
        }

        int max = contatore[0];
        int pos = 0;
        for(int i=0; i<contatore.length; i++) {
            if(contatore[i] > max) {
                max = contatore[i];
                pos = i;
            }
        }
        System.out.println("Il numero che compare più volte è "+array[pos]);

    }
}
