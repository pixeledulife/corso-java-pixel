
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 -Leggere un array di interi di 10 posizioni
 e stampare il numero che compare più volte 
 all’interno dell’array, qualora ci siano 
 più numeri che compaiano lo stesso numero 
 di volte stampare quello che compare per primo.
 
-Leggere  un array di 10 interi e stampare
 solo i numeri che appaiono nell’array una 
 volta soltanto. Ad esempio se l’array 
 contiene  1, 2, 3, 1, 2, 4  il programma stamperà  3, 4
 
-Creare un array di interi con 8 posizione 
e riempirlo con numeri casuali compresi 
tra 1 e 90 senza ripetizioni. Stampare il 
contenuto man mano il contenuto dell’array.
 */
public class esercizio_2 {
    public static int[] recupera(int numero){
        int[] result = new int [numero];
        try{
            Scanner input = new Scanner(System.in);
            for(int i = 0; i<numero;i++){
                System.out.print("Inserisci il numero "+(i+1)+" : ");
                result[i] = input.nextInt();
            
            }
            input.close();
        }catch(Exception e){
            System.out.println("Si e' verificato un errore, si usera' un array di default.");
            for(int i = 0; i<numero;i++){
                result[i] = i;
            
            }
        }
        
        return result;
    }
    
    public static int compare(int[] numeri){
        int[] result = new int[numeri.length];
        for(int i = 0; i<numeri.length;i++){
            for(int posizione:numeri){
                if(numeri[i] == posizione){
                    result[i] += 1;
                }
            }
        }
        return numeri[numeroMassimo(result)];
    }
    
    public static String toString(int[] numero){
        String result = "Il tuo array e' -> ";
        for(int i = 0;i<numero.length;i++){
            result += (numero[i]+" ");
        }
        return result;
    }
    
    public static int numeroMassimo(int[] numeri){
        int result = 0;
        for(int i = 0;i<numeri.length;i++){
            if(result < numeri[i]){
                result = i;
            }
        }
        return result;
    }
    
     public static String toString(int numero){
        String result = "Il numero che accompare più volte e' -> "+numero;
        return result;
    }
    
    public static String toString(boolean test){
        if(test){
            return "L'array contiene un numero pari";
        }else{
            return "L'array non contiene un numero pari";
        }
    }
    
    
    public static void main(String[]args){
        System.out.println(toString(new int[10]));
        System.out.println(toString(compare(recupera(10))));
    }
    
}
