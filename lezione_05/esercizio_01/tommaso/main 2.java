package lezione_05.esercizio_01.tommaso;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        //es1(numbers);
        //es2(numbers);
        //es3(numbers);
        //es4()
        //es5();
        es6();
    }

    public static void es1() {
        Integer[] numbers = new Integer[10];
        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = i;
        }
        for (int num:numbers){
            System.out.println(num);
        }
    }

    public static void es2() {
        Integer[] numbers = new Integer[10];
        Scanner in = new Scanner(System.in);
        Integer sum = 0; 
        for(int i = 0; i < numbers.length; i++) {
            System.out.println("Inserisci un numero");
            Integer num = Integer.parseInt(in.nextLine());
            if(num < 0) {
                sum += num;
                numbers[i] = 0;
            } else {
                numbers[i] = num;
            }
        }
        System.out.println("Somma dei numeri negativi: "+ sum);
        for(int num:numbers) {
            System.out.println(num);
        }
    }

    public static void es3() {
        Integer[] numbers = new Integer[10];
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < numbers.length; i++) {
            System.out.println("Inserisci un numero");
            Integer num = Integer.parseInt(in.nextLine());
            numbers[i] = num;
        }

        Integer min = numbers[0]; 
        Integer max = numbers[0];

        for(int num:numbers) {
            if(num < min) {
                min = num;
            }

            if(num > max) {
                max = num;
            }
        }
        System.out.println("The higher number is: " + max);
        System.out.println("The lower number is: " + min);
        System.out.println("-------");
        for(int num:numbers) {
            System.out.println(num);
        }
    }

    public static String es4(Integer[] numbers) {
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < numbers.length; i++) {
            System.out.println("Inserisci un numero");
            Integer num = Integer.parseInt(in.nextLine());
            if( i % 2 == 0 && !(num % 2 == 0)){
                return "Non tutte le posizioni pari hanno numeri pari";
            }
        }
        return "Tutte le posizioni pari hanno numeri pari";
    }

    public static void es5(){
        Integer[] numbers = new Integer[10];

        int max = 0;
        int cont = 0;
        int idxNum = 0;
        for(Integer nums:numbers) {
            for(Integer num:numbers) {
                if(nums == num) {
                    cont++;
                }
            }
            if(cont > max) {
                max = cont;
                idxNum = nums;
            } 
        }
        System.out.println("the number " + idxNum + " appears " + max + " times");
    }

    public static void es6() {
        Scanner in = new Scanner(System.in);
        System.out.println("Input: ");
        int num = Integer.parseInt(in.nextLine());
        String numS = "";
        while(num != 0) {
            numS += num % 10;
            num /= 10;
        }
        System.out.println(numS);

    }
}
