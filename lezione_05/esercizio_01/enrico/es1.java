package lezione_05.esercizio_01.enrico;
import java.util.*;

class es1{
    public static void es1() {
        int[] numeri = {};

        for(int i = 0; i<10; i++){
            numeri[i] = i;
        }
        for(int j=0; j<numeri.length; j++){
            System.out.println(numeri[j]);
        }
    }

    public static void es2() {
        int[] numeri = {-1,2,-4,3,5,7,-6,8,-9};
        int somma = 0;

        for(int j = 0 ; j < numeri.length; j++){
            if(numeri[j] < 0) {
                somma += numeri[j];
                numeri[j] = 0;
            }
        }

        for(int x = 0; x<numeri.length; x++){
            System.out.println(numeri[x]);
        }
        
        System.out.println("La somma dei valori negativi è: "+somma);
    }

    public static void es3() {
        int[] numeri = {-1,2,-4,3,5,6,7,8,10,1,2};
        int max = 0, posizione = 0;
        for(int i=0; i<numeri.length-1; i++){
            if(numeri[i] > max){
                max = numeri[i+1];
                posizione = i+1;
            }
        }
        System.out.println("Il max è: "+max+" in posizione: "+posizione);
    }

    public static void es4() {
        int[] numeri = {2,4,6,8,0,10,12,14};
        boolean dispari = false;

        for(int i=0;i<numeri.length;i++){
            if(numeri[i] % 2 != 0){
                dispari = true;
            }
        }

        if(dispari){
            System.out.println("Non sono tutti pari");
        }
        else{
            System.out.println("Sono tutti pari");
        }
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        es4();
    }
}