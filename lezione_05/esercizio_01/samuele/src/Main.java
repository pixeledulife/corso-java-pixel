import java.util.Scanner;

import static java.lang.String.valueOf;

public class Main {
    public static void es1(){
        int[] n = new int[10];
        for (int i = 0, j = 1; i < 10; i++, j++) {
            n[i] = j;
            System.out.println(n[i]);
        }
    }

    public static void es2(){
        Scanner sc = new Scanner(System.in);
        int[] n = new int[8];
        int somma = 0;

        for (int i = 0; i < n.length; i++){
            System.out.print("Inserisci un valore: ");
            n[i] = sc.nextInt();
        }

        for (int i = 0; i < n.length; i++) {
            if (n[i] < 0) {
                somma += n[i];
                n[i] = 0;
            }
        }

        for (int i = 0; i < n.length; i++) {
            System.out.print(n[i] + " ");
        }
        System.out.println(" ");
        System.out.println(somma);
    }

    public static void es3(){
        int[] n = {1, 2, 3, 5, 4 };
        int max = n[0];
        int pos = 0;

        for (int i = 0; i < n.length; i++) {
            if(n[i] > max){
                max = n[i];
                pos = i;
            }
        }

        System.out.println("Valore massimo: " + max);
        System.out.println("Posizione: " + pos);

    }

    public static void es4(){
        int[] n = {5, 0, 4, 5, 8, 9, 10, 6, 9, 3};

        for (int i = 0; i < n.length; i++) {
            if((i % 2) == 0){
                if((n[i] % 2) == 0){
                    System.out.println("Il numero " + n[i] + " in posizione " + i + " è pari.");
                }
            }
        }
    }
    public static void main(String[] args) {
        //es1();
        //es2();
        //es3();
        //es4();
        int x = 32;
        int n = x % 10;
        int a = x / 10;
        String b = String.valueOf(n) + String.valueOf(a);
        System.out.println(b);
    }
}