package lezione_05.esercizio_01.lavagna;

import java.util.Scanner;

/*
    1) Memorizzare in un array di dieci 
    posizioni i primi dieci numeri naturali.
    2) Dopo aver letto e memorizzato 8 numeri 
    in un array, calcolare la somma di 
    quelli negativi 
    e memorizzare zero 
    al loro posto.
    3) Leggere un array di interi di 5 posizioni e 
    dopo averlo letto cercare al suo 
    interno il massimo e scrivere 
    il massimo e la posizione della cella in cui è memorizzato.
    4) Leggere un array di interi di 10 posizioni 
    e dire se in tutte le posizioni pari dell’array è memorizzato 
    un numero pari.
 */
public class es {
    public static void main(String[] args){
        esercizio_04();
    }
    static void  esercizio_01(){
        int[] array = new int[10];
        for(int i=0; i<array.length;i++){
            array[i]=i+1;
        }
        for(int i=0; i<10; i++){
            System.out.println("Posizione numero "+i+" "+array[i]);
        }
    }
    /*
     Dopo aver letto e memorizzato 8 numeri 
     in un array, calcolare la somma di 
     quelli negativi e memorizzare zero 
     al loro posto.
    */
    static void esercizio_02(){
        Scanner sc = new Scanner(System.in);
        int array[]= new int[8];
        for(int i=0; i<8; i++){
            System.out.println("Inserire il numero "+i);
            array[i]=sc.nextInt();
        }
        sc.close();
        int somma = 0;
        for(int i=0; i<array.length; i++){
            if(array[i]<0){
                somma += array[i];
                array[i]=0;
            }
        }
        
        for(int i=0; i<array.length; i++){
            System.out.println(array[i]);
        }

        System.out.println("La somma dei negativi è "+somma);
    }
    /* 
    3) Leggere un array di interi di 5 posizioni e 
    dopo averlo letto cercare al suo 
    interno il massimo e scrivere 
    il massimo e la posizione della cella in cui è memorizzato.
    */
    static void esercizio_03(){
        Scanner sc = new Scanner(System.in);
        int array[]= new int[5];
        for(int i=0; i<5; i++){
            System.out.println("Inserire il numero "+i);
            array[i]=sc.nextInt();
        }
        sc.close();

        int max = array[0];
        int pos_max = 0;
        for(int i=1; i<5; i++){
            if(array[i]>max){
                max=array[i];
                pos_max = i;
            }
        }

        int min = array[0];
        int pos_min = 0;
        for(int i=1; i<5; i++){
            if(array[i]<min){
                min=array[i];
                pos_min = i;
            }
        }
        System.out.println("Il massimo è "+max +" in posizione " +pos_max);
        System.out.println("Il minimo è "+min+" in posizione "+ pos_min);

    }

    /* 
    4) Leggere un array di interi di 10 posizioni 
    e dire se in tutte le posizioni pari dell’array è memorizzato 
    un numero pari.
    */
    static void esercizio_04(){
        Scanner sc = new Scanner(System.in);
        int l = 10;
        int array[]= new int[l];
        for(int i=0; i<l; i++){
            System.out.println("Inserire il numero "+i);
            array[i]=sc.nextInt();
        }
        sc.close();
        
        boolean tutti_pari=true;
        /* 
        for(int i=0; i<l; i++){
            if(array[i]%2==1 && i%2==0){
                tutti_pari = false;
            }
        }
        */

        for(int i=0; i<l; i+=2){
            if(array[i]%2==1){
                tutti_pari = false;
            }
        }

        if(tutti_pari){
            System.out.println("Nell'array ci sono solo numeri pari");
        }
        else{
            System.out.println("Nell'array non ci sono solo numeri pari");
        }

    }

}
