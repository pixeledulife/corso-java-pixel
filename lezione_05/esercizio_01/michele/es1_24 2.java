/**
 * es1_24
 * 1) Memorizzare in un array di dieci posizioni i primi dieci numeri naturali.
2 Dopo aver letto e memorizzato 8 numeri in un array, calcolare la somma di quelli negativi e memorizzare 
zero 
al loro posto.
3) Leggere un array di interi di 5 posizioni e dopo averlo letto cercare al suo interno il massimo e 
scrivere 
il massimo e la posizione della cella in cui è memorizzato.
4) Leggere un array di interi di 10 posizioni e dire se in tutte le posizioni pari dell’array è memorizzato 
un 
numero pari.
 */
import java.util.Scanner;
public class es1_24 {

    public static void main(String[] args) {
        int[] lista = {1,2,3,4,5,6,7,8,9,10};
        es2();
    }

public static void es2() {
    Scanner input = new Scanner(System.in);

        // Leggi e memorizza 8 numeri in un array
        int[] numeri = new int[8];
        System.out.println("Inserisci 8 numeri interi:");
        for (int i = 0; i < 8; i++) {
            numeri[i] = input.nextInt();
        }

        // Calcola la somma dei numeri negativi e memorizza zero
        int sommaNegativi = 0;
        for (int i = 0; i < 8; i++) {
            if (numeri[i] < 0) {
                sommaNegativi += numeri[i];
            }
        }
        numeri[0] = 0;

        // Stampa l'array e la somma dei numeri negativi
        System.out.println("Array: ");
        for (int i = 0; i < 8; i++) {
            System.out.print(numeri[i] + " ");
        }
        System.out.println("\nSomma dei numeri negativi: " + sommaNegativi);
    }
public static void es3() {
        Scanner input = new Scanner(System.in);

        // Leggi l'array di interi
        int[] numeri = new int[5];
        System.out.println("Inserisci 5 numeri interi:");
        for (int i = 0; i < 5; i++) {
            numeri[i] = input.nextInt();
        }

        // Cerca il massimo e la sua posizione
        int massimo = numeri[0];
        int posizione = 0;
        for (int i = 1; i < 5; i++) {
            if (numeri[i] > massimo) {
                massimo = numeri[i];
                posizione = i;
            }
        }

        // Stampa il massimo e la posizione
        System.out.println("Il massimo è " + massimo + " ed è memorizzato nella cella " + posizione);
    }

    public static void es4(){
        Scanner input = new Scanner(System.in);

        // Leggi l'array di interi
        int[] numeri = new int[10];
        System.out.println("Inserisci 10 numeri interi:");
        for (int i = 0; i < 10; i++) {
            numeri[i] = input.nextInt();
        }

        // Verifica se in tutte le posizioni pari dell'array è memorizzato un numero pari
        boolean tuttiPari = true;
        for (int i = 0; i < 10; i += 2) {
            if (numeri[i] % 2 != 0) {
                tuttiPari = false;
                break;
            }
        }

        // Stampa il risultato
        if (tuttiPari) {
            System.out.println("In tutte le posizioni pari dell'array è memorizzato un numero pari.");
        } else {
            System.out.println("Non tutti i numeri memorizzati nelle posizioni pari dell'array sono pari.");
        }
    }
}