/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package esercizio_1;
import java.util.Scanner;

/**
 *
 * @author matti
1) Memorizzare in un array di dieci posizioni i primi dieci numeri naturali.
2 Dopo aver letto e memorizzato 8 numeri in un array, calcolare la somma di quelli negativi e memorizzare 
zero al loro posto.
3) Leggere un array di interi di 5 posizioni e dopo averlo letto cercare al suo interno il massimo e 
scrivere 
il massimo e la posizione della cella in cui è memorizzato.
4) Leggere un array di interi di 10 posizioni e dire se in tutte le posizioni pari dell’array è memorizzato 
un 
numero pari.


 */

public class Esercizio_1 {

    /**
     * @param args the command line arguments
     */
    
    public static int[] natural(int numero){
        int[] result = new int[numero];
        for(int i = 0; i<numero;i++){
            result[i] = i;
        }
        return result;
    }
    
    public static int[] recupera(int numero){
        int[] result = new int [numero];
        try{
            Scanner input = new Scanner(System.in);
            for(int i = 0; i<numero;i++){
                System.out.print("Inserisci il numero "+(i+1)+" : ");
                result[i] = input.nextInt();
            
            }
            input.close();
        }catch(Exception e){
            System.out.println("Si e' verificato un errore, si usera' un array di default.");
            for(int i = 0; i<numero;i++){
                result[i] = i;
            
            }
        }
        
        return result;
    }
    
    public static String toString(int[] numero){
        String result = "Il tuo array e' -> ";
        for(int i = 0;i<numero.length;i++){
            result += (numero[i]+" ");
        }
        return result;
    }
    
    public static String toString(int numero){
        String result = "La tua somma e' -> "+numero;
        return result;
    }
    
    public static String toString(boolean test){
        if(test){
            return "L'array contiene un numero pari";
        }else{
            return "L'array non contiene un numero pari";
        }
    }
    
    public static int sommaNegativi(int[] numeri){
        int result = 0;
        for(int i = 0;i<numeri.length;i++){
            if(numeri[i]<0){
                result += numeri[i];
            }
        }
        return result;
    }
    
    public static int[] togliNegativi(int[] numeri){
        int[] result = new int[numeri.length];
        for(int i = 0;i<numeri.length;i++){
            if(numeri[i]<0){
                result[i] = 0;
            }else{
                result[i] = numeri[i];
            }
        }
        return result;
    }
    
    public static int numeroMassimo(int[] numeri){
        int result = 0;
        for(int numero: numeri){
            if(result < numero){
                result = numero;
            }
        }
        return result;
    }
    
    public static int posizioneMassima(int[] numeri){
        int result = 0;
        for(int i= 0; i<numeri.length;i++){
            if(result < numeri[i]){
                result = i;
            }
        }
        return result;
    }
    
    public static boolean pari(int[] numeri){
        boolean result = false;
        for(int numero: numeri){
            if(numero%2==0){
                result = true;
                break;
            }
        }
        return result;
    }
    
    public static boolean pariPari(int[] numeri){
        boolean result = true;
        for(int i= 0; i<numeri.length;i++){
            if(i%2 == 0 && numeri[i]%2 != 0){
                result = false;
                break;
            }
        }
        return result;
    }
    
            
    public static void main(String[] args) {
        //Parte 1
        System.out.println(toString(natural(10)));
        
        //Parte 2
        int[] result = recupera(8);
        System.out.println(toString(result));
        System.out.println(toString(sommaNegativi(result)));
        int[] result_2 = togliNegativi(result);
        System.out.println(toString(result_2));
        
        //Parte 3
        int[] result_3 = recupera(5);
        System.out.println(toString(result_3));
        System.out.println("Il massimo e' "+numeroMassimo(result_3)+" in posizione "+posizioneMassima(result_3));
        
        //Parte_4
        int[] result_4 = recupera(10);
        System.out.println(toString(pari(result_4)));
        
        //Parte_5
        int[] result_5 = recupera(10);
        System.out.println(toString(pariPari(result_5)));
    }
    
}
