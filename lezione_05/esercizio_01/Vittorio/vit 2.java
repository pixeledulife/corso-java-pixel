/*
1) Memorizzare in un array di dieci posizioni i primi dieci numeri naturali.
2 Dopo aver letto e memorizzato 8 numeri in un array, calcolare la somma di quelli negativi e memorizzare 
zero 
al loro posto.
3) Leggere un array di interi di 5 posizioni e dopo averlo letto cercare al suo interno il massimo e 
scrivere 
il massimo e la posizione della cella in cui è memorizzato.
4) Leggere un array di interi di 10 posizioni e dire se in tutte le posizioni pari dell’array è memorizzato 
un 
numero pari.
*/
import java.util.Arrays;

class Main {
    public static void main(String[] args){
        System.out.println(Arrays.toString(es1()));

        int[] a = {7, -2, 8, -6, -8, 6};
        int s = es2(a);
        System.out.println(Arrays.toString(a) + s);

        System.out.println(es3(es1()));

        System.out.println(es4(a));
    }


    public static int[] es1(){
        int[] arr = new int[10];
        for(int i=0; i<10; i++){
            arr[i] = i;
        }
        return arr;
    }

    public static int es2(int [] a){ // modificando a mi modifica la stringa di partenza
        int l = a.length;
        int sum = 0;
        for(int i=0; i<l; i++){
            if (a[i] < 0) {
                sum += a[i];
                a[i] = 0;
            }
        }
        return sum;
    }

    public static int es3(int [] a){
        int maxI = 0;
        int l = a.length;
        for(int i=1; i<l; i++){
            if (a[i] > a[maxI]){
                maxI = i;
            }
        }
        return maxI;
    }

    public static boolean es4(int [] a){
        int l = a.length;
        for(int i=0; i<l; i += 2){
            if (a[i]%2 != 0) {
                return false;
            }
        }
        return true;
    }
}

