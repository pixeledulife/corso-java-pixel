package lezione_05.esercizio_01.zeno;

import java.util.Scanner;
/*
1) Memorizzare in un array di dieci posizioni i primi dieci numeri naturali.
2 Dopo aver letto e memorizzato 8 numeri in un array, calcolare la somma di quelli negativi e memorizzare 
zero 
al loro posto.
3) Leggere un array di interi di 5 posizioni e dopo averlo letto cercare al suo interno il massimo e 
scrivere 
il massimo e la posizione della cella in cui è memorizzato.
4) Leggere un array di interi di 10 posizioni e dire se in tutte le posizioni pari dell’array è memorizzato 
un 
numero pari.
 */
public class es1{
    public static void main(String[] args) {
        esercizioD();
    }

        public static void esercizioA() {
            Scanner input = new Scanner(System.in);
            int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            for(int i = 0; i < 10;i++ ){
                System.out.println(array[i]);
            }

    
    
            input.close();
        }
        public static void esercizioB() {
            Scanner input = new Scanner(System.in);
            int array[] = new int[8];
            int negativi = 0;
            for(int i = 0; i < 8; i++){
                System.out.println("Inserisci il numero "+i);
                array[i] = input.nextInt();
                if(array[i] < 0){
                    negativi = negativi + array[i];
                    array[i] = 0;
                }
            }
            for (int i : array) {
                System.out.println(i);
            }
            System.out.println("la somma è "+negativi);

            

            

    
    
            input.close();
        }
        public static void esercizioC() {
            Scanner input = new Scanner(System.in);
            int array[] = new int[5];

            int max = 0;

            for(int i = 0; i < 5;i++ ){
                System.out.println("Inserisci il numero "+i);
                array[i] = input.nextInt();
                if(array[i] >= array[max]){
                    max = i;
                }
            }
            System.out.println("Il numero massimo è "+array[max]+" e si trova nella posizione "+max);
            input.close();
        }
        public static void esercizioD() {
            Scanner input = new Scanner(System.in);
            int array[] = new int[10];

            int dispari = 0;

            for(int i = 0; i < 10;i++ ){
                System.out.println("Inserisci il numero "+i);
                array[i] = input.nextInt();
                if((array[i] % 2 != 0) && (i % 2 != 0)){
                    dispari++;
                }
            }
            if(dispari == 0){
                System.out.println("Nelle posizioni pari c'erano tutti numeri pari");
            }else{
                System.out.println("In "+dispari+" posizioni pari c'erano dei dispari");
            }
            input.close();
        }

    
}

    
    






    