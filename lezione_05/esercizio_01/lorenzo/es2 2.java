import java.util.*;

public class es2 {
// 2 Dopo aver letto e memorizzato 8 numeri in un array, calcolare la somma di quelli negativi e memorizzare zero al loro posto.
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int[]num = new int [8];
        for (int i = 0; i < 8; i++){
            System.out.println("inserisci il numero della posizione " + i);
            num[i] = s.nextInt();
        }
        int somma = 0;

        for (int i = 0; i < 8; i++){
            if (num[i] < 0){
                somma += num[i];
                num[i] = 0;
            }
            System.out.println(num[i]);
        }
        System.out.println("la somma dei numeri negativi è :" + somma);
        s.close();
    }
}
