import java.util.*;
public class es3 {
// 3) Leggere un array di interi di 5 posizioni e dopo averlo letto cercare al suo interno il massimo e 
// scrivere il massimo e la posizione della cella in cui è memorizzato.
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int[]num = new int [5];
        for (int i = 0; i < 5; i++){
            System.out.println("inserisci il numero della posizione " + i);
            num[i] = s.nextInt();
        }

        int max = num[0];
        int index = 0;
        for (int i = 0; i < 5; i++){
            if (num[i] >= max){
                max = num[i];
                index = i;
            }
        }
        System.out.println("Il massimo nell'array è : " + max + " ed è salvata nella posizione " + index);

        s.close();
    }
}
