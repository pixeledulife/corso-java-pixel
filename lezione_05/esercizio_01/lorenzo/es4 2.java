import java.util.*;

public class es4 {
// Leggere un array di interi di 10 posizioni e dire se in tutte le posizioni 
// pari dell’array è memorizzato un numero pari.

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int[]num = new int [10];
        for (int i = 0; i < 10; i++){
            System.out.println("inserisci il numero della posizione " + i);
            num[i] = s.nextInt();
        }
        int pari = 0;
        for (int i = 0; i < 10; i += 2){
                if(num[i] % 2 == 0){
                    pari++;
                }
            }
        if(pari == (num.length / 2)){
            System.out.println("Tutti i numeri nelle posizioni pari sono pari");
        }
        else{
            System.out.println("Non tutti i numeri nelle posizioni pari sono pari");
        }
        s.close();
    }
}

