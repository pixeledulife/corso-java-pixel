package lezione_05.Esercizio_03.zeno;

import java.util.Scanner;
import java.lang.Math;

/*
-Leggere un array di interi di 10 posizioni
 e stampare il numero che compare più volte 
 all’interno dell’array, qualora ci siano 
 più numeri che compaiano lo stesso numero 
 di volte stampare quello che compare per primo.
-Leggere  un array di 10 interi e stampare
 solo i numeri che appaiono nell’array una 
 volta soltanto. Ad esempio se l’array 
 contiene  1, 2, 3, 1, 2, 4  il programma stamperà  3, 4
-Creare un array di interi con 8 posizione 
e riempirlo con numeri casuali compresi 
tra 1 e 90 senza ripetizioni. Stampare il 
contenuto man mano il contenuto dell’array.
 */
public class es1{
    public static void main(String[] args) {
        esercizioG();
    }

        public static void esercizioA() {
            Scanner input = new Scanner(System.in);
            int array[] = new int[10];
            int contatore[] = new int[10];
            for(int i = 0; i < 10;i++ ){
                System.out.println(array[i]);
            }
            for(int j = 0; j < 10;j++){
                for(int i = 0; i < 10; i++){
                    if(array[j] == array[i]){
                        contatore[j]++;
                    }
                }
            }

    
    
            input.close();
        }
        public static void esercizioB() {
            Scanner input = new Scanner(System.in);

            System.out.println("Inserisci il numero da invertire: ");
            int numero = input.nextInt();
            while(numero != 0){
                System.out.print((numero % 10));
                numero = numero / 10;
            }

    
    
            input.close();
        }
        public static void esercizioC() {
            Scanner input = new Scanner(System.in);

            for(int i = 2; i <= 100; i = i + 2){
                System.out.println(i);
            }
            input.close();
        }
        public static void esercizioD() {
            Scanner input = new Scanner(System.in);
            System.out.println("inserisci il margine inferiore: ");
            int n = input.nextInt();
            System.out.println("inserisci il margine inferiore: ");
            int m = input.nextInt();
            int array[] = new int[20];
            for (int i = 0; i < 20; i++) {
                array[i] = (int)(Math.random() * (m - n + 1) + n);   
            }

            for (int i = (array.length - 1); i > 0; i-- ){

                System.out.print((array[i] * 2)+" ");
            }
            input.close();
        }
        public static void esercizioE() {
            Scanner input = new Scanner(System.in);
            
            int n = -8;
            int m = 15;

            System.out.println("inserisci la lunghezza dell'array: ");
            int lunghezza = input.nextInt();
            int array[] = new int[lunghezza];
            for (int i = 0; i < lunghezza; i++) {
                array[i] = (int)(Math.random() * (m - n + 1) + n);   
            }
            int max = 0;
            int min = 0;
            for(int i = 0; i < lunghezza;i++ ){
                if(array[i] >= array[max]){
                    max = i;
                }else if(array[i] <= array[min]){
                    min = i;
                }
            }
            System.out.println("Il numero massimo è "+array[max]+" e il minimo è: "+array[min]);
            input.close();
        }

        public static void esercizioF() {
            Scanner input = new Scanner(System.in);
            System.out.println("inserisci il tuo nome: ");
            String nome = input.nextLine();
            System.out.println("inserisci il tuo cognome: ");
            String cognome = input.nextLine();
            for(int i = 0; i < 10; i++){
                System.out.println(nome + ' ' + cognome);
            }
            input.close();
        }
        public static void esercizioG() {
           
            for(int i = 1; i <= 10; i++){
                System.out.println(i);
            }      
        }
        



   
}
