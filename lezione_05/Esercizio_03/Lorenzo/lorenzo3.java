import java.util.*;
public class lorenzo3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Inserisci lunghezza array: ");
        int n = s.nextInt();
        s.close();
        int [] arr = new int[n];
        int max = 15;
        int min = -8;
        int range = max - min + 1;
        for (int i = 0; i < arr.length; i++){
            int rand = (int)(Math.random() * range) + min;
            arr[i] = rand;
            System.out.print(arr[i]+ ", ");
        }

        for (int i = 0; i < arr.length; i++){
            if ( arr[i] < max){
               max = arr[i];
            }
            else if(arr[i] > min){
                min = arr[i];
            }
        }
        System.out.println("");
        System.out.println("Il massimo è " + min + " e il minimo è " + max);

    }
}
