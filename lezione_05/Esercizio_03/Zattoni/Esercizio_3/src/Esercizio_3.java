
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 */

public class Esercizio_3 {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        System.out.print("Inserisci numero: ");
        int numero = input.nextInt();
        int result = 0;
        try{
            while(numero!=0){
                numero /= 10;
                result+=1;
            }
            System.out.println(result);
        }catch(Exception e){
            System.out.println(e);
        }
        input.close();
    }
}
