import java.util.Scanner;

public class Main {
    
    public static void es1(){
        for (int i = 1; i <= 100; i++) {
            if((i % 2) == 0){
                System.out.println(i);
            }
        }

        for (int i = 1; i <= 40; i++) {
            if((i % 2) != 0){
                System.out.println(i);
            }
        }
    }

    public static void es2(){
        Scanner input = new Scanner(System.in);
        int[] n = new int[20];

        System.out.print("Inserisci numero di partenza: ");
        int start = input.nextInt();
        System.out.print("Inserisci numero di fine: ");
        int end = input.nextInt();
        int range = end - start + 1;

        for (int i = 0; i < n.length; i++) {
            int rand = (int)(Math.random() * range) + start;
            n[i] = rand;
        }

        for (int i:n) {
            System.out.print(i);
        }
        System.out.println(" ");

        for(int i = n.length - 1; i >= 0; i--){
            System.out.print(n[i]);
        }
        System.out.println(" ");

        for(int i = n.length - 1; i >= 0; i--){
            System.out.print(n[i] * 2);
        }
    }

    public static void es3(){
        Scanner input = new Scanner(System.in);
        System.out.print("Inserisci l'unghezza array: ");
        int leng = input.nextInt();
        int[] n = new int[leng];
        int min = -8;
        int max = 32;
        int range = max - min + 1;

        for(int i = 0; i < leng; i++){
            n[i] = (int)(Math.random() * range) + min;
        }

        for (int i:n) {
            System.out.print(i);
        }

    }
    
    public static void main(String[] args) {
        /*es1();
        es2();
        es3();
         */
    }
}