package lezione_03.esercizio_01.Vittorio;
import java.util.Scanner;

/*

Stampare la tabella della verità della funzione AND usando 
System.out.println


Realizzare un programma che dopo aver chiesto l'età all'utente indica se 
tale persona può usufruire della promozione del cinema. La promozione è 
riservata a coloro che NON hanno meno di 64 anni OPPURE a chi ha tra 5 E 
18 anni.
Per svolgere l'esercizio usare tutti e tre gli operatori !, && e ||.

 */

public class main {
    public static void main(String[] args) {
        es1();
        System.out.println("");
        es2(2);
        es2(7);
        es2(25);
        es2(74);
    }
    
    public static void es2(int eta) {
        if (!(eta < 64) || ((eta >= 5) && (eta <= 18))){
            System.out.println("certo che puo' ");
        } else {
            System.out.println("certo che NON puo' ");
        }
    }

    public static void es1() {
        boolean a = true;
        boolean b = true;

        String A = String.valueOf(a); 
        String B = String.valueOf(b); 
        String C = String.valueOf(a&&b);
    
        System.out.println(A + "\t" + B + "\t" + C);

        a = !a;

        A = String.valueOf(a); 
        C = String.valueOf(a&&b);
    
        System.out.println(A + "\t" + B + "\t" + C);

        a = !a;
        b = !b;

        A = String.valueOf(a); 
        B = String.valueOf(b);
        C = String.valueOf(a&&b);
    
        System.out.println(A + "\t" + B + "\t" + C);

        a = !a;

        A = String.valueOf(a); 
        C = String.valueOf(a&&b);
    
        System.out.println(A + "\t" + B + "\t" + C);
    
        return;
    }
}

