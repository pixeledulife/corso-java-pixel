public static void es4()
    {
        System.out.println("V & V = V");
        System.out.println("V & F = F");
        System.out.println("F & V = F");
        System.out.println("F & F = F");
    }

    public static void es5()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci l'età: ");
        int eta = sc.nextInt();

        if(eta < 64)
        {
            if (eta >= 5 && !(eta >= 18))
            {
                System.out.println("Hai accesso alla promozione");
            }
            else if (eta < 5)
            {
                System.out.println("Non hai accesso alla promozione");
            }
            else
            {
                System.out.println("Non hai accesso alla promozione");
            }
        }
        else
        {
            System.out.println("Hai accesso alla promozione");
        }
    }

    public static void es6()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci la temperatura: ");
        int temp = sc.nextInt();
        System.out.println("Inserisci se piove 0/1: ");
        int piove = sc.nextInt();

        if (temp < 10 && piove == 1)
        {
            System.out.println("Prendi la macchina");
        } else if (temp < 10 || piove == 1)
        {
            System.out.println("Prendi i mezzi pubblici");
        }
        else
        {
            System.out.println("Vai in bici");
        }
    }