package lezione_03.esercizio_01.lavagna;
/*

Stampare la tabella della verità della funzione AND usando 
System.out.println


Realizzare un programma che dopo aver chiesto l'età all'utente indica se 
tale persona può usufruire della promozione del cinema. La promozione è 
riservata a coloro che NON hanno meno di 64 anni OPPURE a chi ha tra 5 E 
18 anni.
Per svolgere l'esercizio usare tutti e tre gli operatori !, && e ||.

 */
public class es{
    public static void main(String[] args){

        System.out.println("Tabella delle verità:");
        System.out.println("false AND false: "+(false && false));
        System.out.println("false AND true: "+(false && true));
        System.out.println("true AND false: "+(true && false));
        System.out.println("true AND true: "+(true && true));

        int age = 65;
        if(!(age<64)|| ( age >= 5 && age <= 18 )){
            System.out.println("promozione");
        }
    }
}
