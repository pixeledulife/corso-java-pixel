package Enrico.esercizio_01;
import java.util.*;

class Main{

    public static void TabellaAnd(){
        boolean a,b,ris;

        a = true;
        b = true;
        ris = a&&b;
        System.out.println(a + " and " + b + " => " + ris);

        a = true;
        b = false;
        ris = a&&b;

        System.out.println(a + " and " + b + " => " + ris);

        a = false;
        b = true;
        ris = a&&b;

        System.out.println(a + " and " + b + " => " + ris);

        a = false;
        b = false;
        ris = a&&b;

        System.out.println(a + " and " + b + " => " + ris);
    }

    public static void controlloPromo(){
        int anni;
        Scanner s = new Scanner(System.in);

        System.out.println("Inserisci anni: ");
        anni = s.nextInt();

        //promozione per chi ha anni !> 64 oppure 5 <= anni <= 18

        if(!(anni < 64) || (anni >= 5 && anni <=18)){
            System.out.println("La tua età è idonea alla promozione!");
        }
        else{
            System.out.println("La tua età non è idonea alla promozione..");
        }
    }
    
    
    public static void main(String[] args) {
        controlloPromo();
    }
}