import java.text.ParsePosition;
import java.util.Scanner;

import org.w3c.dom.TypeInfo;

class Main {
    public static void main(String[] args){
        Scanner s= new Scanner(System.in);
        System.out.println("Inserisci anni");
        int anni = s.nextInt();

        if (anni >= 64 || anni <= 18 && anni > 5 && anni <= 18){
            System.out.println("Hai accesso alla promozione");
        }
        else if (anni < 0){
            System.out.println("Non puoi inserire numeri negativi!");
        }
        else{
            System.out.println("Non hai accesso alla promozione");
        }

    }
}