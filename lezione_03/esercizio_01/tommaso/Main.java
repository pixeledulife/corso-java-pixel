package lezione_03.esercizio_01.tommaso;
import java.util.Scanner;
/**
 * InnerMain
 */
public class Main {
    public static void main(String[] args) {
        es1();
        es2();
        
    }

    public static void es1() {
        System.out.println("Tabella delle verità");
        System.out.println("true && true: " + (true && true));
        System.out.println("false && false: " + (false && false));
        System.out.println("true && false: " + (true && false));
        System.out.println("false && true: " + (false && true));
        System.out.println("true || true: " + (true || true));
        System.out.println("false || false: " + (false || false));
        System.out.println("true || false: " + (true || false));
        System.out.println("false || true: " + (false || true));
    }

    /**
     * es2
     */
    public static void es2() {
        Scanner input = new Scanner(System.in);
        System.out.println("Quanti anni hai?");
        Integer age = input.nextInt();
        if(age > 64 || age >= 5 && age <= 18) {
            System.out.println("Hai diritto alla promozione!");
        } else {
            System.out.println("Non hai diritto all promozione!");
        }
        
    }
}
