package lezione_03.esercizio_01.zeno;

/*  
        Stampare la tabella della verità della funzione AND usando 
        System.out.println
*/
import java.util.Scanner;
public class es1 {
    public static void main(String[] args) {
        esercizioC();
    }

    public static void esercizioA() {
        boolean a = false;
        boolean b = false;

        System.out.println("Con l'operatore &&");

        System.out.println("a == false");
        System.out.println("b == false");
        boolean c = a && b; 
        System.out.println("a && b == "+ c);

        System.out.println("Con l'operatore &&");

        System.out.println("a == false");
        System.out.println("b == true");
        b = true;
        c = a && b; 
        System.out.println("a && b == "+ c);

        System.out.println("a == true");
        System.out.println("b == false");
        b = false;
        a = true;
        c = a && b; 
        System.out.println("a && b == "+ c);

        System.out.println("a == true");
        System.out.println("b == true");
        b = true;
        c = a && b; 
        System.out.println("a && b == "+ c);
    }

/*
        Realizzare un programma che dopo aver chiesto l'età all'utente indica se 
        tale persona può usufruire della promozione del cinema. La promozione è 
        riservata a coloro che NON hanno meno di 64 anni OPPURE a chi ha tra 5 E 
        18 anni.
        Per svolgere l'esercizio usare tutti e tre gli operatori !, && e ||.

 */


    public static void esercizioB(){
        Scanner input = new Scanner(System.in);
        System.out.println("Quanti anni hai?");
        int age = input.nextInt();
        if(!(age <= 64)  || ((age >= 5) && (age <= 18))){
            System.out.println("Hai diritto alla promozione!");
        }else{
            System.out.println("Non hai diritto alla promozione!");

        }
        input.close();
    }

    public static void esercizioC(){
        Scanner input = new Scanner(System.in);
        System.out.println("Piove? Dimmelo scrivendo 1 per il SI e 0 per il NO");
        int piove = input.nextInt();
        System.out.println("Ci sono meno di 10°C? Dimmelo scrivendo 1 per il SI e 0 per il NO");
        int gradi = input.nextInt();


        
        if((piove == gradi) && gradi == 1){
            System.out.println("Piglia la macchina fratellì");
        }else if((piove == 1 || gradi == 1) && !(piove == gradi)){
            System.out.println("Piglia i mezzi fratellì");
        }else{
            System.out.println("Pedala fratellì");
        }
        input.close();
        
    }



    
}