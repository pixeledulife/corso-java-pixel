import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Ex1
        /*
        System.out.println("FALSO " + "FALSO " + "= FALSO");
        System.out.println("FALSO " + "VERO " + "= FALSO");
        System.out.println("VERO " + "FALSO " + "= FALSO");
        System.out.println("VERO " + "VERO " + "= VERO");
         */

        //Ex2
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci età: ");
        int age = sc.nextInt();

        if(!(age < 64) || (age > 5 && age < 18)){
            System.out.println("Hai la promozione");
        }else{
            System.out.println("Niente sconto per te");
        }

    }
}