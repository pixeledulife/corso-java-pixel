package lezione_03.esercizio_04.Vittorio;

/*  Date due stringhe nome e cognome concatenarle e stampare il nome 
    completo della persona, stamparne inoltre le iniziali attraverso 
    la funzione charAt

    Creare un programma chiedendo come dato l’anno di nascita calcola 
    l’età dell’individuo, poi chiede di inserire nome e cognome 
    e restituisce “Il signor [...] è nato nell’anno [...]”. 
    trasformare i nomi e i cognomi ricevuti in parole in minuscolo 
    con le iniziali in maiuscolo.

    Verificare che due stringhe abbiano le stesse lettere anche
    se case e spazi iniziali e finali diversi ( si vedano per questo esercizio 
    le funzioni equals(), trim() e UpperCase())  */

public class main {
    public static void main(String[] args) {
        System.out.println(es1("Vittorio","Fava"));
        es2("vittORIo","faVa",2001);
        System.out.println(ugualiLettere("  CiaO"," cIaO  "));
    }
    
    public static String es1(String s1, String s2){
        return s1 + " " + s2;
    }

    public static int annoToEta(int anno){
        return 2023 - anno;
    }

    public static String firstUpper(String s){
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public static void es2(String nome, String cognome, int anno){
       System.out.println("Il signor " + es1(firstUpper(nome),firstUpper(cognome)) + " è nato nell'anno " + anno); 
    }

    public static boolean ugualiLettere(String s1, String s2){
        return s1.toUpperCase().trim().equals(s2.toUpperCase().trim());
    }
}

