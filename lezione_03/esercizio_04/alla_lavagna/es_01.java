package lezione_03.esercizio_04.alla_lavagna;

import java.util.Scanner;

public class es_01 {
    public static void main(String[] args){
        es3();
    }

    /* 
    Date due stringhe nome e cognome concatenarle e stampare il nome 
    completo della persona, stamparne inoltre le iniziali attraverso 
    la funzione charAt
    */
    public static void es1(){
        System.out.println("Esercizio 1");
        String Nome = "Federico";
        String Cognome = "Fortuna";
        System.out.println(Nome +' '+ Cognome);
        System.out.println(Nome.charAt(0));
        System.out.println(Cognome.charAt(0));
    }

    /* 
    Creare un programma chiedendo come dato l’anno di nascita calcola 
    l’età dell’individuo, poi chiede di inserire nome e cognome 
    e restituisce “Il signor [...] è nato nell’anno [...]”. 
    trasformare i nomi e i cognomi ricevuti in parole in minuscolo 
    con le iniziali in maiuscolo.
    */
    public static void es2(){

        Scanner s = new Scanner(System.in);        
        System.out.println("Inserisci anno di nascita");
        int anno = Integer.parseInt(s.nextLine());
        int annoAttuale = 2023;
        System.out.println("età: " + (annoAttuale - anno));
        System.out.println("Inserisci il tuo nome");
        String nome = s.nextLine();
        nome = nome.toLowerCase();
        nome = nome.substring(0, 1).toUpperCase() + nome.substring(1);
        System.out.println("Inserisci il tuo cognome");
        String cognome = s.nextLine();
        cognome = cognome.toLowerCase();
        cognome = cognome.substring(0, 1).toUpperCase() + cognome.substring(1);

        System.out.println("Il Signor " + nome + " " + cognome + " e' nato nell'anno " + anno);
        s.close();

    }

    /* 
    Verificare che due stringhe abbiano le stesse lettere anche
    se case e spazi iniziali e finali diversi ( si vedano per questo esercizio 
    le funzioni equals(), trim() e toUpperCase())
    */
    public static void es3(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci la prima frase: ");
        String prima = sc.nextLine();
        System.out.println("Inserisci la seconda frase: ");
        String seconda = sc.nextLine();

        prima = prima.trim().toUpperCase();
        seconda = seconda.trim().toUpperCase();

        if(prima.equals(seconda)){
            System.out.println("Le stringhe sono uguali");
        }
        else{
            System.out.println("Le stringhe sono differenti");
        }
        sc.close();

    }

}
