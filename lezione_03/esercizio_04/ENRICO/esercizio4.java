package lezione_03.esercizio_04.ENRICO;
import java.util.*;

public class esercizio4 {
    public static void concat_iniziali(Scanner s){
        String nome, cognome;

        System.out.println("Inserire nome: ");
        nome = s.nextLine();

        System.out.println("Inserire cognome: ");
        cognome = s.nextLine();

        System.out.println(nome + " " + cognome);
        System.out.println("Le tue iniziali sono: " + nome.charAt(0) + " " + cognome.charAt(0));
    }

    public static void inizialiMaiuscole(Scanner s){
        System.out.println("Nome: ");
        String nome = s.nextLine();
        String cutNome = nome.trim().substring(0,1).toUpperCase() + nome.substring(1).toLowerCase();

        System.out.println("Cognome: ");
        String cognome = s.nextLine();
        cognome = cognome.trim().substring(0,1).toUpperCase() + cognome.substring(1).toLowerCase();

        System.out.println("Inserisci anno di nascita:" );
        int anno = s.nextInt();
        anno = 2023 - anno;

        System.out.println("Il signor " + cutNome + " " + cognome + " ha " + anno + "anni!\n");
        
    }

    public static void stringheUguali(Scanner s){
        System.out.println("Inserire la prima stringa: ");
        String s1 = s.nextLine();

        System.out.println("Inserire la seconda stringa: ");
        String s2 = s.nextLine();

        if(s2.trim().toUpperCase().equals(s2.trim().toUpperCase())){
            System.out.println("Stringhe Uguali!");
        }
        else{
            System.out.println("Stringhe diverse!!");
        }
    }


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        stringheUguali(s);
    }
}
