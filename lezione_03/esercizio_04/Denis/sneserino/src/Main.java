import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Inserisci il nome: ");

        String nome = in.nextLine();
        System.out.println("Inserisci il cognome: ");

        String cognome = in.nextLine();
        System.out.println("Inserisci l'anno di nascita: ");

        int anno = in.nextInt();
        char first = Character.toUpperCase(nome.charAt(0));
        char second = Character.toUpperCase(cognome.charAt(0));

        nome = nome.replaceFirst(String.valueOf(nome.charAt(0)), String.valueOf(first));
        cognome = cognome.replaceFirst(String.valueOf(cognome.charAt(0)), String.valueOf(second));

        System.out.println("Il signor " + nome + " " + cognome + " e' nato nell'anno " + anno + " e ha " + (2023 - anno) + " anni.");
        
        if (nome.toUpperCase().trim().equals(cognome.toUpperCase().trim())) {
            System.out.println("Il nome ed il cognome sono uguali!");
        } else {
            System.out.println("Il nome ed il cognome non sono uguali!");
        }
    }
}
