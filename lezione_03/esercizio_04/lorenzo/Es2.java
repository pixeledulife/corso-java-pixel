// Creare un programma chiedendo come dato l’anno di nascita calcola 
// l’età dell’individuo, poi chiede di inserire nome e cognome 
// e restituisce “Il signor [...] è nato nell’anno [...]”. 
// trasformare i nomi e i cognomi ricevuti in parole in minuscolo 
// con le iniziali in maiuscolo.
import java.util.*;

public class Es2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("inserisci l'anno di nascita");
        int anno = Integer.parseInt(s.nextLine());
        
        int eta = 2023 - anno;
        System.out.println("inserisci nome e cognome:");
        String nome = s.nextLine();
        String cognome = s.nextLine();

        nome = nome.substring(0, 1).toUpperCase() + nome.substring(1).toLowerCase();
        cognome = cognome.substring(0, 1).toUpperCase() + cognome.substring(1).toLowerCase();

        System.out.println("Il signor " + nome + " " + cognome + " è nato nell'anno " + anno + " e ha " + eta + " anni");


        s.close();
    }
}
