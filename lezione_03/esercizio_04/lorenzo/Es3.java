// Verificare che due stringhe abbiano le stesse lettere anche
//  se case e spazi iniziali e finali diversi ( si vedano per questo esercizio 
//  le funzioni equals(), trim() e UpperCase())



public class Es3 {
    public static void main(String[] args) {


        String str1 = "  Ciao ";
        String str2 = "ciAo         ";

        str1 = str1.trim().toUpperCase();
        str2 = str2.trim().toUpperCase();

        if (str1.equals(str2)){
            System.out.println("hanno le stesse lettere");
        }
        else{
            System.out.println("non hanno le stesse lettere");
        }

    }
}
