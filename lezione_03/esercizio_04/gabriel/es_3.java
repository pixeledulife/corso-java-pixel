import java.util.*;

public class es_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("prima parola");
        System.out.println("seconda parola");

        String s1 = sc.nextLine();
        String s2 = sc.nextLine();

        s1 = s1.trim().toUpperCase();
        s2 = s2.trim().toUpperCase();

        if (s1.equals(s2)) {
            System.out.println("sono uguali");
        }
        else{
            System.out.println("sono diverse");
        }

        sc.close();
    }
}