import java.util.*;

public class es_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int oggi = 2023;
        System.out.println("che anno sei nato?");
        int anno = Integer.parseInt(sc.nextLine());

        int età = oggi - anno;

        System.out.println("nome?");
        System.out.println("cognome?");
        String nome = sc.nextLine();
        nome = nome.toLowerCase();
        nome = nome.substring(0, 1).toUpperCase() + nome.substring(1);

        String cognome = sc.nextLine();
        
        cognome = cognome.toLowerCase();
        cognome = cognome.substring(0, 1).toUpperCase() + cognome.substring(1);

        System.out.println("il signor " + nome + " " + cognome + " è nato nell'anno " + anno + " e ha " + età + " anni");

        sc.close();
    }
}
