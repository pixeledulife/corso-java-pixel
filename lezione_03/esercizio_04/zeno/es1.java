package lezione_03.esercizio_04.zeno;
/*
    Date due stringhe nome e cognome concatenarle e stampare il nome 
    completo della persona, stamparne inoltre le iniziali attraverso 
    la funzione charAt
 */
import java.util.Scanner;

public class es1 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Dimmi il tuo nome: ");
        String name = input.nextLine();
        System.out.println("Dimmi il tuo nome: ");
        String surname = input.nextLine();

        System.out.println("Ciao "+name+' '+surname);
        System.out.println("Le tue iniziali sono " + name.charAt(0)+surname.charAt(0));
        input.close();
    }
}
