package lezione_03.esercizio_04.zeno;

/*
    Verificare che due stringhe abbiano le stesse lettere anche
    se case e spazi iniziali e finali diversi ( si vedano per questo esercizio 
    le funzioni equals(), trim() e UpperCase())

 */
import java.util.Scanner;

public class es3 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci la prima stringa");
        String prima = input.nextLine();

        System.out.println("Inserisci la seconda stringa");
        String seconda = input.nextLine();

        prima = prima.trim();
        prima = prima.toUpperCase();
        seconda = seconda.trim();
        seconda = seconda.toUpperCase();

        if(prima.equals(seconda)){
            System.out.println("Le due stringhe hanno gli stessi caratteri");
        }else{
            System.out.println("Le due stringhe NON hanno gli stessi caratteri");

        }

        input.close();
    }
}
