package lezione_03.esercizio_04.zeno;

/*
    Creare un programma chiedendo come dato l’anno di nascita calcola 
    l’età dell’individuo, poi chiede di inserire nome e cognome 
    e restituisce “Il signor [...] è nato nell’anno [...]”. 
    trasformare i nomi e i cognomi ricevuti in parole in minuscolo 
    con le iniziali in maiuscolo.
 */
import java.util.Scanner;

public class es2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci il tuo anno di nascita: ");
        int year = Integer.parseInt(input.nextLine());
        System.out.println("Tu hai " + (2023 - year) + " anni");

        System.out.println("Dimmi il tuo nome: ");
        String name = input.nextLine();

        System.out.println("Dimmi il tuo cognome: ");
        String surname = input.nextLine();

        name = name.toLowerCase();
        name = name.substring(0,1).toUpperCase()+name.substring(1);
        surname = surname.toLowerCase();
        surname = surname.substring(0,1).toUpperCase()+surname.substring(1);
        System.out.println(name+' '+surname);

        input.close();
    }
}
