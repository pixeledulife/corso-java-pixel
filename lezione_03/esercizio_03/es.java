package lezione_03.esercizio_03;

public class es {
    public static void main(String[] args){

        String s = "Ciao a tutti";    
        System.out.println("Stringa originale  " + s);  
        System.out.println("Stringa che inizia dall\'indice 6: " + s.substring(6) ); // ?    
        System.out.println("Stringa che va dall\'indice 0 a 6: "+ s.substring(0,6) ); // ? 
        
    }
}
