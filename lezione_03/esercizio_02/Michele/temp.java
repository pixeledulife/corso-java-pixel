import java.util.Scanner;

class Main {
    public static void main(String[] args){
        Scanner s= new Scanner(System.in);
        System.out.println("Sta piovendo");
        int piove = s.nextInt();
        System.out.println("Ci sono meno di 10 gradi?");
        int gradi = s.nextInt();

        if (piove == 1 && gradi == 1){
            System.out.println("Ciapa la macchina");
        }
        else if(piove == 1 && gradi == 0){
            System.out.println("Prendi i mezzi pubblici");
        }
        else if(piove == 0 && gradi == 0){
            System.out.println("Prendi la bici");
        }
        else{
            System.out.println("Devi inserire un numero compreso tra 0 e 1");
        }

    }
}