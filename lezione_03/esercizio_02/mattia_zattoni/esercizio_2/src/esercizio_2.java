import java.util.Scanner;

public class esercizio_2 {
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        System.out.println("Piove -> 0/1 ->");
        boolean piove = (input.nextInt() == 1)? true: false;
        System.out.println("C'e' meno di 10 gradi -> 0/1 ->");
        boolean temperatura = (input.nextInt() == 1)? true: false;
        
        if(piove && temperatura){
            System.out.println("Prendi la macchina");
        }
        if((!piove && temperatura) || (piove && !temperatura)){
            System.out.println("Prendi il bus");
        }
        if(!(piove) && !(temperatura)){
            System.out.println("Prendi la bici");
        }
        
        
        input.close();
        
    }
    
}
