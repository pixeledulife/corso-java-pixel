package lezione_03.esercizio_02.Vittorio;

/*

Scrivere un programma che chieda all’utente se piove o se la temperatura è inferiore ai 10 gradi attraverso il numero 0 e il numero 1

Se piove e fanno meno di 10 gradi il programma suggerirà di prendere la macchina.
Se piove o fa meno di 10 gradi e contemporaneamente non accadono entrambe le cose suggerire di prendere un mezzo pubblico.
Se non piove e se fanno più di dieci gradi suggerire di prendere la bicicletta.

 */

public class main {
    public static void main(String[] args) {
        quale_mezzo_utilizzo(true, 5);
        quale_mezzo_utilizzo(true, 15);
        quale_mezzo_utilizzo(false, 2);
        quale_mezzo_utilizzo(false, 20);
    }
    
    public static void quale_mezzo_utilizzo(boolean piove, int temperatura){
        if (piove && temperatura<10) {
            System.out.println("Prendi la macchina");
        } else if (!piove && temperatura > 10){
            System.out.println("Pedala");
        } else {
            System.out.println("Usa i mezzi pubblici");    
        }
    }
}

