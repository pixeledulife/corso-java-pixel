import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        //Ex 2
        /*
        System.out.println("Sta piovendo?");
        boolean rain = sc.hasNextBoolean();
        System.out.println("Temperatura sotto i 10°?");
        boolean temp = sc.hasNextBoolean();

        if(rain && temp){
            System.out.println("Prendi la macchina");
        } else if(rain ^ !temp) {
            System.out.println("Prendi un mezzo pubblico");
        } else{
            System.out.println("Vai in bici");
        }
        */

        //Ex3.1
/*
        System.out.print("Inserisci nome: ");
        String name = sc.nextLine();
        System.out.print("Inserisci cognome: ");
        String surname = sc.nextLine();
        System.out.println(name + " " + surname);

        char chrName = name.charAt(0);
        char chrSurname = surname.charAt(0);
        System.out.println("Iniziali: " + chrName + chrSurname);
*/

        //Ex3.2
        /*
        System.out.print("Inserisci nome: ");
        String name = sc.nextLine().toLowerCase();
        System.out.print("Inserisci cognome: ");
        String surname = sc.nextLine().toLowerCase();
        name = name.substring(0, 1).toUpperCase() + name.substring(1);
        surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);

        System.out.print("Inserisci anno di nascita: ");
        int birth = sc.nextInt();
        int age = 2023 - birth;

        System.out.println("Il signor " + name + " " + surname + " ha " + age + " anni.");
        */

        

    }
}