package lezione_03.esercizio_02.Enrico;
import java.util.*;

public class enrico_es2 {
    public static void Meteo(){
        int rispostaUtente;
        Scanner s = new Scanner(System.in);

        System.out.println("Qual è il meteo di oggi?? \n0 - Pioggia e gradi sotto i 10 \n1 - Pioggia o Meno di 10 gradi \n");
        rispostaUtente = s.nextInt();

        switch(rispostaUtente){
            case 0:
                System.out.println("Conviene prendere la macchina!\n");
                break;
            case 1:
                System.out.println("Meglio andare con i mezzi pubblici..\n");
                break;
            default:
                System.out.println("Viste la bella giornata usa la bici!!\n");
        }
    }


    public static void main(String[] args) {
        Meteo();
    }
}
