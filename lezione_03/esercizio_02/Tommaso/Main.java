import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Che tempo ce fuori?");
        System.out.println("Piove? [1] vero, [0] falso");
        Integer rain = input.nextInt();
        System.out.println("Temperature inferiore a 10°? [1] vero, [0] falso");
        Integer temp = input.nextInt();
        if(rain == 1 && temp == 1) {
            System.out.println("Prendi la macchina");
        } else if(rain == 1 || temp == 1) {
            System.out.println("Prendi un mezzo pubblico");
        } else {
            System.out.println("Prendi la bici");
        }

    }
}
