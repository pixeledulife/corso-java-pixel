import java.util.*;

public class Main {
    public static void main(String[] args) {
        float money, interest;
        int years;
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci l'importo: ");
        money = input.nextFloat();
        System.out.println("Inserisci l'interesse: ");
        interest = input.nextFloat();
        System.out.println("Inserisci gli anni: ");
        years = input.nextInt();
        System.out.println(calc(money, interest, years));
    }

    public static float calc(float money, float interest, int years){
        if (years > 0) {
            money = money + (money * (interest / 100));
            return calc(money, interest, years - 1);
        } else {
            return (money - (money * 26 / 100));
        }
    }
}
