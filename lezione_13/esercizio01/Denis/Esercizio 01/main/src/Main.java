import java.util.*;

public class Main {
    public static int NUM = 0;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci il numero");
        NUM = input.nextInt();
        stampa(NUM, 0);
    }

    public static int stampa(int x, int y){
        if (x != 0){
            if (y == 0) {
                System.out.print("-");
                return stampa(x - 1, 0);
            } else {
                System.out.print("+");
                if ((x - 1) != 0) {
                    return stampa(x - 1, 1);
                } else {
                    return 0;
                }
            }
        } else {
            return stampa(NUM, 1);
        }
    }
}
