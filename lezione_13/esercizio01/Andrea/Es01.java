/*
Scrivere una funzione ricorsiva che ricevuto un intero n stampi n ‘-‘ seguiti da n ‘+’.
Ad esempio se la funzione riceve 3 stampa – – – + + +.
 */
import java.util.*;
public class Es01{
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println("Inserisci un intero");
        int input = sc.nextInt();
        System.out.println(serie(input));

        
    }
    public static String serie1(int x){
        String ret1 = "";

        if (x > 0){
            String meno = "- "; 

            ret1 = meno + serie1(x-1);
        }

        return ret1;
    }

    public static String serie2(int x){
        String ret2 = "";
        if (x > 0){
            String piu = "+ "; 

            ret2 = piu + serie2(x-1);

        }

        return ret2;
    }
    public static String serie(int x){
        return serie1(x) + serie2(x);
    }

}