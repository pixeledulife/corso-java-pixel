import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        esercizio00(false);
    }

    /**
    1) creare un programma che calcoli la media di 10 numeri inseriti da tastiera
    2) se hai usato il while per fare l’es 1 ora utilizza il for e viceversa
    3) Modifica l'es 2 in modo da calcolare la media di n numeri inseriti da tastiera (fermati quando l'utente inserisce uno zero)
    eg.
    utente digita: 2
    utente digita: 3
    utente digita: 4
    utente digita: 0
    la media è: 3
    */
    static void esercizio00(boolean use_for_cycle){
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserire numero di dati da prendere in input.");
        final int threshold = sc.nextInt();
        ArrayList<Integer> al = new ArrayList<>();
        if(use_for_cycle)
            for(int i=0; i<threshold; i++){
                System.out.println("Inserire valore");
                al.add(sc.nextInt());
            }
        else {
            int i=0;
            while(i<threshold){
                System.out.println("Inserire valore");
                al.add(sc.nextInt());
                i++;
            }
        }

        double average = 0;
        for( Integer a :  al ){
            average += a;
        }
        average /= al.size();
        System.out.println("La media è " + average);
    }
}