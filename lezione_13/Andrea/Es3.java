import java.util.*;
public class Es3{
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        ArrayList <Integer> numbers = new ArrayList<>();
        float media = 0;
        System.out.println("Inserisci numeri da tastiera (0 per terminare)");
        
        while(true){
            int input = sc.nextInt();
            if (input != 0){
                numbers.add(input);

            }
            else{
                break;
            }

        }

       
        for (int i = 0; i < numbers.size(); i++){
            media += numbers.get(i);
        }
        media = media / numbers.size();
        System.out.println("La media dei numeri è: " + media);

    }
}