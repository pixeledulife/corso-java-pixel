public class Main {
    public static void main(String[] args) {
        Gatto g = new Gatto();
        g.verso();

        Cane c = new Cane();
        c.verso();
        System.out.println("Numero gambe: " + c.numeroGambe());
        System.out.println("Numero gambe: " + c.numeroGambe(6));
        System.out.println("Specie: " + Cane.specie());
    }
}