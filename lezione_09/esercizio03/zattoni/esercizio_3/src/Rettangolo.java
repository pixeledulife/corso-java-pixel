public class Rettangolo extends Quadrilatero implements Lati{
    private int base;
    private int altezza;
    
    public Rettangolo(){
        this.setRettangolo(5, 10);
    }
    
    public Rettangolo(int base, int altezza){
        this.setRettangolo(base, altezza);   
    }
    
    public void setRettangolo(int base, int altezza){
        this.setAltezza(altezza);
        this.setBase(base);
    }
    
    public void setBase(int base){
        this.base = base;
    }
    
    public void setAltezza(int altezza){
        this.altezza = altezza;
    }
    
    public int getBase(){
        return this.base;
    }
    
    public int getAltezza(){
        return this.altezza;
    }
    
    @Override
    public String toString(){
        return (this.base+" "+this.altezza);
    }
    
    @Override
    public int area(){
        return (this.base*this.altezza);
    }

    @Override
    public String lati() {
        return ("Il Rettangolo è formato da: \n   - due lati da "+this.getBase()+"\n   - due lati da "+this.getAltezza());
    }
}
