public class Parallelepipedo extends Rettangolo implements Lati{
    public int larghezza;
    public int volume;
    
    public Parallelepipedo(){
        super();
        this.setLarghezza(10);
        this.setVolume();
    }
    
    public Parallelepipedo(int base,int altezza, int larghezza){
        super(base,altezza);
        this.setLarghezza(larghezza);
        this.setVolume();
    }

    public void setLarghezza(int larghezza){
        this.larghezza = larghezza;
    }
    
    public int getLarghezza(){
        return this.larghezza;
    }
    
    public void setVolume(){
        this.volume = (this.area()*this.larghezza);
    }
    
    public int getVolume(){
        return this.volume;
    }
    
    @Override
    public String toString(){
        return ("Il parallelepipedo e' formato da: \n   "+this.getBase()+" di base \n   "+this.getAltezza()+" di altezza \n   "+this.getLarghezza()+" di larghezza \nE quindi hanno un volume di "+this.getVolume());
    }
    
    @Override
    public String lati(){
        return ("Il parallelepipedo e' formato da: \n   - 4 lati da "+this.getBase()+"\n   - 4 lati da "+this.getAltezza()+"\n   - 4 lati da "+this.getLarghezza());
    }
}
