package lezione_09.esercizio03.zeno;

public class Rettangolo extends Quadrilatero {

    public Rettangolo(int latoA, int latoB){
        super(latoA, latoB);
    }

    public int area(){
        return latoA * latoB;
    }

    public int lati(){
        return 4;
    }
    
}
