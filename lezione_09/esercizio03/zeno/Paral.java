package lezione_09.esercizio03.zeno;

public class Paral extends Rettangolo{
    public int altezza;
    public Paral(int latoA, int latoB, int h){
        super(latoA, latoB);
        altezza = h;
    }
    public void volume(){
        System.out.println("Il volume del parallelepipedo è " + (area() * altezza));
    }
    public int lati(){
        return 12;
    }
    public int area() {
        return latoA * latoB * 6;
        
    }
}
