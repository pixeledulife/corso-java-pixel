package lezione_09.esercizio03.zeno;

public abstract class Quadrilatero {
    public int latoA;
    public int latoB;
    public int h;

    public Quadrilatero(int a,int b){
        latoA = a;
        latoB = b;
    }
    abstract int area();
}
