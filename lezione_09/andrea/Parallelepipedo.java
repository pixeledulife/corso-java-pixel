public class Parallelepipedo extends Rettangolo{
    private int profondita;
    private int volume;



    public Parallelepipedo(int base, int altezza, int profondita){
        super(base, altezza);
        this.profondita = profondita;
    }
 
    public int calcolaArea(Parallelepipedo par){
        return calcolaArea(par);
    }
    public int area(Parallelepipedo x){
        return x.calcolaArea(); 

    }
    public int volume(){
        this.volume = this.area * this.profondita;
        return this.volume;
    }

    public static void main(String[] args) {
        Parallelepipedo par = new Parallelepipedo(5,4,3);
        int area = par.area();
        System.out.println(area);
        
        int volume = par.volume();
        System.out.println(volume);    

    }

    
}