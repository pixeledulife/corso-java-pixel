public class Rettangolo extends Quadrilatero {
    public int latoA;
    public int latoB;
    public int area;

    public Rettangolo (int a, int b){
        this.latoA = a;
        this.latoB = b;
        this.area = this.latoA * this.latoB;

    }

    public void impostaDimensioni(int x, int y){
        this.latoA = x;
        this.latoB = y;

    }
    public int calcolaArea(){
       return this.area;
    }

    public int area(Rettangolo x){
        return x.calcolaArea(); 

    }


}
