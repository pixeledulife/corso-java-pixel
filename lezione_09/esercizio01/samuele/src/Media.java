public class Media {
    private final int a;
    private final int b;
    private final int c;

    private final int media;

    public Media(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
        this.media = (this.a + this.b + this.c) / 3;
    }

    public Media(){
        this.a = 1;
        this.b = 1;
        this.c = 1;
        this.media = (this.a + this.b + this.c) / 2;
    }

    public int getMedia(){
        return this.media;
    }


}
