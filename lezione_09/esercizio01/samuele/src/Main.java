import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //Ex1
        /*Rettangolo r = new Rettangolo(10, 5);
        System.out.println(r.calcolaArea());*/

        //Ex2
        /*System.out.print("Inserisci a: ");
        int a = input.nextInt();
        System.out.print("Inserisci b: ");
        int b = input.nextInt();
        System.out.print("Inserisci c: ");
        int c = input.nextInt();

        Media m = new Media(a, b, c);
        System.out.println(m.getMedia());*/

        //Ex3
        Dipendente da = new Dipendente("Robet", 1994, 30000, "64C-WallStreet");
        Dipendente db = new Dipendente("Sam", 2000, 10000, "68D-WallStreet");
        Dipendente dc = new Dipendente("John", 1999, 15000, "26B-WallStreet");
        Dipendente dd = new Dipendente();
        System.out.println(da);
        System.out.println(db);
        System.out.println(dc);
        System.out.println(dd);
    }
}