public class Dipendente {
    private final String nome;
    private final int annoAssunzione;
    private final int salario;
    private final String indirizzo;

    public Dipendente(String nome, int annoAssunzione, int salario, String indirizzo ){
        this.nome = nome;
        this.annoAssunzione = annoAssunzione;
        this.salario = salario;
        this.indirizzo = indirizzo;
    }

    public Dipendente(){
        this.nome = "Mario";
        this.annoAssunzione = 1745;
        this.salario = 3000000;
        this.indirizzo = "28B - Via XX Settembre";
    }

    public String getNome() {
        return nome;
    }

    public int getAnnoAssunzione() {
        return annoAssunzione;
    }

    public int getSalario() {
        return salario;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    @Override
    public String toString(){
        return getNome() + " " + getAnnoAssunzione() + " " + getSalario() + " " + getIndirizzo();
    }


}
