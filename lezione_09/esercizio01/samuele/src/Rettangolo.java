public class Rettangolo {
    private int altezza;
    private int lunghezza;

    public Rettangolo(int altezza, int lunghezza){
        this.altezza = altezza;
        this.lunghezza = lunghezza;
    }
    public Rettangolo(){
        this.altezza = 0;
        this.lunghezza = 0;
    }

    public int getAltezza(){
        return this.altezza;
    }

    public int getLunghezza(){
        return this.lunghezza;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }

    public void setLunghezza(int lunghezza) {
        this.lunghezza = lunghezza;
    }

    public int calcolaArea(){
        return getLunghezza() * getAltezza();
    }


}
