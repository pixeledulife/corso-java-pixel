public class Dipendente{
    private String nome;
    private int anno;
    private String indirizzo;
    private int salario;

    public Dipendente(String nome, int anno, String indirizzo, int salario){
        this.nome = nome;
        this.anno = anno;
        this.indirizzo = indirizzo;
        this.salario = salario;
    }
    public void stampaDipendente(){
        System.out.println(this.nome +"      "+  this.anno  + "       " + this.indirizzo + "          "    + this.salario);

    }


}