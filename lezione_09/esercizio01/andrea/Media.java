public class Media{
    private int a;
    private int b;
    private int c;
    private float media;

    public Media(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;

    }
    public float calcolamedia(){
        this.media = (this.a + this.b + this.c) / 3;
        return this.media;

    }
    public void stampamedia(){
        System.out.print("La media e': ");
        System.out.println(this.media);

    }
}