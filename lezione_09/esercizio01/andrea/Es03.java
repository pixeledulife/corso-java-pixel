import javax.swing.text.StyleContext.SmallAttributeSet;

/*
3)
Creare un programma che stampa le informazioni (nome, anno di entrata, salario, indirizzo) di tre dipendenti creando una classe chiamata ‘Dipendente’. 
L’output dovrebbe essere:
Nome        Anno di assunzione        Indirizzo
Robert            1994             	64C- WallsStreat
Sam                2000                 68D- WallsStreat
John                1999                26B- WallsStreat
 */

public class Es03{
    public static void main(String[] args) {
        Dipendente robert = new Dipendente("Robert", 1994, "64C - WallStreet", 2000);
        Dipendente sam = new Dipendente("Sam", 2000, "68D- WallStreet", 10000);
        Dipendente john = new Dipendente("John", 1999, "26B -WallStreet", 9800);
        System.out.println("Nome        Anno di assunzione        Indirizzo");
        robert.stampaDipendente();
        sam.stampaDipendente();
        john.stampaDipendente();

        
    }

}