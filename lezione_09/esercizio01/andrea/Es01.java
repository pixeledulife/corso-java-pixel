/* 
1) 
Scrivere un programma per calcolare l'area di un rettangolo creando una classe chiamata
'Rettangolo'. 'Rettangolo' ha due metodi:
'impostaDimensioni()' prende in input le due dimensioni del rettangolo.
'calcolaArea()' ritorna l'area del rettangolo se le dimensioni sono state inserite.
Creare infine un costruttore che imposta direttamente le dimensioni del rettangolo.

*/

public class Es01{
    public static void main(String[] args) {
        Rettangolo rettangolo = new Rettangolo(10,5);
        int area = rettangolo.calcolaArea();
        System.out.println(area);
    }
}
