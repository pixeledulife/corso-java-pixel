public class Rettangolo {
    public int latoA;
    private int latoB;

    public Rettangolo (int a, int b){
        this.latoA = a;
        this.latoB = b;

    }

    public void impostaDimensioni(int x, int y){
        this.latoA = x;
        this.latoB = y;

    }
    public int calcolaArea(){
        return this.latoA * this.latoB;
    }


    
    
}
