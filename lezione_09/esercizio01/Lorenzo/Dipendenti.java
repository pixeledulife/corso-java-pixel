public class Dipendenti {
    private String nome, indirizzo;
    private int annoEntrata, salario;

    public Dipendenti(String nome, String indirizzo, int annoEntrata, int salario){
        this.nome = nome;
        this.indirizzo = indirizzo;
        this.annoEntrata = annoEntrata;
        this.salario = salario;
    }

    public void stampaCose(){
        System.out.println(this.nome+"      "+this.indirizzo+"      "+this.annoEntrata+"        "+this.salario);
    }
}
