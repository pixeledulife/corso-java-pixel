// Scrivere un programma per calcolare l'area di un rettangolo creando una classe chiamata
// 'Rettangolo'. 'Rettangolo' ha due metodi:
// 'impostaDimensioni()' prende in input le due dimensioni del rettangolo.
// 'calcolaArea()' ritorna l'area del rettangolo se le dimensioni sono state inserite.
// Creare infine un costruttore che imposta direttamente le dimensioni del rettangolo.

public class es1 {
    public static void main(String[] args) {
        Rettangolo ret01 = new Rettangolo();
        ret01.impostaDimensioni(5, 2);
        System.out.println("L'area di ret01 è "+ ret01.calcoloArea());
    }
}