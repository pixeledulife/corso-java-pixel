import java.util.Scanner;

// Stampare la media di tre numeri immessi dall’utente creando una classe chiamata ‘Media’
//  avente due metodi che calcolano e stampano la media.
public class es2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("inserisci tre valori da calcolarne la media: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        Media media = new Media(a, b, c);
        media.calcoloMedia();
        media.stampaMedia();
        sc.close();
    }
}
