// Creare un programma che stampa le informazioni 
// (nome, anno di entrata, salario, indirizzo) di 
// tre dipendenti creando una classe chiamata ‘Dipendente’. 
// L’output dovrebbe essere:
// Nome        Anno di assunzione        Indirizzo
// Robert            1994             	64C- WallsStreat
// Sam                2000                 68D- WallsStreat
// John                1999                26B- WallsStreat

public class es3 {
    public static void main(String[] args) {
        Dipendenti dipendenti = new Dipendenti("Robert", "64C- WallsStreat", 1994, 100000);
        Dipendenti dipendenti2 = new Dipendenti("Sam", "68D- WallsStreat", 2000, 90000);
        Dipendenti dipendenti3 = new Dipendenti("John", "26B- WallsStreat", 1999, 110000);
        
        System.out.println("Nome        Anno di assunzione      Indirizzo       Salario");
        dipendenti.stampaCose();
        dipendenti2.stampaCose();
        dipendenti3.stampaCose();


    }
}
