public class Rettangolo {
    int latoA, latoB;
    public void impostaDimensioni(int a, int b){
            this.latoA = a;
            this.latoB = b;
    }

    public int calcoloArea(){
        return this.latoA * this.latoB;
    }
}
