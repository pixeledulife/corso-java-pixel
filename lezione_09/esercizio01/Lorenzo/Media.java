public class Media {
    int a, b, c;
    float media;

    public Media(int valA, int valB, int valC){
        this.a = valA;
        this.b = valB;
        this.c = valC;
    }
    
    public float calcoloMedia(){
        this.media = (this.a + this.b + this.c) / 3;
        return this.media;
    }

    public void stampaMedia(){
        System.out.println("la media è " +this.media);
    }

}
