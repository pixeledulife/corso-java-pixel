package lezione_09.esercizio01.zeno;

public abstract class Animale {
    String nome;

    public Animale(String a){
        this.nome = a;
    }

    public String toString(){
        return getClass()+" "+nome;
    }
    abstract String faiVerso();
    abstract void stampa1();

    public void scriviA(){
        System.out.println("A");
    }
}
