package lezione_09.esercizio01.zeno;

import java.nio.ReadOnlyBufferException;

// 3)
// Creare un programma che stampa le informazioni (nome, anno di entrata, salario, indirizzo) di tre dipendenti creando una classe chiamata ‘Dipendente’. 
// L’output dovrebbe essere:
// Nome        Anno di assunzione        Indirizzo
// Robert            1994             	64C- WallsStreat
// Sam                2000                 68D- WallsStreat
// John                1999                26B- WallsStreat

public class es3 {
    public static void main(String[] args) {

        Dipendente dip1 = new Dipendente("Robert", 1994, 1000000, "64C- WallStreat");

        Dipendente dip2 = new Dipendente("Sam", 2000, 1000000, "68D- WallStreat");

        Dipendente dip3 = new Dipendente("John", 1999, 1000000, "26B- WallStreat");

        dip1.stampa();
        dip2.stampa();
        dip3.stampa();
    }
    
}
