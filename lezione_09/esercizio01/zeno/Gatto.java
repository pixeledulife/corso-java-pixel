package lezione_09.esercizio01.zeno;
public class Gatto extends Animale{

    public Gatto(String a) {
        super(a);
    }

    @Override
    String faiVerso() {
        return "Miao";
    }

    @Override
    void stampa1() {
        System.out.println(this.toString());
        System.out.println("Fai verso: "+ faiVerso());
    }
}