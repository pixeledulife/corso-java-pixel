package lezione_09.esercizio01.zeno;


// 1) 
// Scrivere un programma per calcolare l'area di un rettangolo creando una classe chiamata
// 'Rettangolo'. 'Rettangolo' ha due metodi:
// 'impostaDimensioni()' prende in input le due dimensioni del rettangolo.
// 'calcolaArea()' ritorna l'area del rettangolo se le dimensioni sono state inserite.
// Creare infine un costruttore che imposta direttamente le dimensioni del rettangolo.

public class es1 {
    public static void main(String[] args) {

        Rettangolo ret = new Rettangolo(0,0);
        ret.impostaDimensioni(5, 2);
        System.out.println("L'area del rettangolo è " + ret.calcolaArea());
        Rettangolo ret2 = new Rettangolo(2, 2);
        System.out.println("L'area del secondo rettangolo " + ret2.calcolaArea());
    }
}
