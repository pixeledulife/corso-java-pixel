package lezione_09.esercizio01.zeno;

// 2)
// Stampare la media di tre numeri immessi dall’utente creando una classe chiamata ‘Media’ avente due metodi che calcolano e stampano la media.

public class Media {
    public int a;
    public int b;
    public int c;
    public Media(int primo, int secondo, int terzo){
        this.a = primo;
        this.b = secondo;
        this.c = terzo;
    }
   
    

    public float calcolaMedia(int a, int b, int c) {

        return (a + b + c) / 3;
        
    }
    public void stampaMedia(){
        System.out.println("La media è " + ((this.a + this.b + this.c) / 3));
    }
}
