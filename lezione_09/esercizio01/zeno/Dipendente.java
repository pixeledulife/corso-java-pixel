package lezione_09.esercizio01.zeno;
// 3)
// Creare un programma che stampa le informazioni (nome, anno di entrata, salario, indirizzo) di tre dipendenti creando una classe chiamata ‘Dipendente’. 
// L’output dovrebbe essere:
// Nome        Anno di assunzione        Indirizzo
// Robert            1994             	64C- WallsStreat
// Sam                2000                 68D- WallsStreat
// John                1999                26B- WallsStreat
public class Dipendente {
    private String nome;
    private int anno_di_entrata;
    private int salario;
    private String indirizzo;
    public Dipendente(String name, int anno, int salario, String indirizzo){
        this.nome = name;
        this.anno_di_entrata = anno;
        this.salario = salario;
        this.indirizzo = indirizzo;
    }
    public void stampa(){
        System.out.println("\nNome    Anno di Assunzione  Indirizzo\n" + this.nome + "          " + this.anno_di_entrata + "            " + this.indirizzo);
    }
    
}
