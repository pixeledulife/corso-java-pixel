package lezione_09.esercizio01.zeno;
public class Cane extends Animale{
    boolean abbaia; // cane ha un attributo in più 
    public Cane(String name, boolean staAbbaiando){
        super(name);//chiama il costruttore nella classe padre
        abbaia = staAbbaiando; //assegna un valore al nuovo attributo
    }
    public void eccitato(){
        abbaia = true;
    }
    public void quieto(){
        abbaia = false;
    }
    //i metodi astratti della classe padre Animale sono implementati nella sottoclasse
    @Override
    public String faiVerso(){
        if(abbaia){
            return "Bau";
        }else{
            return "";
        }
    }

    @Override
    public void stampa1{
        System.out.println(this.toString());
        System.out.println("fai verso: " + faiVerso());
        System.out.println("sta abbaindo: " + this.abbaia);
    }
    }

    
