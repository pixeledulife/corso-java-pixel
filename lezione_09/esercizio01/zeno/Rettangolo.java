package lezione_09.esercizio01.zeno;

// 1) 
// Scrivere un programma per calcolare l'area di un rettangolo creando una classe chiamata
// 'Rettangolo'. 'Rettangolo' ha due metodi:
// 'impostaDimensioni()' prende in input le due dimensioni del rettangolo.
// 'calcolaArea()' ritorna l'area del rettangolo se le dimensioni sono state inserite.
// Creare infine un costruttore che imposta direttamente le dimensioni del rettangolo.

public class Rettangolo {
    int latoA, latoB;
    public Rettangolo(int a, int b) {
        this.latoA = a;
        this.latoB = b;
        
    }
    public void impostaDimensioni(int latoA, int latoB) {
        this.latoA = latoA;
        this.latoB = latoB;
    }
    public int calcolaArea() {
        return this.latoA * this.latoB;
    }
    
}
