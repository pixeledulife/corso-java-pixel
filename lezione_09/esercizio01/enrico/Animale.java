package lezione_09.esercizio01.enrico;

public abstract class Animale {
    private static String nome;

    public Animale(String nome){
        this.nome = nome;
    }

    public String toString(){
        return getClass()+": "+nome;
    }

    abstract String faiVerso();
    abstract void stampa();
}
