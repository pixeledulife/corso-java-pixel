package lezione_09.esercizio01.enrico;

public class Cane extends Animale{

    private static boolean abbaia;

    public Cane(String nome, boolean staAbbaiando){
        super(nome);
        abbaia = staAbbaiando;
    }

    public void eccitato(){
        abbaia = true;
    }

    public void quieto(){
        abbaia = false;
    }

    @Override
    String faiVerso() {
        if(abbaia){
            return "BAU";
        }else{
            return "OOF";
        }
    }

    @Override
    void stampa() {
        
    }
    
}
