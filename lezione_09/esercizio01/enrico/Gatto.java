package lezione_09.esercizio01.enrico;

public class Gatto extends Animale{
    String nome;
    public Gatto(String nome){
        this.nome = nome;
    }


    @Override
    String faiVerso() {
        return "MIAO";
    }

    @Override
    void stampa() {
        System.out.println(this.toString());
        System.out.println("Verso: "+faiVerso());
    }
    
}
