public class Dipendente {
    String nome;
    int annoEntrata, salario, indirizzo;

    public Dipendente(String nome, int annoEntrata, int salario, int indirizzo) {
        this.nome = nome;
        this.annoEntrata = annoEntrata;
        this.salario = salario;
        this.indirizzo = indirizzo;
    }

    @Override
    public String toString() {
        return "Il dipendente " + this.nome + 
        ", entrato nel " + annoEntrata +
        " e che vive all'indirizzo " + indirizzo + 
        ",guadagna " +  salario + "euro al mese";
    }
}