package lezione_09.esercizio01.tommaso;

public class Media {
    private int a, b, c;

    public Media(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int calcolaMedia() {
        return (this.a + this.b + this.c) / 3;
    }

    @Override
    public String toString() {
        return "Media di " + this.a + ", " + this.b + ", " + this.c + ": " + calcolaMedia();
    }
      
}   
