import java.util.Scanner;

public class Rettangolo {
    private int a, b;

    public void Rettangolo(int a, int b) {
        this.a = a;
        this.b = b;
    }

    private void impostaDimensioni(int a, int b) {
        this.a = a;
        this.b = b;
    }

    private int calcolaArea() {
        return this.a * this.b;
    }

    @Override
    public String toString() {
        return "Il rettangolo con i lati di " + a + " e " + b + " hanno un'area di " + calcolaArea();
    }
}