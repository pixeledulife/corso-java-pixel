public class Dipendente {
    private String nome;
    private int anno;
    private int salario;
    private String indirizzo;
    
    public Dipendente(){
        this.setDipendente("Mattia", "Galtarossa", 2003, 1920);
    }
    
    public void setNome(String Nome){
        this.nome = Nome;
    }
    
    public void setIndirizzo(String indirizzo){
        this.indirizzo = indirizzo;
    }
    
    public void setAnno(int anno){
        this.anno = anno;
    }
    
    public void setSalario(int salario){
        this.salario = salario;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public int getAnno(){
        return this.anno;
    }
    
    public String getIndirizzo(){
        return this.indirizzo;
    }
    
    public int getSalario(){
        return this.salario;
    }
    
    public void setDipendente(String nome, String indirizzo, int anno, int salario){
        this.setAnno(anno);
        this.setIndirizzo(indirizzo);
        this.setSalario(salario);
        this.setNome(nome);
    }

    @Override
    public String toString(){
        String result = this.getNome() + " " + this.getAnno() +" "+ this.getIndirizzo()+" "+this.getSalario();
        return result;
    }
}
