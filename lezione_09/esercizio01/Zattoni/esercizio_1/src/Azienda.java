
import java.util.ArrayList;

public class Azienda {
    private ArrayList<Dipendente> azienda = new ArrayList<Dipendente>();
    
    public Azienda(){
        Dipendente a = new Dipendente();
        for(int i = 0; i<3;i++){
            this.azienda.add(a);
        }
    }
    
    @Override
    public String toString(){
        String result="";
        for(int i = 0;i<3;i++){
            result += this.azienda.get(i).toString();
            result += "\n";
        }
        return result;
    }
}
