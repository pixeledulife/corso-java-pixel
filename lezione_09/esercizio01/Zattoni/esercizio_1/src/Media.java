public class Media {
    private int num_1;
    private int num_3; 
    private int num_2;
    private int media;
    
    public Media(){
        this.setMedia(10, 5, 7);
    }
    
    public Media(int num_1,int num_2, int num_3){
        this.setMedia(num_1, num_2, num_3);
    }
    
    public void setNum1(int num){
        this.num_1 = num;
    }
    
    public void setNum2(int num){
        this.num_2 = num;
    }
    
    public void setNum3(int num){
        this.num_3 = num;
    }
    
    public int getNum1(){
        return this.num_1;
    }
    
    public int getNum2(){
        return this.num_2;
    }
    
    public int getNum3(){
        return this.num_3;
    }

    public void setMedia(int num1, int num2, int num3){
        this.setNum1(num1);
        this.setNum2(num2);
        this.setNum3(num3);
        this.calcolaMedia();
    }
    
    public void calcolaMedia(){
        int result = (this.num_1 + this.num_2 + this.num_3)/3;
        this.media = result;
    }
    
    @Override
    public String toString(){
        String result = "I numeri "+this.num_1+ ","+this.num_2+","+this.num_3+" fanno una media di "+this.media;
        return result;
    }
}
