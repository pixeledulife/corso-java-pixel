public class Rettangolo {
    private int base;
    private int altezza;
    
    public Rettangolo(){
        this.impostaDimensioni(5, 10);
    }
    
    public Rettangolo(int base, int altezza){
        this.impostaDimensioni(base, altezza);
    }
    
    public void impostaDimensioni(int base, int altezza){
        this.setBase(base);
        this.setAltezza(altezza);
    }
    
    public void setBase(int base){
        this.base = base;
    }
    
    public void setAltezza(int altezza){
        this.altezza = altezza;
    }
    
    public int getBase(){
        return this.base;
    }
    
    public int getAltezza(){
        return this.altezza;
    }
    
    public int calcolaArea(){
        int result = this.getBase() * this.getAltezza();
        return result;
    }
    
    @Override
    public String toString(){
        String result = "Il rettangolo ha "+this.getAltezza() +" d'altezza e di base "+this.getBase();
        return result;
    }
}
