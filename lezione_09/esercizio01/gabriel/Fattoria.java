package gabriel;

public class Fattoria {
     Cane snoopy;
     Gatto micio;
     Animale[] pets;

    public Fattoria(Cane cane, Gatto gatto){
        this.snoopy = cane;
        this.micio = gatto;
        this.pets = new Animale[2];
        pets[0] = snoopy;
        pets[1] = micio;
    }

    public void concerto(){
        for(Animale a : pets){
            System.out.println(a.faiVerso());
        }
    }
}
