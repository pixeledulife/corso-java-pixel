package gabriel;

public class Dipendenti {
    private String nome, indirizzo;
    private int annoAssunzione, salario;

    public Dipendenti(String nome, int annoAssunzione, String indirizzo, int salario){
        this.nome = nome;
        this.annoAssunzione = annoAssunzione;
        this.indirizzo = indirizzo;
        this.salario = salario;
    }

    public void stampa(){
        System.out.println("nome " + nome);
        System.out.println("annoAssunzione " + annoAssunzione);
        System.out.println("indirizzo " + indirizzo);
        System.out.println("salario " + salario);
    }
}
