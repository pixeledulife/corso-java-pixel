package gabriel;

public class Gatto extends Animale {
    public Gatto(String unNome){
        super(unNome);
    }

    public String faiVerso(){
        return "miao";
    }

    public void stampa(){
        System.out.println(this.toString());
        System.out.println("Fai verso: " + faiVerso());
    }
    
}
