// Scrivere un programma per calcolare l'area di un rettangolo creando una classe chiamata
// 'Rettangolo'. 'Rettangolo' ha due metodi:
// 'impostaDimensioni()' prende in input le due dimensioni del rettangolo.
// 'calcolaArea()' ritorna l'area del rettangolo se le dimensioni sono state inserite.
// Creare infine un costruttore che imposta direttamente le dimensioni del rettangolo.
package gabriel;

public class es1{
    public static void main(String[] args) {
    Rettangolo rettangolo = new Rettangolo(2, 2);
    rettangolo.impostaDimensioni(3, 3);
    System.out.println(rettangolo.calcolaArea());
    
}
}
