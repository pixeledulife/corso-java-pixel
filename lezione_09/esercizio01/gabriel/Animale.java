package gabriel;

public abstract class Animale {
    String nome;

    public Animale(String unNome) {
        this.nome = unNome;
    }

    public String toString() {
        return getClass() + ": " + this.nome;
    }

    abstract String faiVerso();

    abstract void stampa();
}
