package gabriel;
public class Media {
    private int primo;
    private int secondo;
    private int terzo;
    private float media;

    public Media(int a, int b, int c){
        this.primo = a;
        this.secondo = b;
        this.terzo = c;
    }

    public float calcoloMedia(){
        this.media = (this.primo + this.secondo + this.terzo) / 3;
        return this.media;
    }

    public void stampaMedia(){
        System.out.println("la media è " + this.media);
    }
}
