package gabriel;

public class Cane extends Animale{
    boolean abbaia;
    
    public Cane(String unNome, boolean staAbbaiando){
        super(unNome);
        this.abbaia = staAbbaiando;
    }

    public void eccitato(){
        this.abbaia = true;
    }

    public void quieto(){
        this.abbaia = false;
    }

    public String faiVerso(){
        if(abbaia){
            return "Bau";
        }
        else{
            return "woof";
        }
    }

        public void stampa(){
            System.out.println(this.toString());
            System.out.println("fai verso: " + faiVerso());
            System.out.println("sta abbaiando: " + this.abbaia);
        }
    
        public int numeroGambe(){
            return 4;
        }

        public int numeroGambe(int n){
            return n * 4;
        }

        static Srting specie(){
            return "Canis lupum familiaris";
        }

    }
    

