package gabriel;

public class Rettangolo {
    int latoA;
    int latoB;
    public Rettangolo(int latoa, int latob){
        this.latoA = latoa;
        this.latoB = latob;
    }

    public Rettangolo(){

    }

    public void impostaDimensioni(int a, int b){
        this.latoA = a;
        this.latoB = b;
    }

    public int calcolaArea(){
        return this.latoA*this.latoB;
    }
}
