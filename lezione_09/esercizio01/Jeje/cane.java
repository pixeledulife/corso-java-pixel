public class cane extends Animale{
    boolean abbaia;

    public cane(String jeje,boolean staAbbaiando){
        super(jeje);
        abbaia = staAbbaiando;
    }
    public void eccitato(){
        abbaia = true;
    }

    public void quieto(){
        abbaia = false;
    }

    @Override
    public String faiVerso(){
        if(abbaia)
            return "Bau";
        else
            return " ";
    }

    @Override
    public void stampa(){
        System.out.println(this.toString());
        System.out.println("Fai verso: " + faiVerso());
        System.out.println("Sta abbaiando: " + this.abbaia);
    }
}