public class fattoria {
    private cane snoopy;
    private gatto micio;
    private Animale[] pets;

    public fattoria(){
        snoopy = new cane("Snoopy", false);
        micio = new gatto("Ernesto");
        pets[0] = snoopy;
        pets[1] = micio;
    }

    public void concerto(){
        for(Animale a : pets)
            System.out.println(a.faiVerso());
    }
}