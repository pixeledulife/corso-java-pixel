public abstract class Animale {
    String nome;

    public Animale(String nome){
        this.nome = nome;
    }

    public Animale(){
        this.nome = "boh";
    }

    @Override
    public String toString(){
        return getClass() + ": " + this.nome;
    }

    abstract String faiVerso();

    abstract void stampa();


}
