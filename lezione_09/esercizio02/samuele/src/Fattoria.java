public class Fattoria {
    private Cane snoopy;
    private Gatto micio;
    private Animale[] pets;

    public Fattoria(){
        this.snoopy = new Cane("Snoopy", true);
        this.micio = new Gatto("Ernesto");
        this.pets = new Animale[2];
        pets[0] = snoopy;
        pets[1] = micio;
    }

    public void concerto(){
        for(Animale a:pets){
            System.out.println(a.faiVerso());
        }
    }
}
