public class Cane extends Animale {
    boolean abbaia;

    public Cane(String nome, boolean abbaia){
        super(nome);
        this.abbaia = abbaia;
    }

    public void eccitato(){
        this.abbaia = true;
    }

    public void quieto(){
        this.abbaia = false;
    }

    public String faiVerso(){
        if(abbaia){
            return "bau";
        }else{
            return " ";
        }
    }

    public void stampa(){
        System.out.println(this.toString());
        System.out.println("Fai verso: " + faiVerso());
        System.out.println("Sta abbaiando: " + this.abbaia);
    }

}
