public class Gatto extends Animale {
    public Gatto(String nome){
        super(nome);
    }

    public String faiVerso(){
        return "Miao";
    }

    public void stampa(){
        System.out.println(this.toString());
        System.out.println("Fai verso: " + faiVerso());
    }
}
