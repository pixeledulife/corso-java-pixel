public class Fattoria {
    public Cane snoopy;
    public Gatto micio;
    private Animale[] pets;
    public Fattoria(){
        snoopy = new Cane("Snoopy", true);
        micio = new Gatto("Ernesto");
        pets = new Animale[2];
        pets[0]=snoopy;
        pets[1]=micio;
    }
    public void concerto(){
        for(Animale a:pets){
            System.out.println(a.faiVerso());
        }
    }
}
