public abstract class Animale {
    String nome;

    public Animale(String a){
        this.nome = a;
    }

    public String toString(){
        return getClass()+" "+nome;
    }
    abstract String faiVerso();
    abstract void stampa();

    public void scriviA(){
        System.out.println("A");
    }
}
