import java.sql.SQLOutput;

public class Cane extends Animale{

    boolean abbaia;

    public Cane(String a, boolean staAbbaiando) {
        super(a);
        abbaia = staAbbaiando;
    }

    public void eccitato(){
        abbaia = true;
    }
    public void quieto(){
        abbaia = false;
    }
    @Override
    String faiVerso() {
        if(abbaia)
            return "Bau";
        else
            return "";
    }

    @Override
    void stampa() {
        System.out.println(this.toString());
        System.out.println("Fai verso: "+ faiVerso());
        System.out.println("Sta abbaiando: "+this.abbaia);
    }
}
