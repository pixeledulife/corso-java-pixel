public class Rettangolo extends Quadrilatero{
    public int area;

    public Rettangolo(int base,int profondità){
        super(base, profondità);
    }

    public int area(){
        this.area = profondità * base;
        return this.area;
    }

    public int lati(){
        return 4;
    }
}
