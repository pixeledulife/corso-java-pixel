public abstract class Quadrilatero {
    public int base; 
    public int profondità;

    public Quadrilatero(int base, int profondità){
        this.base = base;
        this.profondità = profondità;
    }

    abstract int area();
    
}
