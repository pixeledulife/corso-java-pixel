public class Parallelepipedo extends Rettangolo{

    int altezza;
    int volume;

    public Parallelepipedo(int altezza, int base, int profondità){
        super(base, profondità);
        this.altezza = altezza;
    }

    public int volume(){
        this.volume = area() * altezza;
        return this.volume;
    }

    public int lati(){
        return 12;
    }
}
