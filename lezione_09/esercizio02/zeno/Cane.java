package lezione_09.esercizio02.zeno;

public class Cane implements Animale {
    public void verso(){
        System.out.println("bau");
    }

    public int numeroGambe(){
        return 4;
    }
    public int numeroGambe(int cani){
        return 4 * cani;
    }
    static String specie(){
        return "Canis lupus familiaris";
    }
    
}
