package lezione_09.esercizio02.tommaso;

public class Animale {
    private String sesso, nome;

    public Animale(String sesso, String nome) {
        this.sesso = sesso;
        this.nome = nome;
    }

    public String getSesso() {
        return sesso;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return getClass() + " " + nome;
    }
}
