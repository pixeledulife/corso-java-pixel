package lezione_09.esercizio02.tommaso;

public class Cane extends Animale{
    private boolean abbaia;

    public Cane(String sesso, String nome, boolean staAbbaiando) {
        super(sesso, nome);
        this.abbaia = staAbbaiando;
    }

    @Override
    public String toString() {
        return "Il cane " + getNome() + " di sesso " + getSesso() + " " + (abbaia ? "" : "non") + " sta abbaiando";
    }


}
