public class Rettangolo extends Quadrilatero{

    public Rettangolo(int base, int altezza){
        super(base, altezza);
    }

    public int getBase(){
        return this.base;
    }

    public int getAltezza(){
        return this.altezza;
    }

    public int area(){
        return getBase() * getAltezza();
    }

    public int lati(int base, int altezza){
        return 4;
    }
}
