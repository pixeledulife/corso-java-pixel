public class Parallelepipedo extends Quadrilatero{
    public int profondita;
    public Parallelepipedo(int base, int altezza, int profondita){
        super(base, altezza);
        this.profondita = profondita;
    }

    public int getProfondita(){
        return this.profondita;
    }

    public int getBase(){
        return this.base;
    }

    public int getAltezza(){
        return this.altezza;
    }

    public int area(){
        return getBase() * getAltezza();
    }

    public int volunme(){
        return area() * getProfondita();
    }

    public int lati(int base, int altezza, int profondita){
        return 12;
    }
}
