public abstract class Quadrilatero {
    public int base;
    public int altezza;
    public int profondita;

    public Quadrilatero(int base, int altezza){
        this.base = base;
        this.altezza = altezza;
    }

    abstract int area();
}
