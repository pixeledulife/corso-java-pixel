package lezione_09.esercizio04.zeno;
import java.util.*;
public class esercizio {


    public static void main(String[] args){

        HashMap<Integer, Dipendente> azienda = new HashMap<Integer, Dipendente>();

        azienda.put(1, new Dipendente("Carlo", 2020));
        azienda.put(2, new Dipendente("Tommaso", 2021));
        azienda.put(3, new Dipendente("Mattia", 2018));


        for(Integer i: azienda.keySet()){
            System.out.println("Chiave " + i);
        }

        for(Dipendente dipendente: azienda.values()){
            System.out.println("Dipendente " + dipendente.getNome() + " assunto nel " + dipendente.getDataAssunzione());
        }


    }
    
}
