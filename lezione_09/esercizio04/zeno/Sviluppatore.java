package lezione_09.esercizio04.zeno;

public class Sviluppatore extends Dipendente {
    String favLanguage;
    public Sviluppatore(String a, int b, String c){
        super(a, b);
        this.favLanguage = c;
    }
    @Override
    public String toString(){
        return this.nome + " " + this.dataAssunzione + " " + this.favLanguage;
    }
    
}
