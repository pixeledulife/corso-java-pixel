public class Furgone extends VeicoloAMotore{
    private int capacita;
        
    public Furgone(){
        super();
        this.setCapacita(10);
    }
    
    public Furgone(String marca, int anno, int cilindrata, int capacita){
        this.setFurgone(marca, anno, cilindrata, capacita);
    }
    
    public void setCapacita(int capacita){
        this.capacita = capacita;
    }
    
    public int getCapacita(){
        return this.capacita;
    }
    
    public void setFurgone(String marca, int anno, int cilindrata, int capacita){
        this.setVeicoloAMotore(marca, anno, cilindrata);
        this.setCapacita(capacita);
    }
}
