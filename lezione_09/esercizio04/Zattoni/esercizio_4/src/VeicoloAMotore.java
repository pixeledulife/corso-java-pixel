public class VeicoloAMotore {
    private String marca;
    private int anno;
    private int cilindrata;
    
    public VeicoloAMotore(){
        this.setVeicoloAMotore("Fiat500", 2002, 50);
    }
    
    public VeicoloAMotore(String marca, int anno, int cilindrata){
        this.setVeicoloAMotore(marca, anno, cilindrata);
    }
    
    public void setMarca(String marca){
        this.marca = marca;
    }
    
    public void setAnno(int anno){
        this.anno = anno;
    }
    
    public void setCilindrata(int cilindrata){
        this.cilindrata = cilindrata;
    }
    
    public String getMarca(){
        return this.marca;
    }
    
    public int getAnno(){
        return this.anno;
    }
    
    public int getCilindrata(){
        return this.cilindrata;
    }
    
    public void setVeicoloAMotore(String marca, int anno, int cilindrata){
        this.setAnno(anno);
        this.setCilindrata(cilindrata);
        this.setMarca(marca);
    }
    
    
}
