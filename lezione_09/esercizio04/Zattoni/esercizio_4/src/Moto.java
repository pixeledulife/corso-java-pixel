public class Moto extends VeicoloAMotore{
    private int tempi;
    
    public Moto(){
        super();
        this.setTempi(10);
    }
    
    public Moto(String marca, int anno, int cilindrata, int tempi){
        this.setMoto(marca, anno, cilindrata, tempi);
    }
    
    public void setTempi(int tempi){
        this.tempi = tempi;
    }
    
    public int getTempi(){
        return this.tempi;
    }
    
    public void setMoto(String marca, int anno, int cilindrata, int tempi){
        this.setVeicoloAMotore(marca, anno, cilindrata);
        this.setTempi(tempi);
    }
}
