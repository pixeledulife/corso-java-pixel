public class Auto extends VeicoloAMotore{
    private int porte;
    private String alimentazione;
    
    public Auto(){
        super();
        this.setPorte(4);
        this.setAlimentazione("Benzina");
    }
    
    public Auto(String marca, int anno, int cilindrata, int porte, String alimentazione){
        this.setAuto(alimentazione, porte, porte, porte, alimentazione);
    }
    
    public void setPorte(int porte){
        this.porte = porte;
    }
    
    public void setAlimentazione(String alimentazione){
        this.alimentazione = alimentazione;
    }
    
    public int getPorte(){
        return this.porte;
    }
    
    public String getAlimentazione(){
        return this.alimentazione;
    }
    
    public void setAuto(String marca, int anno, int cilindrata, int porte, String alimentazione){
            this.setVeicoloAMotore(marca, anno, cilindrata);
            this.setPorte(porte);
            this.setAlimentazione(alimentazione);
    }
    
    @Override
    public String toString(){
        return(this.getMarca()+" "+this.getAnno()+" "+this.getCilindrata()+" "+this.getPorte()+" "+this.getAlimentazione());
    }
    
}
