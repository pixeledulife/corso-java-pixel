package lezione_09.es3;
import java.util.*;

/*
Scrivere un programma in grado di istanziare un oggetto rappresentativo l'area di un rettangolo,
assegnate base ed altezza dello stesso. 

Dalla stessa classe derivare una classe che descriva un oggetto parallelepipedo 
che utilizzi i metodi della classe per il calcolo dell'area per poter restituire il volume del solido. 

Fare in modo che Rettangolo e Parallelepipedo estendano un metodo lati() che restituisca una stringa con i lati della figura.

Fare in modo che Rettangolo estenda a sua volta una classe abstract quadrilatero che abbia in comune con la sua sottoclasse il
 metodo Area()
 */
public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Rettangolo r1 = new Rettangolo();
            r1.insertData(s);
        Parallelepipedo p1 = new Parallelepipedo();

            System.out.println(p1.lati());
    }
}
