package lezione_07.esercizio01.zeno;

public class esercizio {
    public static void main(String[] args) {
        String frase = "Ciao";
        System.out.println("\nStampiamo la parola 10 volte");
        String accumolo = "";
        int i = 0;
        while (i < 10){
            accumolo = accumolo + frase + " ";
            i++;
        }
        System.out.println(accumolo);
        System.out.println("Ora stampiamo la parola 5 volte ma su righe diverse");
        i = 0;
        while (i < 5){
            System.out.println(frase);
            i++;
        }
    }

}
