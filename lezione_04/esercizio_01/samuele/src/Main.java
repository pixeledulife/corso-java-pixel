import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);
        System.out.println("Inserisci un valore: ");
        int a = src.nextInt();
        int absoluteVal;

        if(a > 0){
            absoluteVal = a;
        }else{
            absoluteVal = a * -1;
        }

        System.out.println("Il valore assoluto di " + a + " è " + absoluteVal);
        float b = (float) a;
        System.out.println("Il valore float diviso 2 di " + a + " è " + b / 2);
        System.out.println("Il valore int diviso 2 di " + a + " è " + a / 2);
    }
}