/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 */

import java.util.Scanner;

public class Esercizio_1 {
    public static void main(String[]args){
        try{
            //PRIMA PARTE
            
            System.out.print("Inserisci numero: ");
            Scanner input = new Scanner(System.in);
            int numero = input.nextInt();
            if(numero <0){
                numero *= -1;
            }
            System.out.println("In valore assoluto e': "+ numero);
            input.close();
            
            //SECONDA PARTE
            
            float numero_2 = (float)numero;
            System.out.println("La divisione tra "+ numero_2 + " e 2 e' " + numero_2 / 2);
            
            //TERZA PARTE
            
            System.out.println("La divisione tra "+ numero + " e 2 e' " +numero/2);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
