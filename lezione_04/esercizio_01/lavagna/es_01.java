package lezione_04.esercizio_01.lavagna;

import java.util.Scanner;

/*
    Si realizzi un programma che acquisisca    
    da tastiera un numero int e stampi il valore
    assoluto di tale numero, trasformare quindi 
    in float e stamparne il numero diviso 2. 
    Provare a vedere cosa si ottiene invece 
    dividendo un intero per 2.
 */
public class es_01 {
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci un numero: ");
        int a = sc.nextInt();
        sc.close();
        //int valore_assoluto = a > 0 ? a : a *- 1;
        int valore_assoluto;
        if(a>0){
            valore_assoluto = a;
        }
        else{
            valore_assoluto = a*-1;
        }
        System.out.println("Il valore assoluto di "+ a+ " è "+valore_assoluto);
        float b = (float)a;
        System.out.println("il valore float di " +b+ " diviso due è "+b/2);
        System.out.println("il valore int di " +b+ " diviso due è "+a/2);
    }
}