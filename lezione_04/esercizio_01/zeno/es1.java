/*
    Si realizzi un programma che acquisisca 
    da tastiera un numero int e stampi il valore
    assoluto di tale numero, trasformare quindi 
    in float e stamparne il numero diviso 2. 
    Provare a vedere cosa si ottiene invece 
    dividendo un intero per 2.
 */
package lezione_04.esercizio_01.zeno;

import java.util.Scanner;



public class es1{
    public static void main(String[] args) {
        esercizioA();
    }


    public static void esercizioA() {
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci un numero intero: ");
        int number = input.nextInt();
        System.out.println("Il valore assoluto è: "+Math.abs(number));
        float b = number;
        
        System.out.println("Il numero diviso due: "+(b /= 2));


        input.close();
    }

}
