package org.example;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci un numero: ");
        int num = sc.nextInt();
        sc.close();
        int abs;
        if (num < 0)
        {
            abs = num * -1;
        }
        else
        {
            abs = num;
        }
        System.out.println("Il numero in valore assoluto è: " + abs);

        float numF = abs;
        System.out.println("Il float diviso per 2 è: " + numF / 2);
        System.out.println("Il valore intero diviso per 2 è: " + abs / 2);
    }
}