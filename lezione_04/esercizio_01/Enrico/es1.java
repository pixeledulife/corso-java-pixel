import java.util.*;
import java.lang.*;

class es1{
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Inserire un numero intero: ");
        int num = s.nextInt();

        if(num>=0){
            System.out.println("Il valore assoluto di "+num+" è: "+num);
        }
        else{
            System.out.println("Il valore assoluto di "+num+" è: "+num * -1);
        }

        float num_float = (float) num;
        
        System.out.println("Il valore float di "+num+" diviso 2 è: "+num_float/2);
        System.out.println("Il valore int di "+num+" diviso 2 è: "+num/2);
    }
}