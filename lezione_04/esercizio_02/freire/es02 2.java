import java.util.Scanner;

public class es02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserire la temperatura: ");
        int temperatura = Integer.parseInt(sc.nextLine());
        String commento = crea_commento(temperatura);
        System.out.println(commento);
        sc.close();    
    }
    public static String crea_commento(int temp){
        if(temp <= 0){
            return "clima gelido"
        }else if(temp <= 10){
            return "clima veramente freddo"
        }else if(temp <= 20){
            return "clima freddo"
        }else if(temp <= 30){
            return "Temperatura normale"
        }else if(temp <= 40){
            return "clima caldo"
        }else if(temp > 40){
            return "clima veramente caldo"
        }
    }
}
