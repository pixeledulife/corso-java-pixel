import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserisci temperatura: ");
        int temp = sc.nextInt();

        if(temp < 0){
            System.out.println("Clima gelido");
        }else if(temp < 10){
            System.out.println("Clima veramente freddo");
        }else if(temp < 20){
            System.out.println("Clima freddo");
        }else if(temp < 30){
            System.out.println("Temperatura normale");
        }else if(temp < 40){
            System.out.println("Clima caldo");
        }else{
            System.out.println("Clima veramente caldo");
        }
    }
}