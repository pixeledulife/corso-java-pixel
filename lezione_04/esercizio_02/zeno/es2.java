/*
    Scrivere un programma che legga la temperatura in centigradi 
    e mostri un messaggio che segua la seguente didascalia:
    T < 0: “clima gelido”
    T 0-10: “clima veramente freddo”
    T 10-20: “clima freddo”
    T 20-30: “Temperatura normale”
    T 30-40: “clima caldo”
    T 40: “clima veramente caldo”
 */
package lezione_04.esercizio_02.zeno;

import java.util.Scanner;



public class es2{
    public static void main(String[] args) {
        esercizioA();
    }


    public static void esercizioA() {
        Scanner input = new Scanner(System.in);
        System.out.println("Inserisci la temperatura in Celsius: ");
        int temp = input.nextInt();
        
        
        if(temp < 0){
            System.out.println("clima gelido");
        }else if(temp >= 0 && temp < 10){
            System.out.println("clima veramente freddo");
        }else if(temp >= 10 && temp < 20){
            System.out.println("clima freddo");
        }else if(temp >= 20 && temp < 30){
            System.out.println("Temperatura normale");
        }else if(temp >= 30 && temp < 40){
            System.out.println("clima caldo");
        }else{
            System.out.println("clima veramente caldo");
        }

        

        input.close();
    }

}
