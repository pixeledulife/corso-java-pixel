package lezione_04.esercizio_02.lavagna;

import java.util.Scanner;

/*
    Scrivere un programma che legga la temperatura in centigradi 
    e mostri un messaggio che segua la seguente didascalia:
    T < 0: “clima gelido”
    T 0-10: “clima veramente freddo”
    T 10-20: “clima freddo”
    T 20-30: “Temperatura normale”
    T 30-40: “clima caldo”
    T 40: “clima veramente caldo”
 */
public class stazione_meteo {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserire temperatura: ");
        int temperatura = Integer.parseInt(sc.nextLine());
        String commento = crea_commento(temperatura);
        System.out.println(commento);
        sc.close();
    }
    public static String crea_commento(int temp){
        if(temp < 0) {
            return "clima gelido!";
        } else if(temp < 10 ) {
            return "clima veramente freddo";
        } else if (temp < 20) {
            return "clima freddo";
        } else if (temp < 30) {
            return "temperatura normale";
        } else if (temp < 40) {
            return "clima caldo";
        } else {
            return "clima bollente";
        }
    }
}
