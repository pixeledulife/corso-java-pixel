import java.util.*;

public class es2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("inserisci una temperatura");
        
        int t = s.nextInt();

        if(t < 0){
            System.out.println("clima gelido");
        }

        else if(t < 10){
            System.out.println("clima veramente freddo");
        }

        else if(t < 20){
            System.out.println("clima freddo");
        }
        else if(t < 30){
            System.out.println("clima normale");
        }
        else if(    t < 40){
            System.out.println("clima caldo");
        }
        else{
            System.out.println("clima veramente caldo");
        }

        s.close();
    }


}
