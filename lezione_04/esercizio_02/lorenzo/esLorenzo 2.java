import java.util.*;

// Scrivere un programma che legga la temperatura in centigradi 
// e mostri un messaggio che segua la seguente didascalia:
// T < 0: “clima gelido”
// T 0-10: “clima veramente freddo”
// T 10-20: “clima freddo”
// T 20-30: “Temperatura normale”
// T 30-40: “clima caldo”
// T 40: “clima veramente caldo”


public class esLorenzo {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("inserisci la temperatura: ");
        int temp = Integer.parseInt(s.nextLine());
        if (temp < 0){
            System.out.println("clima gelido");
        }
        else if (temp <= 10){
            System.out.println("clima veramente freddo");
        }
        else if (temp <= 20){
            System.out.println("clima freddo");
        }
        else if (temp <= 30){
            System.out.println("temperatura normale");
        }
        else if (temp <= 40){
            System.out.println("clima caldo");
        }
        else{
            System.out.println("clima veramente caldo");
        }
        s.close();

    }

}