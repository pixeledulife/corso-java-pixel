package esercizio_03.enrico;
import java.util.*;

public class es3 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int ct = 0;

        System.out.println("Dite amici ed entrate");
        String intro = s.nextLine();

        while(!intro.equals("amici") && ct!=3){
            if(ct==0){
                System.out.println("Non succede nulla, ma sento una presenza che si avvicina");
                System.out.println("Dite amici ed entrate");
                intro = s.nextLine();
            }
            else
                if(ct == 1){
                    System.out.println("Non succede nulla, ma la presenza è molto vicina");
                    System.out.println("Dite amici ed entrate");
                    intro = s.nextLine();
                }
                else
                    if(ct == 2){
                        System.out.println("Il mostro è qui, dobbiamo fuggire!");
                        System.exit(0);
                    }
                
            ct++;            
        }

        System.out.println("Benvenuti a Moria");
        System.exit(0);
    
    }
}
