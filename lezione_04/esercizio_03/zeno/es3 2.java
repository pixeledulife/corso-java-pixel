/*
    Scriviamo un programma che per prima cosa stampa la stringa “Dite amici ed entrate” 
    e ci chiede di inserire una parola (che dovrebbe essere “amici”). Se sbagliamo 
    il programma dovrà scrivere “Non succede nulla, ma sento una presenza 
    che si avvicina” e dovrà chiederci nuovamente la parola; se sbaglieremo 
    ancora dovrà scrivere “Non succede nulla, ma la presenza è molto vicina”
    e dovrà richiederci la parola. Se sbagliamo per la terza volta scriverà 
    “Il mostro è qui, dobbiamo fuggire!” e poi il programma deve finire.
    Se in qualsiasi momento inseriamo la parola “amici” il programma deve 
    stampare “Benvenuti a Moria” e deve terminare.
 */
package lezione_04.esercizio_03.zeno;

import java.util.Scanner;



public class es3{
    public static void main(String[] args) {
        esercizioA();
    }


    public static void esercizioA() {
        Scanner input = new Scanner(System.in);
        
        String parola = null;
        int i = 0;
        do{
            System.out.println("Dite amici ed entrate");
            parola = input.nextLine();
            if(!parola.equals("amici")){
                if(i == 0){
                    System.out.println("Non succede nulla, ma sento una presenza che si avvicina\n\n");
                }else if(i == 1){
                    System.out.println("Non succede nulla, ma la presenza è molto vicina\n\n");
                }else{
                    System.out.println("Il mostro è qui, dobbiamo fuggire!\n\n");
                    System.exit(0);
                }
                i++;  
            }

        }while(!parola.equals("amici"));
        System.out.println("Benvenuti a Moria\n\n");


        input.close();
    }

}
