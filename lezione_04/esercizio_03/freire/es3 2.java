import java.util.Scanner;

public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String pwd = "amici";
    System.out.println("Dite amici ed entrate");
    String parola = sc.nextLine();
    if(!parola.equals(pwd)){
        System.out.println("Non succede nulla, ma sento una presenza che si avvicina");
        parola = sc.nextLine();
        if(!parola.equals(pwd)){
            System.out.println("Non succede nulla, ma la presenza è molto avvicina");
            parola = sc.nextLine();
            if(!parola.equals(pwd)){
                System.out.println("Il mostro è qui, dobbiamo fuggire!");
            }
        }
    }
    if(parola.equals(pwd)){
        System.out.println("Benvenuti a Moria");
    }
    sc.close();
}