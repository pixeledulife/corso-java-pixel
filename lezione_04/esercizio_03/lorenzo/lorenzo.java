import java.util.*;

public class lorenzo {

//     Scriviamo un programma che per prima cosa stampa la stringa “Dite amici ed entrate” 
// e ci chiede di inserire una parola (che dovrebbe essere “amici”). Se sbagliamo 
// il programma dovrà scrivere “Non succede nulla, ma sento una presenza 
// che si avvicina” e dovrà chiederci nuovamente la parola; se sbaglieremo 
// ancora dovrà scrivere “Non succede nulla, ma la presenza è molto vicina”
//  e dovrà richiederci la parola. Se sbagliamo per la terza volta scriverà 
//  “Il mostro è qui, dobbiamo fuggire!” e poi il programma deve finire.
// Se in qualsiasi momento inseriamo la parola “amici” il programma deve 
// stampare “Benvenuti a Moria” e deve terminare.

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String pwd = "amici";
        System.out.println("Dite amici ed entrate");
        System.out.println("inserisci una parola: ");
        String parola = s.nextLine();
        if (!parola.equals(pwd)){
            System.out.println("Non succede nulla, ma la presenza è molto vicina");
            parola = s.nextLine();
            if(!parola.equals(pwd)){
                System.out.println("Non succede nulla, ma la presenza è molto vicina");
                parola = s.nextLine();
                if(!parola.equals(pwd)){
                    System.out.println("Il mostro è qui, dobbiamo fuggire!");
                }
            }
        }
        else if(parola.equals(pwd)){
            System.out.println("Benvenuti a Moria");
        }
    
        s.close();
    }
    }