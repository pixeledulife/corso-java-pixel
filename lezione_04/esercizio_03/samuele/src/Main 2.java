import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Boolean x = true;
        String a;
        
        for(int i = 1; x; i++){
            System.out.println("Parola d'ordine? ");
            a = sc.nextLine();
            if(a.equals("Amici")){
                System.out.println("Benvenuti a Moria");
                break;
            }else{
                if(i < 3){
                    System.out.println("Una presenza è vicina...");
                }else{
                    System.out.println("Il mostro è qui! Fuggite.");
                    x = false;
                }
            }
        }

    }
}