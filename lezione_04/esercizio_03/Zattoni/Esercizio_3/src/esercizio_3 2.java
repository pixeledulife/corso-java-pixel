/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 */
import java.util.Scanner;

public class Esercizio_3 {
    public static String amici(){
        Scanner input = new Scanner(System.in);
        
        for(int i = 0; i<3;i++){
            System.out.println("Dite 'amici' e potete entrare: ");
            switch(input.nextLine()){
                case "amici":
                    input.close();
                    return "Benvenuti a Moria";
                default:
                    System.out.println("Non succede nulla ma la presenza e molto vicina");
            }
        }
        
        input.close();
        return "Il mostro e' qui, fuggite sciocchi";
    }
    
    public static void main(String[]args){
        try{
            System.out.println(amici());
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
