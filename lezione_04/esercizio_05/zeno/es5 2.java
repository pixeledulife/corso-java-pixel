/*
    Primo esercizio

    Si scriva un programma che legga da tastiera
    i valori delle lunghezze dei
    tre lati di un triangolo (detti A, B e C),
    e determini:
    • se il triangolo è equilatero
    • se il triangolo è isoscele
    • se il triangolo è scaleno
    • se il triangolo è rettangolo.

Secondo esercizio

Data l’equazione
ax + b = 0
con a e b inseriti da tastiera, scrivere un programma per determinare il
valore di x, se esiste, che risolve l’equazione

Terzo esercizio

Si realizzi un programma per risolvere equazioni di secondo grado. 
In particolare, data una generica equazione di secondo grado nella forma
ax2 + bx + c = 0
dove a, b, c sono coefficienti reali noti e x rappresenta l’incognita, il programma determini
le due radici x1 ed x2 dell’equazione data, ove esse esistano.
Si identifichino tutti i casi particolari (a = 0, ∆ ≤ 0, ...) e si stampino gli opportuni messaggi informativi.

 */
package lezione_04.esercizio_05.zeno;

import java.util.Scanner;



public class es5{
    public static void main(String[] args) {
        esercizioC();
    }


    public static void esercizioA() {
        Scanner input = new Scanner(System.in);
        System.out.println("Lato A: ");
        int latoA = input.nextInt();
        System.out.println("Lato B: ");
        int latoB = input.nextInt();
        System.out.println("Lato C: ");
        int latoC = input.nextInt();

        if(latoA == latoB && latoB == latoC){
            System.out.println("EQUILATERO");
        }else if(((Math.pow(latoA, 2) + Math.pow(latoB, 2)) == Math.pow(latoC, 2)) || ((Math.pow(latoC, 2) + Math.pow(latoB, 2)) == Math.pow(latoA, 2)) || ((Math.pow(latoA, 2) + Math.pow(latoC, 2)) == Math.pow(latoB, 2))){
            System.out.println("RETTANGOLO");
        }else if(latoA == latoB || latoB == latoC || latoA == latoC){
            System.out.println("ISOSCELE");
        }else if(latoA != latoB && latoA != latoC && latoB != latoC){
            System.out.println("SCALENO");
        }


        input.close();
    }
    public static void esercizioB() {
        Scanner input = new Scanner(System.in);
        System.out.println("L'equazione è ax + b = 0");

        System.out.println("Inserisci a: ");
        float a = input.nextFloat();
        System.out.println("Inserisci b: ");
        float b = input.nextFloat();
        
        if(a == 0){
            System.out.println("La a è ugale a 0 pollo!");
        }
        System.out.println("x = "+((b * -1) / a));





        input.close();
    }

    public static void esercizioC() {
        Scanner input = new Scanner(System.in);
        System.out.println("L'equazione è ax^2 + bx + c = 0");

        System.out.println("Inserisci a: ");
        float a = input.nextFloat();
        System.out.println("Inserisci b: ");
        float b = input.nextFloat();
        System.out.println("Inserisci c: ");
        float c = input.nextFloat();
        
        if(a == 0){
            if(b == 0){
                System.out.println("La a è ugale a 0 pollo!");
                System.exit(0);
            }
            System.out.println("x = "+((c * -1) / b));
        }
        float delta = (b * b -4 * a * c);
        if(delta < 0){
            System.out.println("Impossibile!");
            System.exit(0);
        }
        double x1 = (b * (-1) + Math.sqrt(delta)) / (2 * a);

        double x2 = (b * (-1) - Math.sqrt(delta)) / (2 * a);

        System.out.println("X1 è uguale a: "+x1);
        System.out.println("X2 è uguale a: "+x2);



        





        input.close();
    }

}
