/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 */
import java.util.Scanner;
public class Esercizio_5 {
    public static void main(String[]args){
        try{
            //ESERCIZIO 1
            Scanner input = new Scanner(System.in);
            int[] angoli = new int[3];
            for(int i=0; i<3;i++){
                System.out.println("Inserisci lato: ");
                angoli[i] = input.nextInt();
            }
            for(int i = 0; i<angoli.length;i++){
                System.out.print(angoli[i]);
            }
            
            if(angoli[0] == angoli[1] && angoli[1] == angoli[2]){
                System.out.println("EQUILATERO");
            }else if(angoli[0] == angoli[1] || angoli[1] == angoli[2] || angoli[0] == angoli[2]){
                System.out.println("ISOSCELE");
            }else{
                System.out.println("SCALENO");
            }
           
            input.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
}
