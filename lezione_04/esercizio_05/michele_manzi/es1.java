import java.util.Scanner;

public class es1 {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.println("inserisci il primo dato: ");
        int num1 = s.nextInt();
        System.out.println("inserisci il secondo dato: ");
        int num2 = s.nextInt();
        System.out.println("inserisci il terzo dato: ");
        int num3 = s.nextInt();

        if (num1 == num2 && num2 == num3 && num1 == num3){
            System.out.println("Il triangolo è equilatero");
        }
        else if(num1 == num2 || num2 == num3){
            System.out.println("Il triangolo è isoscele");
        }
        else if(num1 > num2 && num1 > num3){
            if(num1 * num1 == (num2*num2) + (num3*num3)){
                System.out.println("Il triangolo è rettangolo");
            }
        }
        else if(num2 > num1 && num2 > num3){
            if(num2 * num2 == (num1*num1) + (num3 * num3)){
                System.out.println("Il triangolo è rettangolo");
            }

        }
        else if(num1 != num2 && num2 != num3 && num1 != num3){
            System.out.println("Il triangolo è scaleno");
        }
    }
 }