package esercizio_05.tommaso;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Inserisci a, b e c");
        int a = Integer.parseInt(in.nextLine());
        int b = Integer.parseInt(in.nextLine());
        int c = Integer.parseInt(in.nextLine());
        es1(a, b, c);
        es2(a, b);

    }
    
    public static String es1(int a, int b, int c) {
        if (a == b && b == c) {
            return "E' equilatero";
        } else if(a == b || b == c || a == c) {
            return "E' isoscele";
        } else if(a != b && b != c && a != c) {
            return "E' scaleno";
        } else if(a * a + b * b == c * c || b * b + c * c == a * a || c * c + a * a == b * b) {
            return "E' rettangolo";
        } else {
            return "Non calcolabile";
        }
    }

    public static int es2(int a, int b) {
        return b / a;
    }
}
