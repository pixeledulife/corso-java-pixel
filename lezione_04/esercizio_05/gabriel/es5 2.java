import java.util.*;

public class es5 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("inserisci lato A, lato B, lato C");
        int A = s.nextInt();
        int B = s.nextInt();
        int C = s.nextInt();

        if(A == B && B == C){
            System.out.println("il triangolo è equilatero");
        }
        else if(A*A + B*B == C*C || A*A + C*C == B*B || B*B + C*C == A*A){
            System.out.println("il triangolo è rettangolo");
        }
        else if(A == B || B == C || C == A ){
            System.out.println("è isoscele");
        }
        else{
            System.out.println("è scaleno");
        }
        s.close();
    }


}
