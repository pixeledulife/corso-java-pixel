import java.util.*;

public class es7 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("inserisci a e b e c");
        float a = s.nextFloat();
        float b = s.nextFloat();
        float c = s.nextFloat();
        float delta = b*b - (4*a*c);

        if(a == 0){
            System.out.println("x è " + (c*-1)/b);
        }
        else if(delta == 0){
            System.out.println((b*-1)/(2*a));
        }
        else if(delta < 0){
            System.out.println("delta minore di zero");
        }
        else{
            double x1 = (((b*-1) - Math.sqrt(delta)) / (2*a));
            double x2 = (((b*-1) + Math.sqrt(delta)) / (2*a));
            System.out.println(x1 + " , " + x2);
        }


        
        s.close();
    }

    
}
