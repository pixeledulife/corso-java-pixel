import java.util.*;

public class Es04c_Baldassari{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("inserisci a, b, c");
        float a = sc.nextFloat();
        float b = sc.nextFloat();
        float c = sc.nextFloat();

        float delta = b*b - 4*a*c;
        if (delta < 0){
            System.out.println("equazione impossibile");
        }
        else{
        
            float x1 = ((b*-1)+ (float)Math.sqrt(delta))/(2*a);
            float x2 = ((b*-1)- (float)Math.sqrt(delta))/(2*a);
            System.out.println("x1 = "+x1);
            System.out.println("x2 = "+x2);

        }
   

        


    }
}