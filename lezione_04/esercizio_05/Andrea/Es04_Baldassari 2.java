import java.util.*
;
public class Es04_Baldassari{
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println("Inserisci le misure di 3 lati");
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        if (a == b && a==c && b == c){
            System.out.println("triangolo equilatero");
        }
        else if (a*a + b*b == c*c || a*a + c*c == b*b || b*b + c*c == a*a){
            System.out.println("triangolo rettangolo");

        }
        else if (a == b || a == c || b == c){
            System.out.println("triangolo isoscele");
        }
        else{
            System.out.println("triangolo scaleno");
        }
        

        
    }
}