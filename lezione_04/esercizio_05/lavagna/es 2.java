package lezione_04.esercizio_05.lavagna;

import java.util.Scanner;

public class es {
    public static void main(String[] args){
       esercizio02(); 
    }
    /*
        Primo esercizio

        Si scriva un programma che legga da tastiera
        i valori delle lunghezze dei
        tre lati di un triangolo (detti A, B e C),
        e determini:
        • se il triangolo è equilatero
        • se il triangolo è isoscele
        • se il triangolo è scaleno
        • se il triangolo è rettangolo.
    */
    public static void esercizio01(){
        System.out.println("Esercizio sui triangoli");
        Scanner sc = new Scanner(System.in);

        System.out.println("Inserisci lato A, B e C: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        if(a == b && b == c){
            System.out.println("Il triangolo è equilatero");
            
        }else if(a == b || b == c || a == c){
            System.out.println("Il tringolo è isoscele");
        }else{
            System.out.println("Il triangolo è scaleno");
        }

        if(a*a==b*b+c*c||b*b==a*a+c*c||c*c==b*b+a*a){

            System.out.println("Il triangolo è rettangolo");
        }




        sc.close();
    }
    /*
      
        Secondo esercizio

        Data l’equazione
        ax + b = 0
        con a e b inseriti da tastiera, scrivere un programma 
        per determinare il
        valore di x, se esiste, che risolve l’equazione

     */
    public static void esercizio02(){
        //x = (b/a)*-1
        try{
        System.out.print("Inserisci il numero b: ");
        Scanner input = new Scanner(System.in);
        float b = input.nextFloat();
        System.out.print("Insersci il numero a: ");
        float a = input.nextFloat();
            System.out.println((b/a)*-1);
        }catch(Exception e){
            System.out.println(e);
        }
    }
    /*
        Terzo esercizio
        Si realizzi un programma per risolvere equazioni 
        di secondo grado. 
        In particolare, data una generica equazione 
        di secondo grado nella forma
        ax2 + bx + c = 0
        dove a, b, c sono coefficienti reali noti e x 
        rappresenta l’incognita, il programma determini
        le due radici x1 ed x2 dell’equazione data, 
        ove esse esistano.
        Si identifichino tutti i casi particolari (a = 0, ∆ ≤ 0, ...) 
        e si stampino gli opportuni messaggi informativi.
     */
}
