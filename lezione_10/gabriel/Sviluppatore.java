public class Sviluppatore extends Dipendente {
    String favLanguages;

    public Sviluppatore(String a, int b, String c){
        super(a, b);
        this.favLanguages = c;
    }

    @Override
    public String toString(){
         return nome + " " + dataAssunzione + " " + this.favLanguages;
    }
}
