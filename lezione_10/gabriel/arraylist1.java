import java.util.ArrayList;
import java.util.List;

public class arraylist1 {

    public static void sostituisci(List<String> lista) {
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) == "p") {
                lista.set(i, "l");
            }
        }
        for (String s : lista) {
            System.out.println(s);
        }

    }

    public static String concatena(List<String> lista) {
        String s = "";
        for (int i = 0; i < lista.size(); i++) {
            s = s + lista.get(i);
        }
        return s;
    }

    public static void main(String[] args) {
        List<String> lista = new ArrayList<String>();
        lista.add("p");
        lista.add("p");
        lista.add("a");
        lista.add("p");
        sostituisci(lista);
        System.out.println(concatena(lista));
    }

}
