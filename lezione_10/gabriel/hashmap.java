import java.util.HashMap;

public class hashmap {
    public static void main(String[] args) {
        HashMap<String, String> capitali = new HashMap<String, String>();
        capitali.put("Italia", "Roma");
        capitali.put("Francia", "Parigi");
        capitali.put("Inghilterra", "Londra");

        for(String i : capitali.keySet()){
            System.out.println("key: " + i + ", value: " + capitali.get(i));
        }

        HashMap<Integer, Dipendente> azienda = new HashMap<Integer, Dipendente>();
        azienda.put(1, new Dipendente("carlo", 2020));
        azienda.put(3, new Dipendente("Gianni", 2021));
        azienda.put(4, new Sviluppatore("Zero", 2022, "Java"));

        for(Dipendente dipendente: azienda.values()){
            System.out.println(dipendente.getNome() + " assunto nel " + dipendente.getDataAssunzione());
        }

        for(int i: azienda.keySet()){
            System.out.println("chiave: " + i + ", dipendete: " + azienda.get(i));
        }
    }
}
