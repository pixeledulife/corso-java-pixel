public class Dipendente {
    protected String nome; 
    protected int dataAssunzione;

    public Dipendente(String a, int b){
        this.nome = a;
        this.dataAssunzione = b;
    }

    public String getNome(){
        return nome;
    }

    public int getDataAssunzione(){
        return dataAssunzione;
    }

    @Override
    public String toString(){
         return this.nome + " " + this.dataAssunzione;
    }
   
}
