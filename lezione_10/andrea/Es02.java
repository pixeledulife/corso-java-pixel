import java.util.ArrayList;

public class Es02 {

    public static void sostituisci(ArrayList x, String y, String z) {
        for (int i = 0; i < x.size(); i++) {
            if (x.get(i).equals(y)) {
                x.set(i, z);
            }
            System.out.println(x.get(i));
        }
    }
    public static String concatena(ArrayList x) {
        String ret = "";
        for (int i = 0; i < x.size(); i++) {
            String y;
            y = x.get(i) + "";
            ret = ret + y;
        }
        return ret;
    }
    public static void main(String[] args) {

        ArrayList<String> prova = new ArrayList<String>();
        prova.add("a");
        prova.add("b");
        prova.add("c");
        prova.add("a");
        sostituisci(prova, "a", "x");
        String conc = concatena(prova);
        System.out.println(conc);
    }
}



