import java.util.ArrayList;
import java.util.HashSet;


public class Es03 {
    public static ArrayList riduci (ArrayList x){
        ArrayList ret = new ArrayList<>();
        if ((x.get(0).equals(x.get(1)))){
            ret.add(x.get(0));

        }
        for(int i = 1; i< x.size() ; i++){
            if ((!x.get(i).equals(x.get(i - 1)))  ){
                ret.add(x.get(i));
            }

        }
        return ret;

    }
    public static void main(String[] args) {
        ArrayList<String> prova = new ArrayList<String>();
        prova.add("a");
        prova.add("a");
        prova.add("b");
        prova.add("c");
        prova.add("c");
        prova.add("c");
        prova.add("a");
        ArrayList<String> ridotto = riduci(prova);

        for(int i = 0; i<ridotto.size(); i++){
            System.out.println(ridotto.get(i));
        }
    }
}
