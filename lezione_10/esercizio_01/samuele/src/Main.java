import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();
        al.add("p");
        al.add("i");
        al.add("p");
        al.add("p");
        al.add("o");

        System.out.println(al);
        System.out.println(concatena(al));

        System.out.println("---------------");

        System.out.println(sostituisci(al, "p", "l"));
        System.out.println(concatena(al));
    }

    public static ArrayList<String> sostituisci(ArrayList<String>al, String a, String b){
        for(int i = 0; i < al.toArray().length; i++){
            if(al.get(i).equals(a)){
                al.set(i, b);
            }
        }

        return al;
    }

    public static String concatena(ArrayList<String> al){
        String result = "";
        for(String i:al){
            result += i;
        }

        return result;
    }
}