import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        /*ArrayList<Character> chars = new ArrayList<Character>();
        chars.add('p');
        chars.add('o');
        chars.add('r');
        chars.add('p');
        chars.add('o');
        chars.add('u');
        chars.add('s');
        chars.add('e');
        Sostituisci(chars, 'p', 'g');*/
        ArrayList<Integer> nums = new ArrayList<Integer>();
        nums.add(1);
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(3);
        nums.add(2);
        nums.add(4);
        nums.add(4);
        RimuoviDuplicati(nums);
    }

    public static void Sostituisci(ArrayList<Character> chars, char valore1, char valore2) {
        for(int i = 0; i < chars.size(); i++) {
            if(chars.get(i) == valore1) {
                chars.set(i, valore2);
            }
        }

        for(Character i : chars) {
            System.out.println(i);
        }
    }

    public static void RimuoviDuplicati(ArrayList<Integer> nums) {
        for(int i = 0; i < nums.size(); i++) {
            if(nums.get(i) == nums.get(i + 1)) {
                nums.remove(i);
            }
        }

        for(int i : nums) {
            System.out.println(i);
        }
    }
}