import java.util.*;

public class main {
    public static void main(String[] args) {
        HashMap<String, String> paesiCapitali = new HashMap<String, String>();
        paesiCapitali.put("Italia", "Roma");
        paesiCapitali.put("Romania", "Bucharest");
        paesiCapitali.put("Francia", "Parigi");
        paesiCapitali.put("Spagna", "Madrid");

        for (String i:paesiCapitali.keySet()){
            System.out.println(i);
        }

        System.out.println("============================");

        for (String i:paesiCapitali.values()){
            System.out.println(i);
        }

        System.out.println("============================");

        for (String i:paesiCapitali.keySet()){
            System.out.println("Key: " + i + " || Value: " + paesiCapitali.get(i));
        }
    }
}
