import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        esercizioAzienda();
        esercizio01();
        esercizio02();
    }

    public static void esercizio01(){
        ArrayList<Character> arr = new ArrayList<Character>();
        arr.add('p');
        arr.add('i');
        arr.add('p');
        arr.add('p');
        arr.add('o');
        ArrayList<Character> nuovo = sostituisci(arr, 'p','l');
        System.out.println(nuovo);

    }

    public static void esercizio02() {
        ArrayList<String> arr = new ArrayList<String>();
        arr.add("p");
        arr.add("i");
        arr.add("p");
        arr.add("p");
        arr.add("o");
        String nuovo = allElements(arr);
        System.out.println(nuovo);

    }

    static String allElements(ArrayList<String> nuovo ){
        StringBuilder a = new StringBuilder();
        for(String b : nuovo){
            a.append(b);
        }
        return a.toString();
    }

    public static ArrayList<Character> sostituisci(ArrayList<Character> a, char b, char c){

        ArrayList<Character> n = new ArrayList<Character>();
        for(int cont = 0; cont<a.size(); cont++){
            char d = a.get(cont);
            if( d != 'p' ) {
                n.add(d);
            }
            else{
                n.add('l');
            }
        }
        return n;
    }

    public static void esercizioAzienda(){
        HashMap<Integer, Dipendente> azienda = new HashMap<Integer, Dipendente>();
        azienda.put(1, new Dipendente("Carlo", 2020));
        azienda.put(4, new Dipendente("Tommaso", 2021));
        azienda.put(3, new Dipendente("Mattia", 2022));

        for(Integer i:azienda.keySet()){
            System.out.println("Chiave "+ i);
        }

        for(Dipendente dipendente: azienda.values()){
            System.out.println("Dipendente "+dipendente.getNome()+" assunto nel "+dipendente.getDataAssunzione());
        }

        for(int i: azienda.keySet()){
            System.out.println("Chiave: "+i+" valore: "+azienda.get(i));
        }
    }

}