import static org.junit.jupiter.api.Assertions.*;

class SommaTest {

    @org.junit.jupiter.api.Test
    void sum() {
        Somma somma = new Somma();
        int val = somma.sum(1,2);
        assertEquals(val, 3);
    }

    @org.junit.jupiter.api.Test
    void sum2() {
        Somma somma = new Somma();
        int val = somma.sum(1,3);
        assertEquals(val, 3);
    }
}