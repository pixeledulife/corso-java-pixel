/*

Leggere  un array di 10 interi e stampare solo 
i numeri che appaiono nell’array una volta soltanto.
Ad esempio se l’array contiene  1, 2, 3, 1, 2, 4 
il programma stamperà  3, 4

Creare un array di interi con 8 posizione 
e riempirlo con numeri casuali compresi tra 
1 e 90 senza ripetizioni. Stampare il contenuto
man mano il contenuto dell’array.

Vi ricordate da python? eseguire un bubblesort
da un array preso in input.
Esercitarsi a imparare a memoria tale algoritmo.

 */
package lezione_08.esercizio01.zeno;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import javax.sound.sampled.AudioFormat;


public class es1{
    
    public static void main(String[] args) {
        es2();
    
        
    }
    public static void es1() {
        int[] array = {1, 2, 3, 3, 4};
    
        for(int i = 0; i < array.length; i++){
            int counter = 0;
            int n = array[i];
            for(int j = 0; j < array.length; j++){
                if(array[i] == array[j]){
                    counter++;
                }
            }
            if(counter == 1){
                System.out.println(n);
            }
        }
    }
    /*/**
    Creare un array di interi con 8 posizione 
    e riempirlo con numeri casuali compresi tra 
    1 e 90 senza ripetizioni. Stampare il contenuto
    man mano il contenuto dell’array.
     */

    
    public static void es2() {
        int[] array = new int[8];
        int max = 8;
        int min = 1;
        int range = max - min + 1;
        for (int i = 0; i < array.length; i++) {
            int rand = (int)(Math.random() * range) + min;
            //System.out.println(rand);
            int count = 0;
            for(int j = 0; j < array.length; j++){
                if(array[j] == rand){
                    count++;
                }
            }
            if (count == 0) {
                array[i] = rand;
                System.out.println(array[i]);
            }
            else{
                i--;
            }
    }
}
/*
Vi ricordate da python? eseguire un bubblesort
da un array preso in input.
Esercitarsi a imparare a memoria tale algoritmo.
 */
/**
 * 
 */
public static void es3() {
    int[] array = {4, 3, 1, 9, 2, 6};
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array.length - 1; j++){
                if(array[j] > array[j + 1]){
                    int n = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = n;
                }
            }
        }   
        for(int i = 0; i < array.length; i ++){
            System.out.println(array[i]);
        }
}
}