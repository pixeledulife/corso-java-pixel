import java.util.Scanner;

// Leggere  un array di 10 interi e stampare solo 
// i numeri che appaiono nell’array una volta soltanto.
// Ad esempio se l’array contiene  1, 2, 3, 1, 2, 4 
// il programma stamperà  3, 4

public class es1 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 1, 2, 4};
        for(int i = 0; i < array.length; i++){
            int count = 0;
            int n = array[i];
            for(int j = 0; j < array.length; j++){
                if(array[i] == array[j]){
                    count++;
                }
            }
            if(count == 1){
                System.out.println(n);
            }
        }
        
    }

}