// Creare un array di interi con 8 posizioni
// e riempirlo con numeri casuali compresi tra 
// 1 e 90 senza ripetizioni. Stampare il contenuto
// man mano il contenuto dell’array.

public class es2 {
    public static void main(String[] args) {
        int[] array = new int[8];
        int max = 10;
        int min = 1;
        int range = max - min + 1;
        for (int i = 0; i < array.length; i++) {
            int rand = (int)(Math.random() * range) + min;
            int count = 0;
            for(int j = 0; j < array.length; j++){
                if(array[j] == rand){
                    count++;
                }
            }
            if (count == 0) {
                array[i] = rand;
                System.out.println(array[i]);
            }
            else{
                i--;
            }
        }
    }
}
