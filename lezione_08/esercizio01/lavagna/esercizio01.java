
import java.lang.Math;

public class esercizio01 {

    public static void main(String[] args){
        esercizio02();
    }

    /*
        Leggere  un array di 10 interi e stampare solo 
        i numeri che appaiono nell’array una volta soltanto.
        Ad esempio se l’array contiene  1, 2, 3, 1, 2, 4 
        il programma stamperà  3, 4
    */
    public static void esercizio01(){
        int[] numArray = {1,2,3,1,2,4};

        for (int i = 0;i < numArray.length; i++) {

            int cont = 0;
            for (int j = 0; j < numArray.length; j++) {
                if(numArray[i] == numArray[j]&&i!=j) {
                    cont++;
                }
            }
            if(cont==0)
                System.out.println(numArray[i]);
            
        }
    }

    /* 
        Creare un array di interi con 8 posizioni 
        e riempirlo con numeri casuali compresi tra 
        1 e 90 senza ripetizioni. Stampare
        man mano il contenuto dell’array.
    */
    public static void esercizio02(){

        int arr[]={-1,-1,-1,-1,-1,-1,-1,-1};
        // define the range
        int max = 90;
        int min = 1;
        int range = max - min + 1;

        int cont=0;
        while (true) {
            if(cont==arr.length)
                break;
            int rand = (int)(Math.random() * range) + min;
            //check if array already contains the number
            boolean contains = false;
            for(int i=0; i<arr.length;i++){
                if(rand==arr[i]){
                    contains = true;
                }
            }
            if(!contains){
                arr[cont]=rand;
                cont++;
            }
        }

        System.out.print("[");
        for(int i = 0; i<arr.length; i++){
            System.out.print(""+arr[i]+" ");
        }
        System.out.print("]");
    }

    /*
        Vi ricordate da python? eseguire un bubblesort
        da un array preso in input.
        Esercitarsi a imparare a memoria tale algoritmo.
    */
    public static void esercizio03(){
        int[] arr = {1,2,3,1,2,0};
        for(int i = 0; i<arr.length-1; i++){
            for(int j=i+1; j<arr.length; j++){
                if(arr[j]<arr[i]){
                    int swap = arr[j];
                    arr[j]=arr[i];
                    arr[i]=swap;
                }
            }
        }
        System.out.print("[");
        for(int i = 0; i<arr.length; i++){
            System.out.print(""+arr[i]+" ");
        }
        System.out.print("]");
    }
}
