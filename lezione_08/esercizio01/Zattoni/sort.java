
import java.util.Random;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author matti
 */
        
public class sort {
    
    public static int[] unicita(int[] array){
        int[] result=new int[array.length];
        int pos= 0;
        for(int i=0;i<array.length;i++){
            int volte = 0;
            for(int num:array){
                if(array[i] == num){
                    volte++;
                }
            }
            if(volte==1){
                result[pos]= array[i];
                pos++;
            }
        }
        return result;
    }
    
    public static String toArray(int[] array){
        String result="";
        for(int num:array){
            if(num!=0){
                result+=num+" ";
            }
        }
        return result;
    }
    
    public static int[] randomArray(){
        int[] result = new int[8];
        Random random = new Random();
        int num= 0;
        int pos= 0;
        boolean test;
        for(int i=0;i<8;i++){
            num= random.nextInt(1,90);
            test = true;
            for(int let:result){
                if(let==num){
                    test = false;
                }
            }
            //System.out.println(num);
            if(test){
                result[pos]=num;
                pos++;
            }else{
                i--;
            }
        }
        return result;
    }
    
    public static int[] ordina(int[] array){
        int[] result=new int[array.length];
        int[] copia= array;
        int max;
        int pos=0;
        int iter =0;
        for(int i=0;i<copia.length;i++){
            do{
                max = copia[iter];
                if(max==0){
                    iter++;
                }
            }while(max==0);
            pos=iter;
            for(int j=0;j<copia.length;j++){
                if(max > copia[j] && copia[j]!=0){
                    max = copia[j];
                    pos = j;
                }
            }
            result[i]=max;
            copia[pos]=0;
            
        }
        return result;
    }
    
    public static void main(String[] args) {
        int[] array = {1,2,3,5,7,9,1};
        int[] result = randomArray();
        System.out.println(toArray(unicita(array)));
        System.out.println(toArray(result));
        System.out.println(toArray(ordina(result)));
    }
}
