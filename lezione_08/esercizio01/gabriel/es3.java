public class es3 {
    public static void main(String[] args) {
        int[] array = {4, 3, 1, 9, 2, 6};

        for(int i = 0; i < array.length - i; i++){
            for(int j = 0; j < (array.length - 1); j++){
                if(array[j] > array[j + 1]){
                    int n = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = n;
                }
            }
        }

        for(int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
    }
}
