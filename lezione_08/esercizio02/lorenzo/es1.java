import java.util.Scanner;

/**
 Ogni mattina entro le 10 i campeggiatori che hanno terminato il proprio soggiorno devono lasciare libera la piazzola
 che hanno occupato. Si presentano quindi a pagare N persone, ciascuna delle quali deve dichiarare:
- il numero di giorni trascorsi
- il numero di persone
- il tipo di alloggio (tenda o roulotte)
- il tipo di veicolo (auto o moto)
Calcola e stampa quanto deve pagare ciascun gruppo in base a questo listino prezzi: 15 euro per persona al giorno 
17 euro per tenda al giorno 15 euro per roulotte al giorno 14 euro per auto al giorno 10 euro per moto al giorno

Calcolare anche l’incasso totole del campeggio per quella giornata. Usare almeno una procedura o una funzione

Testare il codice con un caso d’uso in cui arrivano 5 utenti e alla fine viene calcolato l’incasso di fine giornata

 */
public class es1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("quante persone siete? ");
        int persone = s.nextInt();
        System.out.println("in totale c'è da pagare " + spesa(persone));
        s.close();
    }

    public static int spesa(int persone){
        int totale = 0;
        Scanner sc = new Scanner(System.in);
        Scanner s = new Scanner(System.in);

        System.out.println("quanti giorni siete stati?");
        int giorni = sc.nextInt();
        System.out.println("siete in tenda o roulotte?");
        String alloggio = s.nextLine();
        if(alloggio.equals("tenda")){
            totale = ((15 * persone ) + 17) * giorni;
        }
        else if(alloggio.equals("roulotte")){
            totale = ((15 * persone ) + 15) * giorni;

        }
        System.out.println("siete in macchina o moto?");
        String veicolo = sc.nextLine();
        if(veicolo.equals("macchina")){
            totale += 14 * giorni;
        }
        else if(veicolo.equals("moto")){
            totale += 10 * giorni;
        }
        sc.close();
        s.close();

        return totale;
    }
}

