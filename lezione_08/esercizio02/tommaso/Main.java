package lezione_08.esercizio02.tommaso;

import java.lang.reflect.Array;

public class Main {
    /*
    Creare un array di interi con 8 posizione 
     e riempirlo con numeri casuali compresi tra 
    1 e 90 senza ripetizioni. Stampare il contenuto
    man mano il contenuto dell’array.
     */
    public static void main(String[] args) {
        int[] array = {1,1,1,1,1,1,1,1};
        for( int i = 0; i < array.length; i++) {
            array[i] = (int)(Math.random() * (1 + 90) + 1);
            System.out.println(array[i]);
        }

    }
}
