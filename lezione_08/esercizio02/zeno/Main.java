package lezione_08.esercizio02.zeno;

public class Main {
    public static void main(String[] args){
        Rettangolo rettangolo = new Rettangolo();

        int a = rettangolo.latoA;
        System.out.println("Il valore di lato è " + a);

        int b = rettangolo.getLatoB();
        System.out.println("Il valore del lato è " + b);

        rettangolo.setLatoB(8);
        b = rettangolo.getLatoB();
        System.out.println("Il valore cambiato del lato è: " + b);
    }
    
}
