package lezione_08.esercizio02.zeno;

public class Rettangolo {
    public int latoA = 5;

    private int latoB = 7; 
    
    public int getLatoB(){
        return latoB;
    }
    public void setLatoB(int val){
        this.latoB = val;
    }
}
