package lezione_08.esercizio02.zeno;
/*
 Ogni mattina entro le 10 i campeggiatori che hanno terminato il proprio soggiorno devono lasciare libera la piazzola che hanno occupato. Si presentano quindi a pagare N persone, ciascuna delle quali deve dichiarare:
- il numero di giorni trascorsi
- il numero di persone
- il tipo di alloggio (tenda o roulotte)
- il tipo di veicolo (auto o moto)
Calcola e stampa quanto deve pagare ciascun gruppo in base a questo listino prezzi: 15 euro per persona al giorno 17 euro per persona al giorno 15 euro per roulotte al giorno 14 euro per auto al giorno 10 euro per moto al giorno

Calcolare anche l’incasso totole del campeggio per quella giornata. Usare almeno una procedura o una funzione

Testare il codice con un caso d’uso in cui arrivano 5 utenti e alla fine viene calcolato l’incasso di fine giornata
 */
import java.util.Scanner;

public class es {
    
    public static void main(String[] args) {
        int persona = 15;
        int tenda = 17;
        int roulotte = 15;
        int auto = 14;
        int moto = 10;
        int incasso = incasso(persona, tenda, roulotte, auto, moto);
        System.out.println("Devi pagare " + incasso + "€");
        
    }
    public static int incasso(int persona, int tenda, int roulotte, int auto, int moto){
        Scanner sc=new Scanner(System.in);  
        System.out.println("in quanti siete?");
        int persone = sc.nextInt();
        int incasso = persone * persona;
        System.out.println("Tenda o roulotte?");
        switch (sc.nextLine()) {
            case "tenda":
                incasso = incasso + tenda;
                break;
            case "roulotte":
                incasso = incasso + roulotte;
                break;
            case "auto":
                incasso = incasso + auto;
                break;
            case "moto":
                incasso = incasso + moto;
            default:
                break;
        }
        System.out.println("Quanti giorni siete rimasti?");
        int giorni = sc.nextInt();



        return (incasso * giorni);
    }
    
}
