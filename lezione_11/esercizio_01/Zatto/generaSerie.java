public class generaSerie {
    int[] array;
    int colonna;

    public generaSerie(int numcolonna){
        this.colonna= numcolonna;
        this.setArray(numcolonna);
    }

    public void setArray(int numcolonna){
        int[] result = new int[32];
        int numpartenza = (int)Math.pow(2,numcolonna);
        int inse = numpartenza;
        int count = 0;

        for(int it=0;it<32;it++){
            if(count<numpartenza){
                result[it]=inse;
            }else{
                count=0;
                inse+=numpartenza;
                result[it]=inse;
            }
            inse++;
            count++;
        }
        this.array=result;
    }

    public int[] getArray(){
        return this.array;
    }




}
