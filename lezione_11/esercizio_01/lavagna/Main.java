import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Pensa un numero da 0 a 63");
        int guessed_nbr = 0;
        for(int i = 1; i<=6; i++) {
            System.out.println("è per caso un numero fra questi? ( 1 = sì ) ( 0 = no ) ");
            System.out.println(generateColumn(i, 63));
            Scanner sc = new Scanner(System.in);
            int ans = sc.nextInt();
            guessed_nbr += ans==1?(int)Math.pow(2, i-1):0;

        }
        System.out.println("Il numero è "+guessed_nbr);
    }

    public static String generateColumn(int i, int upperbound) {
        String question = "";
        int first=(int)Math.pow(2, i-1);
        int cont2 = 0;
        for(int cont = first; cont <= upperbound; cont++ ) {
            if (cont2 >= first) {
                cont += first-1;
                cont2 = 0;
            } else {
                question += cont+" ";
                cont2++;
            }
        }
        return question;
    }
}