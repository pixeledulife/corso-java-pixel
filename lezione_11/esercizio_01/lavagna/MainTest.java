import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void generateColumnTest() {
        String a = Main.generateColumn(1,10);
        System.out.println("Result = "+a);
        assertEquals("1 3 5 7 9 ", a);
        String a2 = Main.generateColumn(2,10);
        System.out.println("Result = "+a2);
        assertEquals("2 3 6 7 ", a2);
        String a3 = Main.generateColumn(4,10);
        System.out.println("Result = "+a3);
        assertEquals("8 9 ", a3);
        String a5 = Main.generateColumn(3,14);
        System.out.println("Result = "+a5);
        assertEquals("4 5 6 7 12 13 ", a5);
    }
}