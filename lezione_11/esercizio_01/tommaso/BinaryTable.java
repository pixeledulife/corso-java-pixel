package lezione_11.esercizio_01.tommaso;

import java.util.ArrayList;

public class BinaryTable {

    private int complexity = 64;

    public BinaryTable(int maxNumber) {
        this.complexity = maxNumber;
    }

    public void getBinaryTable() {
        ArrayList<Integer> firstCol = getFirstColumn();
        ArrayList<Integer> secondCol = getSecondColumn();
        ArrayList<Integer> thirdCol = getThirdColumn();
        ArrayList<Integer> fourthCol = getFourthColumn();
        ArrayList<Integer> fifthCol = getFifthColumn();
        ArrayList<Integer> sixthCol = getSixthColumn();
        for(int i = 0; i < complexity; i++) {
            String row = "";
            row += firstCol.get(i) + "  ";
            row += secondCol.get(i) + " ";
            row += thirdCol.get(i) + "  ";
            row += fourthCol.get(i) + " ";
            row += fifthCol.get(i) + "  ";
            row += sixthCol.get(i) + "  ";
            System.out.println(row);
        }
        
    }

    public ArrayList<Integer> getFirstColumn() {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for(int i = 1; i < complexity; i++) {
            if(i % 2 == 1) {
                numList.add(i);
            }
        }
        return numList;
    }

    public ArrayList<Integer> getSecondColumn() {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for(int i = 2; i < complexity; i+=4) {
            int m = i + 2;
            for(int j = i; j < (m); j++) {
                numList.add(j);
            }
        }
        return numList;
    }

    public ArrayList<Integer> getThirdColumn() {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for(int i = 4; i < complexity; i+=8) {
            int m = i + 4;
            for(int j = i; j < m; j++) {
                numList.add(j);
            }
        }
        return numList;
    }

    public ArrayList<Integer> getFourthColumn() {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for(int i = 8; i < complexity; i+=16) {
            int m = i + 8;
            for(int j = i; j < m; j++) {
                numList.add(j);
            }
        }
        return numList;
    }

    public ArrayList<Integer> getFifthColumn() {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for(int i = 16; i < complexity; i+=32) {
            int m = i + 16;
            for(int j = i; j < m; j++) {
                numList.add(j);
            }
        }
        return numList;
    }

    public ArrayList<Integer> getSixthColumn() {
        ArrayList<Integer> numList = new ArrayList<Integer>();
        for(int i = 32; i < complexity; i+=64) {
            int m = i + 32;
            for(int j = i; j < m; j++) {
                numList.add(j);
            }
        }
        return numList;
    }
}
