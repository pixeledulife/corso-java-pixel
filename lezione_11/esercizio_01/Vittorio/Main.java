import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        int n = 0;
        for(int i = 1; i <= 6; i++){
            System.out.println("E' in questi numeri (s/n): ");
            System.out.println( getColumn(i) );

            String r = s.nextLine().trim();


            n |= (r.toLowerCase().charAt(0) == 's')?(1 << (i - 1)):0;

        }
        System.out.println("Il tuo numero è: " + n);
        s.close();
    }
    
    static String getColumn(int index){
        int n = (1 << (index - 1));
        String c = "";
        for(int i = 0; i < 63; i++){
            if ((i|n) == i){
                c += i + ", ";
            }
        }
        return c;
    }
}